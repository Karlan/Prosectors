
--//=====================================================================================================

local function f_action_1()
    print('f_action_1')
end

--//----- ��������� ������� --------------------------------------------------------------------------------------
local timers = {}
local counter = 0
local time_factor = system_ini():r_u32('alife', 'time_factor')

local time_hud

local function _create_hud(hud)
    time_hud = CUIStatic()
    time_hud:Init(unpack(hud.rect))
    time_hud:SetAutoDelete(true)
    time_hud:InitTexture(hud.texture)
    time_hud:SetStretchTexture(true)
    time_hud:SetTextX(10)
    time_hud:SetTextY(20)
    time_hud:SetTextColor(unpack(hud.color))
    time_hud:SetFont(hud.font)
end

local function _new_name()
    counter = counter + 1
    return '_generated_name_'..counter
end

local function _update_pos(start_pos)
    start_pos = start_pos or 1
    for i = start_pos, #timers do
        timers[i].data.pos = i
    end
end

local function _sort()
    table.sort(timers, function (a,b) return a.data.complete < b.data.complete end)
    _update_pos()
end

function _load()
    timers = storage.timers
end

function _save()
    storage.timers = timers
end
--//----- ������ ������� -----------------------------------------------------------------------------------------
local methods = {}

-- ������ (��������������) �������
function methods:start()
    local timer = self.data
    local add = timer.remain or (timer.type == 'game' and timer.time/time_factor or timer.time)
    timer.complete = time_global() + add
    timer.remain = nil -- ��� ����� ����� ���������� ��� ��������� �������
    _sort()
    if timer.hud.show then
        get_hud():AddDialogToRender(time_hud)
    end
    return self
end

-- ������������ ������ �������
function methods:pause()
    local timer = self.data
    timer.remain = timer.complete - time_global()
    timer.complete = math.huge
    _sort()
    return self
end

-- ��������� �������
function methods:stop()
    self:pause()
    self:hud(false)
    self.data.remain = nil
    return self
end

-- �������� �������
function methods:kill()
    self:hud(false)
    local timer = self.data
    timers[timer.name] = nil
    table.remove(timers, timer.pos)
    _update_pos()
end

function methods:hud(b)
    local hud = self.data.hud
    if b == nil then return hud end
    if b == hud.show then return end
    
    if b == true then
        _create_hud(hud)
        hud.show = true
        if timer.remain then -- ���� ��� �������
            get_hud():AddDialogToRender(time_hud)
        end
    elseif b == false then
        get_hud():RemoveDialogToRender(time_hud)
        hud.show = false
    end
    
    return self
end

-- ��������� ���� ������� ('game' or 'real'). �������������� "�� ����", �.�. � ������� ������ ����� ������ ���
-- ������ �������, ���������� ����� ����� ��������� � ��������� ���������� ����.
function methods:type(t)
    local timer = self.data
    local typ = timer.type
    if not t then return typ end -- ��� ������ ��� ���������� ���������� ��� �������
    if typ ~= t and (t == 'game' or t == 'real') then
        timer.type = t
        -- ������� "�� ����"
        local remain = timer.remain
        local complete = timer.complete
        
        if remain then                    -- ���� ����������
            timer.remain = (t == 'game') and remain/time_factor or remain*time_factor
        elseif complete ~= math.huge then -- ���� �������
            local global_time = time_global()
            local x = global_time - timer.complete
            local y = (t == 'game') and x/time_factor or x*time_factor
            timer.complete = global_time + y
        end
        _sort()
    end
    
    return self
end

-- ��������� ����� ����������� ������� (true or false)
function methods:cyclic(b)
    local timer = self.data                  -- �������� ������� ������� �������
    if b == nil then return timer.cyclic end -- ��� ������ ��� ���������� ���������� ���������
    timer.cyclic = b
    return self
end

-- ��������� ����� ������� ��� ����������� ������� �� ����� � �������. ����. ������ ������ timer('any_name').
-- � ���� ����� ���������� � ��� - timer.any_name
function methods:name(n)
    local timer = self.data       -- �������� ������� ������� �������
    local name = timer.name       -- �������� ������� ��� �������
    if not n then return name end -- ��� ������ ��� ���������� ���������� ��� �������
    if name ~= n then             -- ���� ��� ����� ��, �� ������ �� ������
        timers[name] = nil        -- ������� ������� ������ �� ������� �������
        timers[n] = self          -- ������� ����� ������ � ������� ��������
        timer.name = n            -- �������� �� ����� ���  � ������� ������ �������
    end
    return self
end

-- ��������� ����� ������������ � ���������� ��� ������������ ������� �������
-- �������� ���� ������ �� ���� � ����������, � ��� ��������� ���������� � �������� ���������,
-- ��� ���������� � ���������� ������, ����� ��������� ������ ��� ��������� (methods:stop)
function methods:reset(b)
    local timer = self.data              -- �������� ������� ������� �������
    if b == nil then return timer.reset end -- ��� ������ ��� ���������� ���������� ��������� ������������
    timer.reset = b
    return self
end

-- �������� ����� ������������ ��� ��� ����������� �������, ��� ���������� ����� �� ������������
-- �������������� �������. ���� ����� �� �������� ������������ ����� �������.
-- �������� ���� 10-���. ������ �� ���� � ����������. � � ����������� �� �������� ����� ���������\���������
-- ����� ���������� � �������� �����������.
function methods:add(t)
    local timer = self.data           -- �������� ������� ������� �������
    
    local remain = timer.remain 
    if remain then
        timer.remain = remain + t     -- ���� ������ ����������
    end
    
    local complete = timer.remain
    if complete ~= math.huge then
        timer.complete = complete + t -- ���� ������ ��������
    end
    
    _sort()
    return self
end

-- ���� ����� �������� ������ ������������ �����, �������� ��� �������� �������.
-- ����� ����� ��� ��� ���������� ������� ����� �������������� ������ ����� ������������ (methods:reset).
-- ��� ��������� ������� ������ ������� "�� ����" ������������ 'methods:add'
function methods:time(t)
    local timer = self.data             -- �������� ������� ������� �������
    if not t then return timer.time end -- ��� ������ ��� ���������� ���������� ��� �������
    if t < 0 then t = 0 end             -- ����� � ����� �� �������
    timer.time = t*1000                 -- ���������� ����� ����� ������
    return self
end

-- ��������� ������� ������� �������. �� ��������� 1 ��� (1000 ����)
function methods:period(p) -- � �������� !!! �� ���� � ��� �������� ������. � �������� ��� �������������??? !!!
    local timer = self.data               -- �������� ������� ������� �������
    if not t then return timer.period end -- ��� ������ ��� ���������� ���������� ������� ������� �������
    timer.period = p * 1000               -- ���������� ����� ����� �������
    return self
end

function methods:action(a)
    if not a then return self.data.action end -- ��� ������ ��� ���������� ���������� ��� 'action'
    if self.data.action ~= a then             -- ���� �������� ����� ��, �� ������ �� ������
        self.data.action = a
    end
    return self
end
--//--------------------------------------------------------------------------------------------------------------
local mt_timer = {__metatable = nil} -- ��������� ����������� ��������� ����������

function mt_timer.__index(t,k)
    return timer(k)
end

function mt_timer.__call(timer, name, time, action)
    -- ������������� ���������
    action = action or (type(time) ~= 'number' and time)
    time = (type(time) == 'number') and time or name or 0
    name = (type(name) == 'string') and name or _new_name()
    -- ���� ���� ������ � ����� ������, �� ���������� ��� (��������� �������)
    local tm = timers[name]
    if tm then return tm end
    
    local new_timer = {}
    new_timer.data = {
        name     = name     ,
        time     = time*1000, -- ������� ������ �������
        action   = action   ,
        complete = math.huge, -- ����� ����������� �������
        period   = 1000     , -- ��������� ������� ������� �� ���������.
        type     = 'real'   , -- ��� ������� ('real', 'game', 'hud', 'cyclic')
        reset    = false    , -- ����� �� ������ �������� � �������� ��������� ����� ��������� ������
                              -- ������ �� ��������� � ��������� � ������ �������� ������� (methods:start)
        hud = { show = false,
                type = 'inc', -- ('inc', 'dec') ������� ����������� (increment) ��� ��������� (decrement)
                texture = "ui\\ui_mn_weapons",
                rect = {900,40,140,60},
                font = GetFontGraffiti32Russian(),
                color = {255,255,255,0},
            },
        cyclic   = false    , -- ����������� ������ ��� ���
        --remain   = number             , -- �����, ���������� �� ������������ (��� ����������)
        --pos      = number             , -- ����� � ������� (��� ���������� �� ������� ������������)
    }
    
    table.insert(timers, setmetatable(new_timer, {__index = methods})) -- � ������ ��� ����������� ����������
    timers[name] = new_timer -- ��� ����������� ������� �� �����
    _sort()
    return new_timer
end

_G.timer = setmetatable({}, mt_timer)

--table.print(timers)
--print(timer('asd'):name())
--print(timer.asd:name())
--print(timer.asd:time())
--[[
print(timer.tm_1.name)
timer.tm_1:start()]]

--//--------------------------------------------------------------------------------------------------------------
local  update_time = 0
-- ������
_G._update = function()        -- �� ������
    local timer = timers[1]
    local tm = timer and timer.data
    -- ���� ��� �� ������ ������� ��� ������ ���������� (��� �� �������)
    if not tm or tm.complete == math.huge then return end
    
    local current_time = time_global()
    if current_time < update_time then return end -- ��� �� ��������� ����� �������
    
    local add = tm.complete - current_time        -- ������� �������� �� ������������ �������
    
    local hud = tm.hud
    if hud.show then
        if hud.type == 'dec' then
            time_hud:SetText(math.round(add/1000))                 -- 'dec'
        elseif hud.type == 'inc' then
            time_hud:SetText(math.round(1 + (tm.time - add)/1000)) -- 'inc'
        end
    end
    
    
    add = add < tm.period and add or tm.period    -- ���� ������ ��� ������� �������, �� ��������� ������
    update_time = current_time + add              -- ���������� ��������� ����� �������
    
    if add <= 0 then                              -- ����� ������(�����) �����
        
        if tm.cyclic then                         -- ��������� ������������ ������� ����� �������������
            timer:stop():start()
        elseif tm.reset then                      -- ���� �������, "������������" ������
            timer:stop()
        else
            timer:kill()                          -- ���� �������
        end
        
        if tm.action then                         -- ���� ���� ��������, �� ���������
            tm.action()
        end
    end
end

timer('tm_1', 10, f_action_1)--[[:type('game')]]:hud(true)--[[:cyclic(true)]]:start()
--timer():name('asd'):time(5):action(f_action_1):type('game'):cyclic(true):start()--:stop()
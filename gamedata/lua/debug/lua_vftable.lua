if VERIFY_ARRAYS then
    -- log1('lua_vtables.lua succsesfully prefetched')
    local _concat, _foreach, _foreachi, _getn, _insert, _remove, _setn, _sort, _transpose, _ipairs = table.concat, table.foreach, table.foreachi, table.getn, table.insert, table.remove, table.setn, table.sort, table.transpose, ipairs
    table.concat = function (tab, ...)
        if tab[0] ~= nil then warning('lua array looks like c++ list, check it') end
        assert(select('#', unpack(tab)) >= table.size(tab), "select >= size")
        return _concat(tab, ...)
    end
    table.foreach = function (tab, ...)
        if tab[0] ~= nil then warning('lua array looks like c++ list, check it') end
        assert(select('#', unpack(tab)) >= table.size(tab), "select >= size")
        return _foreach(tab, ...)
    end
    table.foreachi = function (tab, ...)
        if tab[0] ~= nil then warning('lua array looks like c++ list, check it') end
        assert(select('#', unpack(tab)) >= table.size(tab), "select >= size")
        return _foreachi(tab, ...)
    end
    table.getn = function (tab, ...)
        if tab[0] ~= nil then warning('lua array looks like c++ list, check it') end
        assert(select('#', unpack(tab)) >= table.size(tab), "select >= size")
        return _getn(tab, ...)
    end
    table.insert = function (tab, ...)
        if tab[0] ~= nil then warning('lua array looks like c++ list, check it') end
        -- assert(select('#', unpack(tab)) >= table.size(tab), "select >= size") --// Karlan: ��� ��� ����� �������������� ��� ���������� ������������������ ������� (��. ��������)
        if select('#', unpack(tab)) < table.size(tab) then
            log1('======= PRINT INCORRECT TABLE : START =======')
            log1(debug.traceback([[~ possibly bad argument #1 to 'table.insert' => list expected]], 3)..'\n')
            for k,v in pairs(tab) do log1('['..tostring(k)..'] = '..tostring(v)..' | ['..type(k)..'] = '..type(v)) end
            log1('======= PRINT INCORRECT TABLE : FINISH ======')
        end
        return _insert(tab, ...)
    end
    table.remove = function (tab, ...)
        if tab[0] ~= nil then warning('lua array looks like c++ list, check it') end
        -- assert(select('#', unpack(tab)) >= table.size(tab), "select >= size") --// Karlan: ��� ��� ����� �������������� ��� �������� �� ������������������ ������� (��. ��������)
        if select('#', unpack(tab)) < table.size(tab) then
            log1('======= PRINT INCORRECT TABLE : START =======')
            log1(debug.traceback([[~ possibly bad argument #1 to 'table.remove' => list expected]], 3)..'\n')
            for k,v in pairs(tab) do log1('['..tostring(k)..'] = '..tostring(v)..' | ['..type(k)..'] = '..type(v)) end
            log1('======= PRINT INCORRECT TABLE : FINISH ======')
        end
        return _remove(tab, ...)
    end
    table.setn = function (tab, ...)
        if tab[0] ~= nil then warning('lua array looks like c++ list, check it') end
        assert(select('#', unpack(tab)) >= table.size(tab), "select >= size")
        return _setn(tab, ...)
    end
    table.sort = function (tab, ...)
        if tab[0] ~= nil then warning('lua array looks like c++ list, check it') end
        assert(select('#', unpack(tab)) >= table.size(tab), "select >= size")
        return _sort(tab, ...)
    end
    table.transpose = function (tab, ...)
        if tab[0] ~= nil then warning('lua array looks like c++ list, check it') end
        assert(select('#', unpack(tab)) >= table.size(tab), "select >= size")
        return _transpose(tab, ...)
    end
    ipairs = function(tab)
        if tab[0] ~= nil then warning('lua array looks like c++ list, check it') end
        assert(select('#', unpack(tab)) >= table.size(tab), "select >= size")
        return _ipairs(tab)
    end
    --// ����� ����� ��������� ���� ������� ��� ������������ ������ �� �����������
end
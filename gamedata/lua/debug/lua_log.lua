--[[ ----------------------------------------------------------------------------------------------
 File       : lua_log.lua
 Description: ������ ������ ���������� ����������
 Copyright  : 2017 � Prosectors Project
 Author     : Karlan
--]] ----------------------------------------------------------------------------------------------

module '_G'
--//-----------------------------------------------------------------------------------------------
--// Local functions
--//-----------------------------------------------------------------------------------------------
local flags = {all = 0, tab = 1, log = 2, printf = 3}
local extract_function      = function(str) return (str ~= nil and str ~= "" and str ~= "?") and str .. ">:" or "*unidentified func>:" end
local extract_detail_level  = function(str) return tonumber(str:match('%[D(%d?%d)]')) end
local extract_fixed_level   = function(str) return tonumber(str:match('%[T(%d?%d)]')) end
local extract_color         = function(str)
    local pt = '^%s*[~!@#$%%^&*%-+=/]?'
    local c = str:match(pt)
    if c then
        str = str:gsub(pt, "")
    end
    return c, str
end
local create_string         = function(verify_flag, info, str, add, ...)
    local dbg_verify = nil
    if verify_flag == flags.all then dbg_verify = false end
    if verify_flag ~= flags.all then dbg_verify = true end
    if dbg_verify and not PROSECTORS_DEBUG then return end
    local module_name = info.short_src:gsub('^.-([^\\]+)%.[^.]+$', '%1')
    if dbg_verify and not _G[module_name]._DEBUG_ then return end
    if dbg_verify and verify_flag == flags.printf and not _G[module_name]._DEBUG_GSC_ then return end
    local det, fdet = extract_detail_level(str), extract_fixed_level(str)
    if is_number(_G[module_name]._DEBUG_) and is_number(det) and _G[module_name]._DEBUG_ < det then return end
    if is_number(_G[module_name]._DEBUG_) and is_number(fdet) and _G[module_name]._DEBUG_ ~= fdet then return end
    local c, str = extract_color(str)
    str = c .. string.exformat(module_name .. "(" .. info.currentline .. "):<" .. extract_function(info.name) .. add .."\t" .. str, ...)
    return str
end
--//-----------------------------------------------------------------------------------------------
--// Global functions
--//-----------------------------------------------------------------------------------------------
t_print = function(t, ...) --// special function for table output in engine log 
    local str = create_string(flags.tab, ...)
    if not is_string(str) then return end
    log1(str)
    log1(table.tree(t))
end
log = function(fmt, ...) --// output in engine log by Prosectors scripts
    local str = create_string(flags.log, debug.getinfo(2), fmt, "", ...)
    if not is_string(str) then return end
    log1(str)
end
printf = function(fmt, ...) --// output in engine log by GSC scripts
    local str = create_string(flags.printf, debug.getinfo(2), fmt, "[GSC_dbg]:", ...)
    if not is_string(str) then return end
    log1(str)
end
logt    = function(fmt, ...) traceback  (create_string(flags.all, debug.getinfo(2), fmt, "", ...))      end
logf    = function(fmt, ...) log1       (create_string(flags.all, debug.getinfo(2), fmt, "", ...))      end
logc    = function(fmt, ...) log1       (extract_color(fmt) .. '=|  ' .. string.exformat(fmt,...))      end
logp    = function(fmt, ...) log1       (string.format(fmt,...))                                        end
warning = function(fmt, ...) error_warn (create_string(flags.all, debug.getinfo(2), fmt, "", ...))      end
abort   = function(fmt, ...) error      (create_string(flags.all, debug.getinfo(2), fmt, "", ...))      end
--//-----------------------------------------------------------------------------------------------
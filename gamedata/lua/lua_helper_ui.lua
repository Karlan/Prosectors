--[[ ----------------------------------------------------------------------------------------------
 File       : lua_helper_ui.lua
 Description: ��������������� ���������� ��� ��������� ������ �����������
 Copyright  : 2017 � Prosectors Project
 Author(s)  : Karlan, Malandrinus, Nazgool 
--]] ----------------------------------------------------------------------------------------------
module '_G'

function get_texture_info(id_name, id_default)
    if id_default == nil then id_default = id_name end

    local task_info = GetTextureInfo(id_name, id_default)
    local r = task_info:get_rect()

    r.x2 = r.x2 - r.x1
    r.y2 = r.y2 - r.y1
    return task_info:get_file_name(), r
end


--// ��������� ������� ������
function ui.is_widescreen()
	return (device().width/device().height>1024/768+0.01)
end

--// ��� ������� �������� �������� �� ����� �������
--// is_16x9 - ������������ ������ �� 16�9 (�������� 4�3)
function ui.get_current_kx(is_16x9)
	local aspect = device().height/device().width
	return aspect / (is_16x9 and 9/16 or 768/1024)
end	-- 0,75

function ui.screen_rect()
	return Frect():set(0,0,1024,768)
end

--// ����������� ���� � ���� ����� (�������)
--// offset - ���������� �� ����; min_max: true = ���������� �����������, false = ���������� ������������, nil = �������.
function ui.align_to_rect(wnd,rect,align,offset,min_max)
	local r		= Frect()
	wnd:GetAbsoluteRect(r)
	if align == 'l' or align == 't' or align == 'r' or align == 'b' then
		local cur	= align == 'l' and r.x1-rect.x1 or align == 't' and r.y1-rect.y1 or align == 'r' and rect.x2-r.x2 or align == 'b' and rect.y2-r.y2
		local pos	= nil
		if min_max == true then
			pos		= math.max(offset,cur)
		elseif min_max == false then
			pos		= math.min(offset,cur)
		else
			pos		= offset
		end
		local diff	= pos - cur
		if align == 'r' or align == 'b' then
			diff	= diff*-1
		end
		if align == 'l' or align == 'r' then
			wnd:SetWndPos(wnd:GetWndPos().x+diff,wnd:GetWndPos().y)
		else
			wnd:SetWndPos(wnd:GetWndPos().x,wnd:GetWndPos().y+diff)
		end
	elseif align == 'c' then	--// ���� � �����
		wnd:SetWndPos(((rect.x2-rect.x1)-(r.x2-r.x1))/2, ((rect.y2-rect.y1)-(r.y2-r.y1))/2)
	end
end

function ui.place_wnd_at_pos(wnd,pos,rect)
	local size	= vector2():set(wnd:GetWndSize())
	local pos	= vector2():set(size):div(-2):add(pos)
	if size.x > rect.x2-rect.x1 then
		pos.x	= rect.x1 + (rect.x2-rect.x1)/2 - size.x/2
	else
		pos.x	= math.max(pos.x,rect.x1)
		pos.x	= math.min(pos.x,rect.x2-size.x)
	end
	if size.y > rect.y2-rect.y1 then
		pos.y	= rect.y1 + (rect.y2-rect.y1)/2 - size.y/2
	else
		pos.y	= math.max(pos.y,rect.y1)
		pos.y	= math.min(pos.y,rect.y2-size.y)
	end
	wnd:SetWndPos(pos.x,pos.y)
end

function ui.get_cursor_placement()
	if get_cursor():IsVisible() then
		return get_cursor():GetPosition(), true
	elseif ui.is_crosshair_shown() then
		return ui.get_crosshair_pos()
	else
		return vector2():set(512,384)
	end
end

--// ��������� �������� ���
function ui.close_active_wnd()
    local wnd = level.main_input_receiver()
    if not wnd then return end
    level.start_stop_menu(wnd, false)
    ui.close_active_wnd() --// Karlan: ���� ���� ��������� ������������� ������
end

--// ������ � ������
colors = {
default    = "%c[default]",
gray       = "%c[255,160,160,160]",
--//-------------------------
red        = "%c[255,200,35,20]", --// loss item/money
coral      = "%c[255,255,80,80]", --// story quests
fire       = "%c[255,255,130,67]", --// fail task
lemon      = "%c[255,253,233,16]", --// get treasure
fern       = "%c[255,113,188,120]", --// complete task --// "%c[255,90,150,130]"
green      = "%c[255,60,180,60]", --// get item/money
blue       = "%c[255,80,210,220]", --// new/update task
purple     = "%c[255,247,84,225]", --// ex-fail task --// "%c[255,128,0,128]" --// "%c[255,230,0,230]"
orange     = "%c[255,255,165,0]", --// get recipe --// "%c[255,255,140,0]"
stateblue  = "%c[255,128,128,250]",
lightgreen = "%c[255,27,255,8]", --// "%c[255,0,223,8]"
--//-------------------------
answer     = "%c[255,135,183,116]",
caption    = "%c[255,238,153,26]",
text       = "%c[255,216,186,140]",
}

colors.tasks = {new = colors.blue, update = colors.blue, complete = colors.fern, fail = colors.fire, cancel = colors.gray}

--// spots data
--// Karlan->Team: ���� ����� ���� ���� ����������
map_spots = {
zone_big = 'crlc_big',
zone_mdl = 'crlc_mdl',
zone_small = 'crlc_small',
blue = 'blue_location',
green = 'green_location',
red = 'red_location',
hideout_big = 'hideout_big',
hideout_mdl = 'hideout_mdl',
hideout_small = 'hideout_small',
}

-- CUIScriptWnd Extension ----------------------------------------------------------------------------------------
local function connect_action(func, ...)
    local arg = {...}
    return  function (obj) func(obj, unpack(arg)) end
end

function CUIScriptWnd:AddCallback_A(name, event, func, obj, ...)
    self:AddCallback(name, event, connect_action(func, ...), obj)
end
---------------------------------------------------------------------------------


local visual_statics = {}
local function check_visual_statics()
	local ret = false
	for name,st in pairs(visual_statics) do
		for idx,pr in pairs(st[1]) do
			if not get_hud():GetCustomStatic(name..'_'..idx) then
				st[1][idx] = nil ; ret = true
			end
		end
	end
	return ret
end
local function sort_visual_statics(name)
	check_visual_statics()
	local st	= visual_statics[name]
	if table.size(st[1]) == 0 then
		st[2] = {};	return
	end
	local prt	= {}
	for idx,pr in pairs(st[1]) do
		table.insert(prt,{idx,pr})
	end
	table.sort(prt,function(a,b) return a[2] > b[2] end)
	for idx,t in pairs(prt) do
		local pos = st[2][idx]
		assert(pos ~= nil)
		get_hud():GetCustomStatic(name..'_'..t[1]):wnd():SetWndPos(pos.x,pos.y)
	end
end
function ui.add_visual_static(name,prior)
	local st				= visual_statics[name] or {{},{}}	--// [1] = added, [2] = ipos
	local idx				= 0
	local hud				= get_hud()
	for i = 1, 10 do
		if not hud:CustomStaticExist(name..'_'..i) then break end
		if not hud:GetCustomStatic(name..'_'..i) then
			idx = i; break;
		end
	end
--	logc("#add_visual_static [%s][%s] idx [%s]",name,prior,idx)
	assert					(idx > 0,name)
	local cs				= hud:AddCustomStatic(name..'_'..idx)
	st[1][idx]				= prior or 0
	if st[2][idx] == nil then	--// store initial positions
		st[2][idx]			= vector2():set(cs:wnd():GetWndPos())
	end
	visual_statics[name]	= st
	if table.size(st[1]) > 0 then
		sort_visual_statics	(name)
	end
	return					idx, cs:wnd()
end
function ui.remove_visual_static(name,idx)
	get_hud():RemoveCustomStatic(name..'_'..idx)
--	logc("#remove_visual_static [%s][%s] : [%s]",name,idx,visual_statics[name][1][idx])
	if visual_statics[name][1][idx] then
		visual_statics[name][1][idx] = nil
		sort_visual_statics(name)
	end
end

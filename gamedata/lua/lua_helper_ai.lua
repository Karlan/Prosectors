--[[ ----------------------------------------------------------------------------------------------
 File       : lua_helper_ai.lua
 Description: ������� � ����� ������ ��� ��������� ������
 Copyright  : 2017 � Prosectors Project
 Author(s)  : Karlan, Rulix
--]] ----------------------------------------------------------------------------------------------

--//-----------------------------------------------------------------
module '_G'
--//-----------------------------------------------------------------------------------------------
--// Variables
--//-----------------------------------------------------------------------------------------------

--//-------------------------
local _DEBUG_       = true
local _DEBUG_GSC_   = _DEBUG_ and false
local module_name   = 'lua_helper_ai'
--//-----------------------------------------------------------------
local log         = function(msg, ...) return _DEBUG_ and log1(string.exformat(module_name..': '..tostring(msg), ...)) end
local printf     = function(msg, ...) return _DEBUG_GSC_ and print(string.exformat(module_name..': '..tostring(msg), ...)) end
--//-----------------------------------------------------------------------------------------------
--// Global functions
--//-----------------------------------------------------------------------------------------------
--//---------------------------------------------
--// Logic functions
--//---------------------------------------------
--// �������� ��� ���������� � ������� � ��������, ������������ ����� ��������� ����� �������� � �������
--// ����� ��������� ���-�� ���-�� �������, ���� ����-������ �������, ��������, ��������� ������ ��� �� �������� ���� ������ ������ ������������
--// ����/��������/������ ��������/������ ����/������ �������/���-������ � ���-������ ������/������� ���������� ����� ����� ����� � �����-�� ������� ��� ��������
function action(obj, ...) 
    local act = entity_action()
    for _,v in ipairs{...} do act:set_action(v) end
    if obj then obj:command(act, false) end
    return entity_action(act)
end

--// �������� ����� ������ ���� 'scheme[1234567890]@bla_bla_123' � ���� 'scheme'
function get_scheme_by_section(section)
    -- return section:gsub('%d', ''):match('^([%w%p]+)@') --// Karlan: need test
    local scheme = string.gsub(section, "%d", "")
    local at, to = string.find(scheme, "@", 1, true)
    if at and to then scheme = string.sub(scheme, 1, at - 1) end
    return scheme
end

--// �������������� ������ � ���������� �������
function reset_action(obj, script_name)
    local buff = obj:get_script_name()
    if obj:get_script() then
       obj:script(false, buff)
    else
--		verify(not obj:action(),"not obj:action() (%s)",obj:name())
       obj:reset_action_queue()
    end
    obj:script(true, is_string(script_name) and script_name or buff)
end

--// object capture functional
function obj_captured(obj)
    return obj:get_script()
end

function obj_capture(obj, script_name, reset_actions)
    assert(reset_actions ~= nil, "mob_capture: reset_actions parameter's value is not specified")
    if reset_actions then
        reset_action(obj, script_name)
    else
        if not obj:get_script() then
            obj:script(true, script_name)
        end
    end
end

function obj_release(obj)
    if obj:get_script() then
        obj:script(false, obj:get_script_name())
    end
end

--// ���������� �������������� ���� �� �������
function stop_play_sound(obj)
    if obj:alive() then
        obj:set_sound_mask(-1)
        obj:set_sound_mask(0)
    end
end

local issue_combat_event = function(obj)
    local st = db.storage[obj:id()]
    if st.mob_combat then
        printf("mob_alife_control: issuing event combat_callback")
        xr_logic.issue_event(obj, st.mob_combat, "combat_callback")
    end
end

function mob_alife_control(obj, mode)
    --// ������� ������� ����� ����������� ��� alife
    if not obj:alive() then
        obj_release(obj)
        return true
    end
    --// �������, � ������� ���� ���� - ����� ������ �������� � alife
    local enemy = obj:get_enemy()
    if enemy then
        if not mode or (not mode.braindead and not mode.friendly and (not mode.actor_friendly or enemy:id () ~= actor_id) and (not mode.npc_friendly or enemy:id() == actor_id)) then
            issue_combat_event(obj)
            obj_release(obj)
            return true
        end
    end
    --// ��������� � alife ��������, ������� ����������
    local h = obj:get_monster_hit_info()
    if h.who and h.time ~= 0 then
        if mode.braindead then
            --// Braindead ������� �� ������ �� ��� ������� ���� ���� �� ����������
            return false
        end
        if h.who:id() == actor_id and mode.actor_friendly then
            --// ���� ���� �� ��� ��� �������� actor-��, �� ������ �� ������������ � ����
            mode.actor_friendly = false
        end
        if h.who:id() ~= actor_id and mode.npc_friendly then
            --// ���� ���� �� ��� ��� �������� �� actor-��, �� ������ �� ������������ � ����
            mode.npc_friendly = false
        end
        issue_combat_event(obj)
        obj_release(obj)
        return true
    end
    --// ��������� ������� �������� ��� ������
    return false
end
--// ������� ������� ������� �����
function get_scheme_signals(obj, scheme)
    local id = get_id(obj)
    local sig = db.storage[id][scheme].signals --// ��� ����� ���� ������, ���� �������� ��� �������������� ������ ����� (�� ������ � �������� ����� ������ ��� ������� � ���� �����������)
    if not is_table(sig) then
        sig = {}
        db.storage[id][scheme].signals = sig
    end
    return sig
end
function get_object_logic(obj,make_ini)
    local st = db.storage[get_id(obj)]
	if not st and obj.is_server_object then
		st = m_storage.read_logic(obj,make_ini)
	end
	if st then
		return st.ini, st.section_logic, st.active_section
	end
end
--// �������� �� ������ ��������� (�� �� ������, � �� ������, �� ���� ������� ��������� �������� ��������� ���-����), ��������� ���� �� ������� ��� false
function is_trader_mode(obj)
	--// TODO: logic flag
	local id = get_id(obj)
	local st = db.storage[id]
	--// Rulix: ����� ��� �� �����������, ����� � xr_logic �������� ������ ����� � ����� ����
--	if not st and obj.is_server_object then
--		st = m_storage.read_logic(obj,true)	������� ��������� ��������
--	end
    if st and st.section_logic and st.ini:value_exist(st.section_logic, "trade") then
        return true
    end
--	if not obj.is_server_object and obj:is_stalker() and obj:get_stalker():specific_character_read("trade") then
--		return true
--	end
	return false
end
--//---------------------------------------------
--// Movement functions
--//---------------------------------------------
--// ���������, ��������� �� stalker ����� � ������ path_point ���� patrol_path
function stalker_at_waypoint(stalker, patrol_path, path_point, point_info)
	if point_info and point_info.pp == true then	-- precise
		return stalker:patrol_position_reached(patrol_path:point(path_point))
	end
	local distance = stalker:position():distance_to_sqr(patrol_path:point(path_point))
	return distance <= 0.13
end

--// �������� � ��������� ����������� ���� �� ����������� � ��������
--// ���������� vertex_id, � ������� �������� ���������
function send_to_nearest_accessible_vertex(npc, v_id)
    npc:set_dest_level_vertex_id(v_id,true)
    return npc:dest_level_vertex_id()
end

--//---------------------------------------------
--// Point functions
--//---------------------------------------------
--// �������� ��������� ����� ���� � �������
function get_nearest_waypoint(obj, pathname, ptr, cnt)
    local pt_chosen = nil
    local min_dist = nil
    local dist
    for i = 0, cnt - 1 do
        dist = obj:position():distance_to(ptr:point(i))
        if not min_dist or dist < min_dist then
            min_dist = dist
            pt_chosen = i
        end
    end
    if not pt_chosen then
        abort("object '%s': path '%s': get_nearest_waypoint: unable to choose a nearest waypoint (path has no waypoints?)", obj:name(), pathname)
    end
    return pt_chosen
end

function get_path_data(pathname) --// �� ������ ������������
    local path = patrol(pathname)
    assert(path, 'There is no path [%s]', pathname)
    local res = {}
    res.pathname = pathname --// ��� �������������� ���������� �������� ������, �� ���� �� ����� �������, �� ������ �� �����
    for i=0, path:count()-1 do --// Karlan: ���� ��������, �������� ��� �������� ������
        local wp = {}
        wp.pos = path:point(i)
        wp.lv = path:level_vertex_id(i)
        wp.gv = path:game_vertex_id(i)
        wp.name = path:name(i)
        wp.index = path:index(wp.name) --// Karlan: �� ���� index �������� ����������, ��� ��� ���� ������������� ��������� ��� ������, �� �� ����� �� �����
        wp.data = parse_waypoint_data(pathname, path:flags(i), wp.name)
        wp.ln = sim:level_name(game_graph():vertex(wp.gv):level_id())
        res[i] = wp
    end
    --// Karlan->ALL: �� ����� ��������, ��� �������� ������ ��� #res-1 + �����
    return res
end

function get_rain_path(name)
	name = name..'_rain'
	if level.patrol_path_exists(name) then
--		local path = patrol(name)
--		if string.find(path:name(0),'rain') then
			return name
--		end
	end
end

function use_rain_path()
	if rain_override ~= nil then
		return rain_override
	end
	return level.rain_factor() > 0.225 or m_surge.surge_is_run()
end
--//---------------------------------------------
--// Gulag/Squad functions
--//---------------------------------------------
function change_team_sqaud_group(obj, team, squad, group)
    if is_game_object(obj) then
        obj:change_team(team, squad, group)
    else
        obj.team = team
        obj.squad = squad
        obj.group = group
    end
    --printf("_G:TSG: [%s][%s][%s]", tostring(se_obj.team), tostring(se_obj.squad), tostring(se_obj.group))
end

function get_object_squad(object)
    if object == nil then abort("You are trying to get squad_object from NIL object!!!") end
    local obj_id = nil
    if type(object.id) == "function" then
        obj_id = object:id()
    else
        obj_id = object.id
    end
    local se_obj = alife():object(obj_id)
    if se_obj and se_obj.group_id ~= 65535 then
        return alife():object(se_obj.group_id)
    end
    return nil
end
--//---------------------------------------------
--// Misc condition functions
--//---------------------------------------------
game_object.is_enemy = function(npc,object)
	if not (object:is_entity_alive() and npc:is_entity_alive()) then
		return false
	end
	if npc:relation(object) ~= game_object.enemy then
		return false
	end
	local stalker = npc:get_stalker()
	if stalker then
		return not stalker:is_tolerant(object)
	elseif npc:is_actor() then
		assert(not object:is_actor())
		return object:is_enemy(npc)
	end
	return true
end

function npc_in_cover(npc,stand)
	assert(npc:alive())
	local npc_id = npc:id()
	local stor = db.storage[npc_id]
	local st = stor.ai('in_cover')
	local be,de = npc:best_enemy(),stor.danger_flag
	if be and not be:alive() then
		log("!1[%s]best_enemy is not alive (%s)",npc:name(),be:name())
		st.evn = false
		return false
	end
	if (be and not ai_wounded.is_wounded(be) or de) then
		if stand and not npc:path_completed() then
			st.evn = false
			return false
		end
	else
		return true
	end
	local tg = time_global()
	if (st.wait or 0) > tg then
		return st.evn
	end
	local enemies,tt = {},{}
	if be and not ai_wounded.is_wounded(be) then
		enemies[1] = be
		tt[be:id()] = true
	else	-- � ���������� ����� ��������� ������
		if not be and de and npc:best_danger() then
			local bd = npc:best_danger()
			local dir = bd:position():sub(npc:position())
			if dir:magnitude() < (bd:type() == danger_object.grenade and 20 or 8) or 1-level.cover_in_direction(npc:level_vertex_id(),dir) < 0.3 then
				st.evn = false
				st.wait = tg+3000
				return false
			end
		end
		st.evn = true
		st.wait = tg+2000
		return true
	end
	for o in npc:memory_visible_objects() do
		local obj = o:object()
		if obj then
			local id = obj:id()
			if not tt[id] and obj:alive() and obj:is_enemy(npc) and not (obj:is_stalker() and ai_wounded.is_wounded(obj)) then
				table.insert(enemies,obj)
--				tt[id] = true
			end
		end
	end
--[[	for o in npc:memory_sound_objects() do
		local obj = o:object()
		if obj and obj:alive() and not tt[obj:id()] and obj:is_enemy(npc) and not (obj:is_stalker() and ai_wounded.is_wounded(obj)) then
			table.insert(enemies,obj)
		end
	end]]
	local npc_lvid = npc:level_vertex_id()
	local f = 28
	for i,enemy in ipairs(enemies) do
		if enemy:is_monster() then
			local dist = enemy:position():distance_to_sqr(npc:position())
			local ebe = enemy:get_enemy()
			if (dist < 12*12 and enemy:see(npc)) or (dist < 50*50 and ebe and ebe:id() == npc_id) then
				st.evn = false
				st.wait = tg+5000
				return false
			end
		else
			local dir = enemy:position():sub(npc:position())
			local dist = dir:magnitude()
			local seez = npc:see(enemy) and enemy:see(npc)	-- ��� �����, ��� ���� ��� �����
			local cover = level.cover_in_direction(npc_lvid,dir)
			if seez or dist < 7 or cover > 0.8 or dist*cover > dist-f*cover then		-- ��� ������ ����, ��� ������ ��������� ���������
				st.evn = false
				st.wait = tg+2500
				return false
			end
		end
	end
	st.evn = true
	st.wait = tg+400
	return true
end

function actor_aiming_at_me(npc,df)
	local aim_dir = device().cam_dir
	local my_dir = npc:center():sub(device().cam_pos)
	local aH,aP = aim_dir:getH(),aim_dir:getP()
	local fH,fP = my_dir:getH(),my_dir:getP()
	local f = 0.03+(df or 0.5)/my_dir:magnitude()
	if (aH > fH and aH-fH or fH-aH) < f and (aP > fP and aP-fP or fP-aP) < f*2 then
		return true
	end
end

function weapon_locked(npc)
	return not (npc:weapon_unstrapped() or npc:weapon_strapped())
end

local wpn_ready_state = {[CWeapon.eIdle] = true, [CWeapon.eFire] = true, [CWeapon.eFire2] = true}
function weapon_ready(npc)
	if not npc:weapon_unstrapped() then
		return false
	end
	local wpn = npc:active_item()
	wpn = wpn and wpn:get_weapon()
	return wpn and wpn_ready_state[wpn.state] == true or false
end

function get_current_weapon(npc)
	local awpn = npc:active_item()
	return awpn and awpn:is_weapon() and awpn or npc:best_weapon()
end

function visual_mouth(npc)
	local ini = npc:get_visual_ini()
	if ini then
		return cfg_get_bool(ini,'specifications','mouth',true)
	end
	return true
end
--//---------------------------------------------
--// Logic functions
--//---------------------------------------------
--// from xr_spawner.script
function check_spawn(sobj)
    local cond = xr_logic.cfg_get_condlist(sobj:spawn_ini(), "spawner", "cond", sobj)
    return xr_logic.pick_section_from_condlist(sobj, cond.condlist) --// Karlan: ������������� ������� ������, ����-�� ���� ���������� �������� � ��������� ����� � ������������� :)
end
--//---------------------------------------------
--// Custom data functions
--//---------------------------------------------
--// Karlan->Team: ����� ������ ����� ������ ��������� ����� �������� �����, �� ��� �����, ����� ������������ � ���������� ������ � �������� ��� ���� � ������-������ ������������� ����
--// todo: � ���, ���� ������ ������ ��� �����?
function add_smart_terrain(smrt, str)
    if not str then str = '' else str = str .. '\n\n'  end
	if is_string(smrt) then
		str = str .. '[smart_terrains]\n'.. smrt .. ' = true'
	else
		assert(is_table(smrt), [[bad argument #1 to 'add_smart_terrain' (string or table excepted, got %s)]], type(smrt))
		str = str .. '[smart_terrains]'
		for i,sm in ipairs(smrt) do
			str = str .. '\n'.. sm .. ' = true'
		end
	end
	return str
end
--//---------------------------------------------
--// Common data
--//---------------------------------------------
function cfg_get_mode(ini, section, npc)
    local alife = {}
    alife.actor_friendly  = cfg_get_bool(ini, section, "actor_friendly", npc, false)
    alife.npc_friendly    = cfg_get_bool(ini, section, "npc_friendly", npc, false)
    alife.friendly        = cfg_get_bool(ini, section, "friendly", npc, false)
    alife.braindead       = cfg_get_bool(ini, section, "braindead", npc, false)
    return alife
end
--//---------------------------------------------
--// 
--//---------------------------------------------
local gfb_fire = {}
game_object.fire_bone = function(obj,part)
	local sec = obj:section()
	if part == "head" then
		return obj:head_bone_name()
	elseif part == "torso" then
		return obj:torso_bone_name()
	end
	if not gfb_fire[sec] then
		gfb_fire[sec] = cfg_get_string(sini,sec,"bone_fire","bip01_head")
	end
	return gfb_fire[sec]
end
--//---------------------------------------------
--// ��������� ������� (from xr_actions_id.script)
--//---------------------------------------------
local id_start = stalker_ids.action_script
actions_id = {
	--// ����������
	alife					= stalker_ids.action_alife_planner,
	anim_play				= id_start + 2,
	acting					= id_start + 3,
	state_mgr				= id_start + 10,
	--// �����
	stohe_kamp_base			= id_start + 20,
	stohe_meet_base			= id_start + 30,
	stohe_camper_base		= id_start + 40,
	abuse_base				= id_start + 50,
	--// Sidorovich
	sidor_act_patrol		= id_start + 60,
	sidor_act_wounded_base	= id_start + 70,
	sidor_attendant_act		= id_start + 80,
	--// chugai
	chugai_heli_hunter_base	= id_start + 90,
	combat_zombied_base		= id_start + 100,
	combat_monolith_base	= id_start + 110,
	--// ����
	zmey_sleeper_base		= id_start + 120,
	zmey_walker_base		= id_start + 130,
	zmey_remark_base		= id_start + 140,

	escort_base				= id_start + 150,
	--// modules
	_pointer				= id_start + 500
}
--//---------------------------------------------
--// ��������� ����������� (from xr_evaluators_id.script)
--//---------------------------------------------
local id_start = stalker_ids.property_script
evaluators_id = {
	--// ����������
	script_combat			= id_start + 1,
	anim_play				= id_start + 2,
	acting					= id_start + 3,
	state_mgr				= id_start + 10,

	stohe_kamp_base			= id_start + 20,
	stohe_meet_base			= id_start + 30,
	stohe_camper_base		= id_start + 40,
	abuse_base				= id_start + 50,

	sidor_patrol_base		= id_start + 60, 
	sidor_wounded_base		= id_start + 70,
	sidor_attendant_eva		= id_start + 80,

	chugai_heli_hunter_base	= id_start + 90,
	combat_zombied_base		= id_start + 100,
	combat_monolith_base	= id_start + 110,

	zmey_sleeper_base		= id_start + 120,
	zmey_walker_base		= id_start + 130,
	zmey_remark_base		= id_start + 140,

	escort_base				= id_start + 150,
	--// modules
	_pointer				= id_start + 500
}
--// ����������� ����� ����
local id_meta = {}
function id_meta.__call(tbl,name,add_cp)
	assert(tbl[name] == nil)
	local id = tbl._pointer
	tbl._pointer = id+1
	tbl[name] = id
	if is_boolean(add_cp) then
		assert(tbl==evaluators_id)
		ai_common.add_common_property{id,add_cp}
	end
	return id
end
setmetatable(actions_id,	id_meta)
setmetatable(evaluators_id,	id_meta)

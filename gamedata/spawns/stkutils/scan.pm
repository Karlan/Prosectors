# Module for scanning stalker config files
# Update history:
#	29/08/2012 - changed priority of clsids.ini querys , implemented two-way config scan
#	26/08/2012 - fix for new fail() syntax, fall back to 'include scanning', added some extra debug check
#   01/06/2018 - replaced section_to_clsid to Prosectors edition (by Karlan)
######################################################
package stkutils::scan;
use strict;
use stkutils::debug qw(fail);
use stkutils::ini_file;
use stkutils::utils qw(get_includes get_path get_all_includes get_filelist);
use IO::File;
use constant section_to_clsid => {
 'actor' => 'O_ACTOR',
 'af_ameba_mica' => 'ARTEFACT',
 'af_ameba_slime' => 'ARTEFACT',
 'af_ameba_slug' => 'ARTEFACT',
 'af_blood' => 'ARTEFACT',
 'af_cristall' => 'ARTEFACT',
 'af_cristall_flower' => 'ARTEFACT',
 'af_drops' => 'ARTEFACT',
 'af_dummy_battery' => 'ARTEFACT',
 'af_dummy_dummy' => 'ARTEFACT',
 'af_dummy_glassbeads' => 'ARTEFACT',
 'af_dummy_pellicle' => 'ARTEFACT',
 'af_dummy_spring' => 'ARTEFACT',
 'af_electra_flash' => 'SCRPTART',
 'af_electra_moonlight' => 'SCRPTART',
 'af_electra_sparkler' => 'SCRPTART',
 'af_fireball' => 'ARTEFACT',
 'af_fuzz_kolobok' => 'ARTEFACT',
 'af_gold_fish' => 'ARTEFACT',
 'af_gravi' => 'ARTEFACT',
 'af_medusa' => 'ARTEFACT',
 'af_mincer_meat' => 'ARTEFACT',
 'af_night_star' => 'ARTEFACT',
 'af_rusty_kristall' => 'ARTEFACT',
 'af_rusty_sea-urchin' => 'ARTEFACT',
 'af_rusty_thorn' => 'ARTEFACT',
 'af_soul' => 'ARTEFACT',
 'af_vyvert' => 'ARTEFACT',
 'agr_bandit_respawn_1' => 'AI_STL_S',
 'agr_bandit_respawn_2' => 'AI_STL_S',
 'agr_soldier_regular' => 'AI_STL_S',
 'agr_soldier_veteran' => 'AI_STL_S',
 'agr_stalker_regular' => 'AI_STL_S',
 'agr_stalker_veteran' => 'AI_STL_S',
 'ammo_11.43x23_fmj' => 'AMMO',
 'ammo_11.43x23_hydro' => 'AMMO',
 'ammo_12x70_buck' => 'AMMO',
 'ammo_12x76_dart' => 'AMMO',
 'ammo_12x76_zhekan' => 'AMMO',
 'ammo_12x76_zhekan_heli' => 'AMMO',
 'ammo_5.45x39_ap' => 'AMMO',
 'ammo_5.45x39_fmj' => 'AMMO',
 'ammo_5.56x45_ap' => 'AMMO',
 'ammo_5.56x45_ss190' => 'AMMO',
 'ammo_7.62x54_7h1' => 'AMMO',
 'ammo_7.62x54_7h14' => 'AMMO',
 'ammo_pkm_100' => 'AMMO',
 'ammo_9x18_fmj' => 'AMMO',
 'ammo_9x18_pbp' => 'AMMO',
 'ammo_9x18_pmm' => 'AMMO',
 'ammo_9x19_fmj' => 'AMMO',
 'ammo_9x19_pbp' => 'AMMO',
 'ammo_9x39_ap' => 'AMMO',
 'ammo_9x39_pab9' => 'AMMO',
 'ammo_9x39_sp5' => 'AMMO',
 'ammo_gauss' => 'AMMO',
 'ammo_m209' => 'AMMO',
 'ammo_og-7b' => 'AMMO',
 'ammo_vog-25' => 'AMMO',
 'ammo_vog-25p' => 'AMMO',
 'antirad' => 'II_ANTIR',
 'repair_toolbox' => 'II_ATTCH',
 'repair_wpn_parts' => 'II_ATTCH',
 'repair_otf_parts' => 'II_ATTCH',
 'art_container_1' => 'II_ATTCH',
 'art_container_3' => 'II_ATTCH',
 'art_container_8' => 'II_ATTCH',
 'art_container_8_anom' => 'II_ATTCH',
 'backpack_ataka' => 'II_BACKP',
 'backpack_cyclop' => 'II_BACKP',
 'backpack_eagle' => 'II_BACKP',
 'backpack_haversack' => 'II_BACKP',
 'bad_psy_helmet' => 'II_ATTCH',
 'bandage' => 'II_BANDG',
 'bandit_outfit' => 'E_STLK',
 'bandit_armor_outfit' => 'E_STLK',
 'bandit_brown_cloak' => 'E_STLK',
 'bandit_black_cloak' => 'E_STLK',
 'bar_dolg_respawn_1' => 'AI_STL_S',
 'bar_dolg_respawn_2' => 'AI_STL_S',
 'bar_dolg_respawn_3' => 'AI_STL_S',
 'bar_ecolog_flash' => 'II_ATTCH',
 'bar_stalker_respawn_1' => 'AI_STL_S',
 'bar_stalker_respawn_2' => 'AI_STL_S',
 'bar_stalker_respawn_3' => 'AI_STL_S',
 'bar_stalker_respawn_4' => 'AI_STL_S',
 'bloodsucker_arena' => 'SM_BLOOD',
 'bloodsucker_fire' => 'SM_BLOOD',
 'bloodsucker_mil' => 'SM_BLOOD',
 'bloodsucker_normal' => 'SM_BLOOD',
 'bloodsucker_strong' => 'SM_BLOOD',
 'bloodsucker_weak' => 'SM_BLOOD',
 'boar_normal' => 'SM_BOARW',
 'boar_strong' => 'SM_BOARW',
 'boar_weak' => 'SM_BOARW',
 'boar_cop_normal' => 'SM_BOARW',
 'boar_cop_strong' => 'SM_BOARW',
 'bolt' => 'II_BOLT',
 'bread' => 'II_FOOD',
 'breakable_object' => 'O_BRKBL',
 'burer_arena' => 'SM_BURER',
 'burer_normal_indoor' => 'SM_BURER',
 'burer_normal_outdoor' => 'SM_BURER',
 'burer_strong_indoor' => 'SM_BURER',
 'burer_strong_outdoor' => 'SM_BURER',
 'burer_weak_indoor' => 'SM_BURER',
 'burer_weak_outdoor' => 'SM_BURER',
 'burer_karlan_weak' => 'SM_BURER',
 'burer_karlan_strong' => 'SM_BURER',
 'cash_1' => 'II_ATTCH',
 'cash_10' => 'II_ATTCH',
 'cash_100' => 'II_ATTCH',
 'cash_1000' => 'II_ATTCH',
 'cash_200' => 'II_ATTCH',
 'cash_25' => 'II_ATTCH',
 'cash_3' => 'II_ATTCH',
 'cash_5' => 'II_ATTCH',
 'cash_50' => 'II_ATTCH',
 'cash_500' => 'II_ATTCH',
 'cat_weak' => 'SM_CAT_S',
 'cat_normal' => 'SM_CAT_S',
 'chimera_weak' => 'SM_CHIMS',
 'dc_cave_documents' => 'II_ATTCH',
 'climable_object' => 'O_CLMBL',
 'conserva' => 'II_FOOD',
 'controller_tubeman' => 'SM_CONTR',
 'controller_x16' => 'SM_CONTR',
 'dar_document1' => 'II_ATTCH',
 'dar_document2' => 'II_ATTCH',
 'dar_document3' => 'II_ATTCH',
 'dar_document4' => 'II_ATTCH',
 'dar_document5' => 'II_ATTCH',
 'decoder' => 'II_ATTCH',
 'detector_advances' => 'D_SIMDET',
 'detector_elite' => 'D_SIMDET',
 'detector_simple' => 'D_SIMDET',
 'device_pda' => 'D_PDA',
 'device_torch' => 'TORCH_S',
 'dog_normal' => 'SM_DOG_S',
 'dog_strong' => 'SM_DOG_S',
 'dog_weak' => 'SM_DOG_S',
 'dog_bulterier' => 'SM_DOG_S',
 'dolg_outfit' => 'E_STLK',
 'dolg_scientific_outfit' => 'E_STLK',
 'door_lab_x8' => 'O_PHYS_S',
 'drug_anabiotic' => 'II_ANTIR',
 'drug_antidot' => 'II_ANTIR',
 'drug_booster' => 'II_ANTIR',
 'drug_coagulant' => 'II_BANDG',
 'drug_mezaton' => 'II_ANTIR',
 'drug_psy_blockade' => 'II_ANTIR',
 'drug_radioprotector' => 'II_ANTIR',
 'dynamite' => 'II_ATTCH',
 'ecolog_outfit' => 'E_STLK',
 'electro_chimera_weak' => 'SM_CHIMS',
 'energy_drink' => 'II_BOTTL',
 'esc_bandit_respawn_1' => 'AI_STL_S',
 'esc_bandit_respawn_2' => 'AI_STL_S',
 'esc_soldier_respawn_1' => 'AI_STL_S',
 'esc_soldier_respawn_specnaz' => 'AI_STL_S',
 'esc_stalker_respawn_1' => 'AI_STL_S',
 'esc_stalker_respawn_2' => 'AI_STL_S',
 'esc_wounded_flash' => 'II_ATTCH',
 'exo_outfit' => 'E_STLK',
 'explosive_barrel' => 'II_EXPLO',
 'explosive_barrel_low' => 'II_EXPLO',
 'explosive_dinamit' => 'II_EXPLO',
 'explosive_fuelcan' => 'II_EXPLO',
 'explosive_mobiltank' => 'II_EXPLO',
 'explosive_tank' => 'II_EXPLO',
 'filter_battle' => 'II_ATTCH',
 'filter_cartridge' => 'II_ATTCH',
 'filter_exo' => 'II_ATTCH',
 'filter_respirator' => 'II_ATTCH',
 'filter_tactic' => 'II_ATTCH',
 'fireball_acidic_zone' => 'ZS_TORRD',
 'fireball_electric_zone' => 'ZS_TORRD',
 'fireball_zone' => 'ZS_TORRD',
 'flesh_normal' => 'SM_FLESH',
 'flesh_strong' => 'SM_FLESH',
 'flesh_weak' => 'SM_FLESH',
 'fracture_weak' => 'SM_IZLOM',
 'gar_bandit_respawn_1' => 'AI_STL_S',
 'gar_bandit_respawn_2' => 'AI_STL_S',
 'gar_dolg_respawn_1' => 'AI_STL_S',
 'gar_dolg_respawn_2' => 'AI_STL_S',
 'gar_stalker_respawn_1' => 'AI_STL_S',
 'gar_stalker_respawn_2' => 'AI_STL_S',
 'gigant_normal' => 'SM_GIANT',
 'gigant_strong' => 'SM_GIANT',
 'graph_point' => 'AI_GRAPH',
 'grenade_f1' => 'G_F1',
 'grenade_f1_fake' => 'II_EXPLO',
 'grenade_gd-05' => 'G_F1',
 'grenade_gd-05_fake' => 'II_EXPLO',
 'grenade_rgd5' => 'G_F1',
 'grenade_rgd5_fake' => 'II_EXPLO',
 'guitar_a' => 'II_ATTCH',
 'gunslinger_flash' => 'II_ATTCH',
 'hand_radio' => 'II_ATTCH',
 'harmonica_a' => 'II_ATTCH',
 'helicopter' => 'C_HLCP_S',
 'helm_battle' => 'E_HLMET',
 'helm_hardhat' => 'E_HLMET',
 'helm_protective' => 'E_HLMET',
 'helm_respirator' => 'E_HLMET',
 'helm_tactic' => 'E_HLMET',
 'helmet' => 'E_HLMET',
 'hunters_toz' => 'WP_BM16',
 'inventory_box' => 'O_INVBOX',
 'killer_outfit' => 'E_STLK',
 'kolbasa' => 'II_FOOD',
 'kruglov_flash' => 'D_SIMDET',
 'lab_x16_documents' => 'II_ATTCH',
 'level_changer' => 'LE_CHG_S',
 'lights_hanging_lamp' => 'O_HLAMP',
 'm_barman' => 'AI_STL_S',
 'm_bloodsucker_e' => 'SM_BLOOD',
 'm_car' => 'SCRPTCAR',
 'vehicle_uaz' => 'SCRPTCAR',
 'm_controller_normal' => 'SM_CONTR',
 'm_controller_normal_fat' => 'SM_CONTR',
 'm_controller_old' => 'SM_CONTR',
 'm_controller_old_fat' => 'SM_CONTR',
 'm_crow' => 'AI_CROW',
 'm_flesh_e' => 'SM_FLESH',
 'm_osoznanie' => 'AI_STL_S',
 'm_phantom' => 'AI_PHANT',
 'm_poltergeist_normal' => 'SM_POLTR',
 'm_poltergeist_normal_flame' => 'SM_POLTR',
 'm_poltergeist_normal_tele' => 'SM_POLTR',
 'm_poltergeist_strong_flame' => 'SM_POLTR',
 'm_poltergeist_tele_outdoor' => 'SM_POLTR',
 'm_pseudodog_e' => 'SM_P_DOG',
 'm_rat_e' => 'AI_RAT',
 'm_trader' => 'AI_TRD_S',
 'm_lesnik' => 'AI_TRD_S',
 'matras' => 'SCRPTOBJ',
 'medkit' => 'II_MEDKI',
 'medkit_army' => 'II_MEDKI',
 'medkit_scientic' => 'II_MEDKI',
 'mil_freedom_barier_respawn_1' => 'AI_STL_S',
 'mil_freedom_respawn_1' => 'AI_STL_S',
 'mil_freedom_respawn_2' => 'AI_STL_S',
 'mil_freedom_respawn_3' => 'AI_STL_S',
 'mil_freedom_respawn_sniper' => 'AI_STL_S',
 'mil_killer_respawn_1' => 'AI_STL_S',
 'mil_killer_respawn_2' => 'AI_STL_S',
 'mil_killer_respawn_3' => 'AI_STL_S',
 'mil_killer_respawn_4' => 'AI_STL_S',
 'mil_monolit_rush_respawn_1' => 'AI_STL_S',
 'mil_neutral_barier_respawn_1' => 'AI_STL_S',
 'mil_stalker_respawn_1' => 'AI_STL_S',
 'mil_stalker_respawn_2' => 'AI_STL_S',
 'mil_stalker_respawn_3' => 'AI_STL_S',
 'mil_svoboda_leader_pda' => 'II_ATTCH',
 'military_outfit' => 'E_STLK',
 'mineral_water' => 'II_BOTTL',
 'monolit_outfit' => 'E_STLK',
 'monster_blaster' => 'G_F1',
 'mounted_weapon' => 'W_MOUNTD',
 'mutant_boar_leg' => 'II_ATTCH',
 'mutant_burer_hand' => 'II_ATTCH',
 'mutant_cat_tail' => 'II_ATTCH',
 'mutant_chimera_claw' => 'II_ATTCH',
 'mutant_controller_brain' => 'II_ATTCH',
 'mutant_controller_token' => 'II_ATTCH',
 'mutant_dog_tail' => 'II_ATTCH',
 'mutant_flesh_eye' => 'II_ATTCH',
 'mutant_fracture_pda' => 'II_ATTCH',
 'mutant_krovosos_jaw' => 'II_ATTCH',
 'mutant_poltergeist_eye' => 'II_ATTCH',
 'mutant_pseudogigant_hand' => 'II_ATTCH',
 'mutant_psevdodog_tail' => 'II_ATTCH',
 'mutant_snork_leg' => 'II_ATTCH',
 'mutant_tushkano_face' => 'II_ATTCH',
 'mutant_zombie_hand' => 'II_ATTCH',
 'novice_outfit' => 'E_STLK',
 'online_offline_group' => 'ON_OFF_S',
 'outfit_bandit_m1' => 'E_STLK',
 'outfit_dolg_m1' => 'E_STLK',
 'outfit_exo_m1' => 'E_STLK',
 'outfit_killer_m1' => 'E_STLK',
 'outfit_novice_m1' => 'E_STLK',
 'outfit_specnaz_m1' => 'E_STLK',
 'outfit_stalker_m1' => 'E_STLK',
 'outfit_stalker_m2' => 'E_STLK',
 'outfit_svoboda_m1' => 'E_STLK',
 'outfit_template' => 'E_STLK',
 'ph_skeleton_object' => 'P_SKELET',
 'physic_destroyable_object' => 'P_DSTRBL',
 'physic_door' => 'P_DSTRBL',
 'physic_object' => 'O_PHYS_S',
 'pri_decoder_documents' => 'II_ATTCH',
 'pri_monolith_respawn_1' => 'AI_STL_S',
 'pri_monolith_respawn_2' => 'AI_STL_S',
 'pri_monolith_respawn_3' => 'AI_STL_S',
 'pri_respawn_dolg' => 'AI_STL_S',
 'pri_respawn_freedom' => 'AI_STL_S',
 'pri_respawn_military' => 'AI_STL_S',
 'pri_respawn_neutral' => 'AI_STL_S',
 'protection_outfit' => 'E_STLK',
 'pseudodog_arena' => 'SM_P_DOG',
 'pseudodog_normal' => 'SM_P_DOG',
 'pseudodog_strong' => 'SM_P_DOG',
 'pseudodog_weak' => 'SM_P_DOG',
 'psy_dog' => 'SM_DOG_P',
 'psy_dog_phantom' => 'SM_DOG_F',
 'psy_dog_radar' => 'SM_DOG_P',
 'quest_case_01' => 'II_ATTCH',
 'quest_case_02' => 'II_ATTCH',
 'quest_case_03' => 'II_ATTCH',
 'rad_freedom_respawn_1' => 'AI_STL_S',
 'rad_freedom_respawn_2' => 'AI_STL_S',
 'rad_freedom_respawn_3' => 'AI_STL_S',
 'rad_monolith_respawn_1' => 'AI_STL_S',
 'rad_monolith_respawn_2' => 'AI_STL_S',
 'rad_monolith_respawn_3' => 'AI_STL_S',
 'rad_scientist_flash' => 'II_ATTCH',
 'rad_soldier_master' => 'AI_STL_S',
 'rad_specnaz_respawn_specnaz' => 'AI_STL_S',
 'rad_zombied_respawn_1' => 'AI_STL_S',
 'rad_zombied_respawn_2' => 'AI_STL_S',
 'rad_zombied_respawn_3' => 'AI_STL_S',
 'rat_group' => 'AI_RAT_G',
 'ref_container_8' => 'II_ATTCH',
 'respawn' => 'RE_SPAWN',
 'ros_bandit_respawn_3' => 'AI_STL_S',
 'ros_bandit_respawn_4' => 'AI_STL_S',
 'ros_killer_respawn_1' => 'AI_STL_S',
 'ros_killer_respawn_2' => 'AI_STL_S',
 'ros_killer_respawn_3' => 'AI_STL_S',
 'ros_killer_respawn_4' => 'AI_STL_S',
 'scientific_outfit' => 'E_STLK',
 'script_object' => 'SCRPTOBJ',
 'matras' => 'SCRPTOBJ',
 'script_zone' => 'SCRPT_ZN',
 'search_light' => 'O_SEARCH',
 'sim_faction' => 'SCRPT_ZN',
 'sleeping_bag' => 'II_ATTCH',
 'smart_terrain' => 'SMRTTRRN',
 'snork_arena' => 'SM_SNORK',
 'snork_cop' => 'SM_SNORK',
 'snork_indoor' => 'SM_SNORK',
 'snork_jumper' => 'SM_SNORK',
 'snork_normal' => 'SM_SNORK',
 'snork_outdoor' => 'SM_SNORK',
 'snork_strong' => 'SM_SNORK',
 'snork_weak' => 'SM_SNORK',
 'soldier_outfit' => 'E_STLK',
 'space_restrictor' => 'SPC_RS_S',
 'specops_outfit' => 'E_STLK',
 'spectator' => 'SPECT',
 'stalker' => 'AI_STL_S',
 'stalker_exoskeleton' => 'AI_STL_S',
 'stalker_fresh_zombied' => 'AI_STL_S',
 'stalker_monolith' => 'AI_STL_S',
 'stalker_monolith_exo' => 'AI_STL_S',
 'stalker_range_sniper' => 'AI_STL_S',
 'stalker_outfit' => 'E_STLK',
 'stalker_sakharov' => 'AI_STL_S',
 'stalker_trader' => 'AI_STL_S',
 'stalker_zombied' => 'AI_STL_S',
 'stalker_arena' => 'AI_STL_S',
 'stationary_mgun' => 'W_STMGUN',
 'svoboda_heavy_outfit' => 'E_STLK',
 'svoboda_light_outfit' => 'E_STLK',
 'cs_light_outfit' => 'E_STLK',
 'cs_heavy_outfit' => 'E_STLK',
 'cs_heavy2_outfit' => 'E_STLK',
 'tushkano_normal' => 'SM_TUSHK',
 'val_bandit_respawn_1' => 'AI_STL_S',
 'val_bandit_respawn_2' => 'AI_STL_S',
 'val_bandit_respawn_3' => 'AI_STL_S',
 'val_bandit_respawn_4' => 'AI_STL_S',
 'val_key_to_underground' => 'II_ATTCH',
 'val_soldier_respawn_1' => 'AI_STL_S',
 'vodka' => 'II_BOTTL',
 'drink_soda_glass' => 'II_BOTTL',
 'forester_radio' => 'II_ATTCH',
 'wpn_abakan' => 'WP_AK74',
 'wpn_abakan_m1' => 'WP_AK74',
 'wpn_abakan_m2' => 'WP_AK74',
 'wpn_addon_grenade_launcher' => 'W_GLAUNC',
 'wpn_addon_grenade_launcher_m203' => 'W_GLAUNC',
 'wpn_addon_scope' => 'WP_SCOPE',
 'wpn_addon_scope_detector' => 'WP_SCOPE',
 'wpn_addon_scope_dynamic_zoom' => 'WP_SCOPE',
 'wpn_addon_scope_night' => 'WP_SCOPE',
 'wpn_addon_scope_susat' => 'WP_SCOPE',
 'wpn_addon_scope_susat_custom' => 'WP_SCOPE',
 'wpn_addon_scope_susat_dusk' => 'WP_SCOPE',
 'wpn_addon_scope_susat_night' => 'WP_SCOPE',
 'wpn_addon_scope_susat_x16' => 'WP_SCOPE',
 'wpn_addon_scope_x27' => 'WP_SCOPE',
 'wpn_addon_silencer' => 'W_SILENC',
 'wpn_ak74' => 'WP_AK74',
 'wpn_ak74_m1' => 'WP_AK74',
 'wpn_ak74u' => 'WP_LR300',
 'wpn_ak74u_m1' => 'WP_LR300',
 'wpn_beretta' => 'WP_PM',
 'wpn_binoc' => 'WP_BINOC',
 'wpn_bm16' => 'WP_BM16',
 'wpn_bm16_full' => 'WP_BM16',
 'wpn_colt1911' => 'WP_PM',
 'wpn_colt_m1' => 'WP_PM',
 'wpn_desert_eagle' => 'WP_PM',
 'wpn_eagle_m1' => 'WP_PM',
 'wpn_fake_missile' => 'G_FAKE',
 'wpn_fake_missile1' => 'G_FAKE',
 'wpn_fake_missile2' => 'G_FAKE',
 'wpn_fn2000' => 'WP_GROZA',
 'wpn_fort' => 'WP_PM',
 'wpn_fort_m1' => 'WP_PM',
 'wpn_g36' => 'WP_AK74',
 'wpn_gauss' => 'WP_SVD',
 'wpn_groza' => 'WP_GROZA',
 'wpn_groza_m1' => 'WP_GROZA',
 'wpn_hpsa' => 'WP_HPSA',
 'wpn_knife' => 'WP_KNIFE',
 'wpn_l85' => 'WP_AK74',
 'wpn_l85_m1' => 'WP_AK74',
 'wpn_l85_m2' => 'WP_AK74',
 'wpn_lr300' => 'WP_AK74',
 'wpn_lr300_m1' => 'WP_AK74',
 'wpn_mp5' => 'WP_LR300',
 'wpn_mp5_m1' => 'WP_LR300',
 'wpn_mp5_m2' => 'WP_LR300',
 'wpn_pb' => 'WP_PM',
 'wpn_pm' => 'WP_PM',
 'wpn_rg-6' => 'WP_RG6',
 'wpn_rg6_m1' => 'WP_RG6',
 'wpn_rpg7' => 'WP_RPG7',
 'wpn_rpg7_missile' => 'G_RPG7',
 'wpn_sig220' => 'WP_PM',
 'wpn_sig550' => 'WP_AK74',
 'wpn_sig_m1' => 'WP_AK74',
 'wpn_sig_m2' => 'WP_AK74',
 'wpn_spas12' => 'WP_SHOTG',
 'wpn_spas12_m1' => 'WP_SHOTG',
 'wpn_svd' => 'WP_SVD',
 'wpn_svd_m1' => 'WP_SVD',
 'wpn_svu' => 'WP_SVU',
 'wpn_toz34' => 'WP_BM16',
 'wpn_toz34_premium' => 'WP_BM16',
 'wpn_toz34_mark4' => 'WP_BM16',
 'wpn_toz34_sawed' => 'WP_BM16',
 'wpn_usp' => 'WP_USP45',
 'wpn_val' => 'WP_VAL',
 'wpn_val_m1' => 'WP_VAL',
 'wpn_vintorez' => 'WP_VINT',
 'wpn_walther' => 'WP_WALTH',
 'wpn_walther_m1' => 'WP_WALTH',
 'wpn_wincheaster1300' => 'WP_SHOTG',
 'wpn_winchester_m1' => 'WP_SHOTG',
 'yan_ecolog_respawn_1' => 'AI_STL_S',
 'yan_zombied_respawn_1' => 'AI_STL_S',
 'yan_zombied_respawn_2' => 'AI_STL_S',
 'yan_zombied_respawn_3' => 'AI_STL_S',
 'zombie_ghost' => 'SM_ZOMBI',
 'zombie_immortal' => 'SM_ZOMBI',
 'zombie_normal' => 'SM_ZOMBI',
 'zombie_old' => 'SM_ZOMBI',
 'zombie_plague' => 'SM_ZOMBI',
 'zombie_strong' => 'SM_ZOMBI',
 'zombie_weak' => 'SM_ZOMBI',
 'zone_ameba' => 'ZS_AMEBA',
 'zone_plitka_1' => 'ZS_AMEBA',
 'zone_plitka_2' => 'ZS_AMEBA',
 'zone_plitka_3' => 'ZS_AMEBA',
 'zone_beton_lom_x18w' => 'ZS_AMEBA',
 'zone_beton_lom_x18w16' => 'ZS_AMEBA',
 'zone_5beton_lom' => 'ZS_AMEBA',
 'zone_zil130' => 'ZS_AMEBA',
 'zone_catcher' => 'ZS_AMEBA',
 'zone_liana' => 'ZS_AMEBA',
 'zone_liana_tree' => 'ZS_AMEBA',
 'zone_burning_fuzz' => 'ZS_HAIRS',
 'zone_burning_fuzz_average' => 'ZS_HAIRS',
 'zone_burning_fuzz_strong' => 'ZS_HAIRS',
 'zone_burning_fuzz_weak' => 'ZS_HAIRS',
 'zone_buzz' => 'ZS_MBALD',
 'zone_buzz_average' => 'ZS_MBALD',
 'zone_buzz_strong' => 'ZS_MBALD',
 'zone_buzz_weak' => 'ZS_MBALD',
 'zone_campfire_grill' => 'ZS_CAMPF',
 'zone_emi' => 'ZS_CAMPF',
 'zone_flame' => 'ZS_CAMPF',
 'zone_flame_light' => 'ZS_CAMPF',
 'zone_flame_small' => 'ZS_CAMPF',
 'zone_flame_small_static' => 'ZS_CAMPF',
 'zone_flame_small_static_nolight' => 'ZS_CAMPF',
 'zone_gravi_zone' => 'ZS_GALAN',
 'zone_gravi_zone_average' => 'ZS_GALAN',
 'zone_gravi_zone_killing' => 'ZS_GALAN',
 'zone_gravi_zone_strong' => 'ZS_GALAN',
 'zone_gravi_zone_weak' => 'ZS_GALAN',
 'zone_gravi_zone_weak_noart' => 'ZS_GALAN',
 'zone_mincer' => 'ZS_MINCE',
 'zone_mincer_average' => 'ZS_MINCE',
 'zone_mincer_strong' => 'ZS_MINCE',
 'zone_mincer_weak' => 'ZS_MINCE',
 'zone_mincer_weak_noart' => 'ZS_MINCE',
 'zone_mine_field' => 'ZS_MINEF',
 'zone_monolith' => 'ZS_RADIO',
 'zone_mosquito_bald' => 'ZS_MBALD',
 'zone_mosquito_bald_average' => 'ZS_MBALD',
 'zone_mosquito_bald_strong' => 'ZS_MBALD',
 'zone_mosquito_bald_strong_noart' => 'ZS_MBALD',
 'zone_mosquito_bald_weak' => 'ZS_MBALD',
 'zone_mosquito_bald_weak_noart' => 'ZS_MBALD',
 'zone_no_gravity' => 'ZS_NOGRA',
 'zone_radioactive' => 'ZS_RADIO',
 'zone_radioactive_average' => 'ZS_RADIO',
 'zone_radioactive_killing' => 'ZS_RADIO',
 'zone_radioactive_strong' => 'ZS_RADIO',
 'zone_radioactive_weak' => 'ZS_RADIO',
 'zone_teleport' => 'ZS_MBALD',
 'zone_teleport_small' => 'ZS_MBALD',
 'zone_thrasher' => 'ZS_GRAVI',
 'zone_thrasher_weak' => 'ZS_GRAVI',
 'zone_thrasher_strong' => 'ZS_GRAVI',
 'zone_witches_galantine' => 'ZS_GALAN',
 'zone_witches_galantine_average' => 'ZS_GALAN',
 'zone_witches_galantine_strong' => 'ZS_GALAN',
 'zone_witches_galantine_weak' => 'ZS_GALAN',
 'zone_zhar' => 'ZS_CAMPF',
 'zone_zharka_static' => 'ZS_MBALD',
 'zone_zharka_static_average' => 'ZS_MBALD',
 'zone_zharka_static_strong' => 'ZS_MBALD',
 'zone_zharka_static_weak' => 'ZS_MBALD',
 'zone_field_acidic' => 'ZS_RADIO',
 'zone_field_acidic_weak' => 'ZS_RADIO',
 'zone_field_acidic_average' => 'ZS_RADIO',
 'zone_field_acidic_strong' => 'ZS_RADIO',
 'zone_field_psychic' => 'ZS_RADIO',
 'zone_field_psychic_weak' => 'ZS_RADIO',
 'zone_field_psychic_average' => 'ZS_RADIO',
 'zone_field_psychic_strong' => 'ZS_RADIO',
 'zone_field_psychic_special' => 'ZS_RADIO',
 'zone_field_thermal' => 'ZS_RADIO',
 'zone_field_thermal_weak' => 'ZS_RADIO',
 'zone_field_thermal_average' => 'ZS_RADIO',
 'zone_field_thermal_strong' => 'ZS_RADIO',
};
use constant clsid_to_class => {
	# build 749
	O_ACTOR		=> 'cse_alife_creature_actor',				# deprecated from clear sky
	R_ACTOR		=> 'cse_alife_object_idol',					# deprecated from build 1098
	W_MGUN 		=> 'cse_alife_item_weapon_magazined',		# deprecated from build 1098
	W_RAIL		=> 'cse_alife_item_weapon_magazined',		# deprecated from build 1098
	W_ROCKET	=> 'cse_alife_item_weapon_magazined',		# deprecated from build 1098

	# build 788
	W_M134		=> 'cse_alife_item_weapon',					# deprecated from build 1834 [2004-04-09]

	# build 1098
	AI_HUMAN	=> 'se_stalker',							# deprecated from build 1114
	W_GROZA		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]
	W_M134en	=> 'cse_alife_item_weapon',					# deprecated from build 1229

	# build 1114-1096
	AI_CROW 	=> 'cse_alife_creature_crow',
	AI_HEN		=> 'unknown',								# deprecated from build 1229
	AI_RAT		=> 'se_monster',							# deprecated from call of pripyat
	AI_SOLD		=> 'se_stalker',							# deprecated from build 1475
	AI_ZOMBY	=> 'cse_alife_monster_zombie',				# deprecated from build 1254
	EVENT		=> 'not_used',								# not used
	W_AK74		=> 'cse_alife_item_weapon',					# deprecated from clear sky
	W_FN2000	=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]
	W_HPSA		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]
	W_LR300		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]

	#build 1154
	C_NIVA		=> 'cse_alife_car',							# deprecated from build 2205 [2005-04-15]
	O_DUMMY		=> 'cse_alife_object_dummy',				# deprecated from build 1510
	W_BINOC		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]
	W_FORT		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]
	W_PM		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]

	# build 1230
	O_HEALTH	=> 'unknown',								# deprecated from build 1893 [2004-09-06]

	# build 1254
	AF_MBALL	=> 'cse_alife_item_artefact',				# deprecated from build 1902
	AI_ZOM		=> 'cse_alife_monster_base',				# deprecated from build 2205 [2005-04-15]
	T_ASS		=> 'not_used',
	T_CS		=> 'not_used',
	T_CSBASE	=> 'cse_target_cs_base',
	T_CSCASK	=> 'cse_target_cs_cask',
	W_SHOTGN	=> 'cse_alife_item_weapon',					# deprecated from clear sky

	# build 1265
	SPECT		=> 'cse_spectator',

	# build 1465
	AI_RAT_G	=> 'cse_alife_rat_group',					# deprecated from call of pripyat
	AI_STL		=> 'cse_alife_human_stalker',				# deprecated from call of pripyat
	A_PM		=> 'cse_alife_item_ammo',					# deprecated from build 1472
	AI_CONTR	=> 'cse_alife_monster_base',				# deprecated from build 2205 [2005-04-15]
	AI_DOG	 	=> 'cse_alife_monster_base',				# deprecated from build 1510
	AI_GRAPH 	=> 'cse_alife_graph_point',
	AI_SOLDR	=> 'se_stalker',							# deprecated from build 1475
	AI_TRADE	=> 'cse_alife_trader',						# deprecated from build 2939
	AR_BDROP => 'cse_alife_item_artefact',					# deprecated from build 1610
	AR_GRAVI => 'cse_alife_item_artefact',					# deprecated from build 1610
	AR_MAGNT => 'cse_alife_item_artefact',					# deprecated from build 1610
	AR_MBALL => 'cse_alife_item_artefact',					# deprecated from build 1610
	AR_RADIO => 'cse_alife_item_artefact',					# deprecated from build 1610
	D_SIMDET => 'cse_alife_item_detector',
EQ_ASUIT => 'unknown',								# deprecated from build 1472
EQ_CNT => 'unknown',								# deprecated from build 1472
EQ_CNT_A => 'unknown',								# deprecated from build 1472
EQ_CNT_B => 'unknown',								# deprecated from build 1472
EQ_CPS => 'unknown',								# deprecated from build 1472
EQ_CPS_G => 'unknown',								# deprecated from build 1472
EQ_CSUIT => 'unknown',								# deprecated from build 1472
EQ_DTC => 'unknown',								# deprecated from build 1472
EQ_DTC_L => 'unknown',								# deprecated from build 1472
EQ_DTC_S => 'unknown', 								# deprecated from build 1472
EQ_DTC_U => 'unknown',								# deprecated from build 1472
EQ_LIFES => 'unknown',								# deprecated from build 1472
EQ_MKT => 'unknown',								# deprecated from build 1472
EQ_MKT_U => 'unknown',								# deprecated from build 1472
EQ_PSI_P => 'unknown',								# deprecated from build 1472
EQ_PSUIT => 'unknown',								# deprecated from build 1472
EQ_RADIO => 'unknown',								# deprecated from build 1472
EQ_TSUIT => 'unknown',								# deprecated from build 1472
	II_BOLT => 'cse_alife_item_bolt',
W_AK_CHR => 'unknown',								# deprecated from build 1472
W_FN_CHR => 'unknown',								# deprecated from build 1472
W_FR_CHR => 'unknown',								# deprecated from build 1472
W_HP_CHR => 'unknown',								# deprecated from build 1472
W_LR_CHR => 'unknown',								# deprecated from build 1472
W_PM_CHR => 'unknown',								# deprecated from build 1472
W_TZ_CHR => 'unknown',								# deprecated from build 1472
	W_SVD		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]
	W_SVU		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]
	Z_MBALD		=> 'cse_alife_anomalous_zone',
	Z_MINCER	=> 'cse_alife_anomalous_zone',				# not used from build 2571 [2006-03-16]

	# build 1469
	AI_IDOL		=> 'cse_alife_object_idol',					# deprecated from build 2945
	AMMO		=> 'cse_alife_item_ammo',					# deprecated from call of pripyat
	G_F1		=> 'cse_alife_item_grenade',				# deprecated from call of pripyat
	G_RGD5		=> 'cse_alife_item_grenade',				# deprecated from call of pripyat
	G_RPG7		=> 'cse_alife_item_ammo',
	O_HLAMP		=> 'cse_alife_object_hanging_lamp',			# deprecated from call of pripyat
	W_RPG7		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]

	# build 1472
	D_TORCH		=> 'cse_alife_item_torch',					# deprecated from build 2559 [2005-05-04]
	O_PHYSIC	=> 'cse_alife_object_physic',				# deprecated from build 2939
	W_USP45		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]
	W_VAL		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]
	W_VINT		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]
	W_WALTHR	=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]

	# build 1475
	LVLPOINT 	=> 'cse_alife_graph_point',					# deprecated from build 1510
	LVL_CHNG	=> 'se_level_changer',
	W_KNIFE		=> 'cse_alife_item_weapon',

	# build 1510
	AI_BLOOD	=> 'cse_alife_monster_base',				# deprecated from build 2205 [2005-04-15]
	AI_DOG_R 	=> 'se_monster',							# deprecated from clear sky
	AI_FLESH	=> 'cse_alife_monster_base',				# deprecated from build 2205 [2005-04-15]
	AI_FLE_G 	=> 'cse_alife_flesh_group',
	AI_HIMER	=> 'cse_alife_monster_base',				# deprecated from build 2205 [2005-04-15]
	AI_SPGRP	=> 'cse_alife_spawn_group',
	ARTEFACT	=> 'cse_alife_item_artefact',
	D_PDA		=> 'cse_alife_item_pda',
	G_FAKE		=> 'cse_alife_item_grenade',
	Z_ACIDF		=> 'cse_alife_anomalous_zone',				# deprecated from build 2218 [2005-01-28]
	Z_BFUZZ		=> 'cse_alife_zone_visual',					# deprecated from build 2559 [2005-05-04]
	Z_DEAD		=> 'cse_alife_anomalous_zone',				# deprecated from build 2218 [2005-01-28]
	Z_GALANT	=> 'cse_alife_anomalous_zone',				# deprecated from build 2559 [2005-05-04]
	Z_RADIO		=> 'cse_alife_anomalous_zone',
	Z_RUSTYH	=> 'cse_alife_anomalous_zone',				# deprecated from call of pripyat

	# build 1558
	AI_BOAR		=> 'cse_alife_monster_base',				# deprecated from build 2205 [2005-04-15]
	EQU_EXO		=> 'cse_alife_item_custom_outfit',			# deprecated from build 2571 [2006-03-16]
	EQU_MLTR	=> 'cse_alife_item_custom_outfit',			# deprecated from clear sky
	EQU_SCIE	=> 'cse_alife_item_custom_outfit',			# deprecated from build 2571 [2006-03-16]
	EQU_STLK	=> 'cse_alife_item_custom_outfit',			# deprecated from build 2559 [2005-05-04]
	II_ANTIR	=> 'cse_alife_item',						# deprecated from call of pripyat
	II_BREAD	=> 'cse_alife_item',						# deprecated from build 1828 [2004-02-03]
	II_DOC		=> 'cse_alife_item_document',
	II_MEDKI	=> 'cse_alife_item',						# deprecated from call of pripyat

	# build 1567
	AF_BDROP	=> 'cse_alife_item_artefact',				# deprecated from build 1902
	AF_NEEDL	=> 'cse_alife_item_artefact',				# deprecated from build 1902
	D_AFMERG	=> 'cse_alife_item',						# deprecated from build 2945
	SCRIPTZN	=> 'cse_alife_space_restrictor',

	# build 1610
	AF_BAST		=> 'cse_alife_item_artefact',				# deprecated from build 1902
	AF_BGRAV	=> 'cse_alife_item_artefact',				# deprecated from build 1902
	AF_DUMMY	=> 'cse_alife_item_artefact',				# deprecated from build 1902
	AF_EBALL 	=> 'cse_alife_item_artefact',				# deprecated from build 1902
	AF_FBALL	=> 'cse_alife_item_artefact',				# deprecated from build 1902
	AF_GALAN	=> 'cse_alife_item_artefact',				# deprecated from build 1902
	AF_RHAIR	=> 'cse_alife_item_artefact',				# deprecated from build 1902
	AF_THORN	=> 'cse_alife_item_artefact',				# deprecated from build 1902
	AF_ZUDA		=> 'cse_alife_item_artefact',				# deprecated from build 1902
	AI_DOG_B	=> 'cse_alife_monster_base',				# deprecated from build 2205 [2005-04-15]
	W_SCOPE		=> 'cse_alife_item',						# deprecated from build 2559 [2005-05-04]
	W_SILENC	=> 'cse_alife_item',						# deprecated from call of pripyat
	W_GLAUNC	=> 'cse_alife_item',						# deprecated from call of pripyat

	# build 1623
	SCRPTOBJ	=> 'cse_alife_dynamic_object_visual',

	# build 1851 [2004-01-26]
	O_SEARCH	=> 'cse_alife_object_projector',

	# build 1828 [2004-02-03]
	II_BOTTL	=> 'cse_alife_item',						# deprecated from call of pripyat
	II_FOOD		=> 'cse_alife_item',						# deprecated from call of pripyat

	# build 1844 [2004-02-19]
	C_HLCPTR	=> 'cse_alife_helicopter',					# deprecated from build 2939
	II_ATTCH	=> 'cse_alife_item',
	W_MOUNTD	=> 'cse_alife_mounted_weapon',

	# build 1834 [2004-04-09]
	II_EXPLO	=> 'cse_alife_item_explosive',				# deprecated from call of pripyat
	O_BRKBL		=> 'cse_alife_object_breakable',

	# build 1834 [2004-05-09]
	AI_BURER	=> 'cse_alife_monster_base',				# deprecated from build 2205 [2005-04-15]
	AI_GIANT	=> 'cse_alife_monster_base',				# deprecated from build 2205 [2005-04-15]
	Z_TEAMBS	=> 'cse_alife_team_base_zone',

	# build 1842 [2004-06-17]
	A_M209		=> 'cse_alife_item_ammo',					# deprecated from call of pripyat
	A_OG7B		=> 'cse_alife_item_ammo',					# deprecated from call of pripyat
	A_VOG25		=> 'cse_alife_item_ammo',					# deprecated from call of pripyat
	W_BM16		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]

	# build 1865 [2004-08-09]
	AI_PHANT	=> 'cse_alife_monster_base',
	NW_ATTCH	=> 'cse_alife_item',
	P_SKELET	=> 'cse_alife_ph_skeleton_object',
	Z_TORRID	=> 'cse_alife_torrid_zone',					# deprecated from call of pripyat

	# build 1893 [2004-09-06]
	AI_FRACT	=> 'cse_alife_monster_base',				# deprecated from build 2205 [2005-04-15]
	SPACE_RS	=> 'cse_alife_space_restrictor',			# deprecated from build 2939

	# build 1925
	AI_SNORK	=> 'cse_alife_monster_base',				# deprecated from build 2205 [2005-04-15]
	O_CLMBL		=> 'cse_alife_object_climable',

	# build 1935
	SMRTTRRN	=> 'se_smart_terrain',

	# build 1964
	AI_CAT		=> 'cse_alife_monster_base',				# deprecated from build 2205 [2005-04-15]
	II_BTTCH	=> 'cse_alife_item',

	# build 1971
	CLSID_Z_BFUZZ	=> 'cse_alife_anomalous_zone',			# deprecated from build 1994
	Z_AMEBA		=> 'cse_alife_zone_visual',					# deprecated from call of pripyat

	# build 2212 [2005-01-22]
	AI_STL_S	=> 'se_stalker',
	P_DSTRBL	=> 'cse_alife_object_physic',				# deprecated in call of pripyat
	O_SWITCH	=> 'unknown',								# deprecated from clear sky
	W_RG6		=> 'cse_alife_item_weapon',					# deprecated from build 2559 [2005-05-04]

	# build 2205 [2005-04-15]
	SM_BLOOD	=> 'se_monster',
	SM_BOARW	=> 'se_monster',
	SM_BURER	=> 'se_monster',
	SM_CAT_S	=> 'se_monster',
	SM_CHIMS	=> 'se_monster',
	SM_CONTR	=> 'se_monster',
	SM_FLESH	=> 'se_monster',
	SM_GIANT	=> 'se_monster',
	SM_IZLOM	=> 'se_monster',
	SM_POLTR	=> 'se_monster',
	SM_P_DOG	=> 'se_monster',
	SM_SNORK	=> 'se_monster',
	SM_TUSHK	=> 'se_monster',
	SM_ZOMBI	=> 'se_monster',
	SCRPTART	=> 'cse_alife_item_artefact',
	SCRPTCAR	=> 'cse_alife_car',

	# build 2217 [2005-07-27]
	W_STMGUN	=> 'cse_alife_stationary_mgun',

	# build 2571 [2006-03-16]
	II_BANDG	=> 'cse_alife_item',							# deprecated in call of pripyat
	ON_OFF_G	=> 'cse_alife_online_offline_group',			# deprecated from call of pripyat
	RE_SPAWN	=> 'se_respawn',								# deprecated in call of pripyat
	SM_DOG_F	=> 'se_monster',
	SM_DOG_P	=> 'se_monster',
	Z_NOGRAV	=> 'cse_alife_anomalous_zone',

	# build 2559 [2006-05-04]
	TORCH_S		=> 'cse_alife_item_torch',
	E_STLK		=> 'cse_alife_item_custom_outfit',
	WP_AK74		=> 'cse_alife_item_weapon_magazined_w_gl',
	WP_BINOC	=> 'cse_alife_item_weapon_magazined',
	WP_BM16		=> 'cse_alife_item_weapon_shotgun',
	WP_GROZA	=> 'cse_alife_item_weapon_magazined_w_gl',
	WP_HPSA		=> 'cse_alife_item_weapon_magazined',
	WP_KNIFE	=> 'cse_alife_item_weapon',
	WP_LR300	=> 'cse_alife_item_weapon_magazined',
	WP_PM		=> 'cse_alife_item_weapon_magazined',
	WP_RG6		=> 'cse_alife_item_weapon_shotgun',
	WP_RPG7		=> 'cse_alife_item_weapon_magazined',
	WP_SCOPE	=> 'cse_alife_item',
	WP_SHOTG	=> 'cse_alife_item_weapon_shotgun',				# deprecated in call of pripyat
	WP_SVD		=> 'cse_alife_item_weapon_magazined',
	WP_SVU		=> 'cse_alife_item_weapon_magazined',
	WP_USP45	=> 'cse_alife_item_weapon_magazined',			# deprecated in call of pripyat
	WP_VAL		=> 'cse_alife_item_weapon_magazined',
	WP_VINT		=> 'cse_alife_item_weapon_magazined',			# deprecated in clear sky
	WP_WALTH	=> 'cse_alife_item_weapon_magazined',			# deprecated in clear sky
	ZS_BFUZZ	=> 'se_zone_visual',
	ZS_GALAN	=> 'se_zone_anom',
	ZS_MBALD	=> 'se_zone_anom',
	ZS_MINCE	=> 'se_zone_anom',
	Z_ZONE		=> 'cse_alife_anomalous_zone',					# deprecated in call of pripyat

	# build 2588
	O_INVBOX	=> 'cse_alife_inventory_box',					# deprecated in call of pripyat

	# build 2939
	C_HLCP_S	=> 'cse_alife_helicopter',
	O_PHYS_S	=> 'cse_alife_object_physic',
	SPC_RS_S	=> 'cse_alife_space_restrictor',
	AI_TRD_S	=> 'cse_alife_trader',
	AI_TRADE_S	=> 'cse_alife_trader',

	# build 3120
	SFACTION	=> 'se_sim_faction',							# deprecated in call of pripyat
	Z_CFIRE		=> 'cse_alife_anomalous_zone',

	# clear sky
	D_ADVANC	=> 'cse_alife_item_detector',					# deprecated in call of pripyat
	D_ELITE		=> 'cse_alife_item_detector',					# deprecated in call of pripyat
	D_FLARE		=> 'unknown',
	SMRT_C_S	=> 'se_smart_cover',
	SM_DOG_S	=> 'se_monster',
	S_ACTOR		=> 'se_actor',

	# call of pripyat
	AMMO_S		=> 'cse_alife_item_ammo',
	DET_ADVA	=> 'cse_alife_item_detector',
	DET_ELIT	=> 'cse_alife_item_detector',
	DET_SCIE	=> 'cse_alife_item_detector',
	DET_SIMP	=> 'cse_alife_item_detector',
	E_HLMET		=> 'cse_alife_item_helmet',
	G_F1_S		=> 'cse_alife_item_grenade',
	G_RGD5_S	=> 'cse_alife_item_grenade',
	O_DSTR_S	=> 'cse_alife_object_physic',
	ON_OFF_S	=> 'sim_squad_scripted',
	SO_HLAMP	=> 'cse_alife_object_hanging_lamp',
	S_ANTIR		=> 'cse_alife_item',
	S_BANDG		=> 'cse_alife_item',
	S_BOTTL		=> 'cse_alife_item',
	S_EXPLO		=> 'cse_alife_item_explosive',
	S_FOOD		=> 'cse_alife_item',
	S_INVBOX	=> 'cse_alife_inventory_box',
	S_MEDKI		=> 'cse_alife_item',
	S_M209		=> 'cse_alife_item_ammo',
	S_OG7B		=> 'cse_alife_item_ammo',
	S_PDA		=> 'cse_alife_item_pda',
	S_VOG25		=> 'cse_alife_item_ammo',
	WP_ASHTG	=> 'cse_alife_item_weapon_shotgun',
	WP_GLAUN	=> 'cse_alife_item',
	WP_SILEN	=> 'cse_alife_item',
	ZS_RADIO	=> 'se_zone_anom',
	ZS_TORRD	=> 'se_zone_torrid',

	# Prosectors
	II_BACKP	=> 'cse_alife_item_backpack',
    LE_CHG_S    => 'se_level_changer',
    ZS_CAMPF    => 'cse_alife_anomalous_zone',
    ZS_NOGRA    => 'se_zone_anom',
    ZS_TOXIC    => 'se_zone_anom',
    ZS_MINEF    => 'cse_alife_anomalous_zone',
    ZS_HAIRS	=> 'se_zone_visual',
    ZS_GRAVI	=> 'se_zone_anom',
    SCRPT_ZN	=> 'cse_alife_space_restrictor',
	ZS_AMEBA	=> 'se_zone_anom',
};
sub launch {
	print "scanning configs...";
	my $stalker_path = $_[1];
	my $s_to_cl = IO::File->new('sections.ini', 'w') or fail("$!: sections.ini\n");
	print $s_to_cl "[sections]\n";
	my $clsids_ini = stkutils::ini_file->new('clsids.ini', 'r');
	my %engine_hash = ();
	my $obj = {};
	$obj->{sections_hash} = ();
	$obj->{sections_list} = [];
	my %table_hash = ();
	if (defined $stalker_path) {
		# scanning configs
		scan_system($stalker_path, $obj, $_[2]);
		foreach my $section (@{$obj->{sections_list}}) {
			delete ($obj->{sections_hash}{$section}) and next if $section =~ /^mp_/;
#			print "$section\n";
			my $sect = $section;
			my $parent_id = 0;
			while (1) {
				if (defined $obj->{sections_hash}{$section}{class}) {
					# if exists class or we alredy get some parent class
					$obj->{sections_hash}{$sect}{class} = $obj->{sections_hash}{$section}{class} if !exists($obj->{sections_hash}{$sect}{class});
#					print "	$obj->{sections_hash}{$sect}{class}\n";
					last;
				} elsif ($#{$obj->{sections_hash}{$section}{parent}} != -1)  {
#					print "	$obj->{sections_hash}{$section}{parent}[$parent_id]\n";
					# if no class, but parent exists, get class from parent
					if (defined $obj->{sections_hash}{$obj->{sections_hash}{$section}{parent}[$parent_id]}) {
						$section = $obj->{sections_hash}{$section}{parent}[$parent_id];
						next;
					} else {delete($obj->{sections_hash}{$sect}) and last;}
					# if no class, but parent exists in section_to_clsid hash, get class from parent through hash
					my @clsids = get_clsid($obj->{sections_hash}{$section}{parent});
					if ($#clsids != -1) {
						fail ("section $section has two or more parent sections with defined class\n") if $#clsids != 0;
						$obj->{sections_hash}{$sect}{class} = $clsids[0];
						last;
					}
				} else {
					# delete section if no class and no parent
					delete($obj->{sections_hash}{$sect}) and last;
				}
			}
		}
		# output
		my %result;
#			my %r1;
#			my $fhd = IO::File->new('debug.ini', 'w');
#			foreach my $section (%{$obj->{sections_hash}}) {
#				next if !defined $obj->{sections_hash}{$section}{class};
#				$r1{$obj->{sections_hash}{$section}{class}} = $section;
#			}
#			foreach my $class (sort {$a cmp $b} keys %r1) {
#				print $fhd "$class = $r1{$class}\n";
#			}
#			$fhd->close();
		foreach my $section (%{$obj->{sections_hash}}) {
			next if !defined $obj->{sections_hash}{$section}{class};
			my $cse_class;
			$cse_class = $clsids_ini->value('clsids', $obj->{sections_hash}{$section}{class}) if defined $clsids_ini;
			$cse_class = clsid_to_class->{$obj->{sections_hash}{$section}{class}} if !defined $cse_class;
			$result{$section} = $cse_class;
			$result{$section} = $obj->{sections_hash}{$section}{class} unless defined $cse_class;
		}
		foreach my $section (sort {$result{$a} cmp $result{$b}} keys %result) {
			my $lcSection = lc($section);
			print $s_to_cl "'$lcSection' = $result{$section}\n";
		}
	} else {
		die usage();
	}
	$s_to_cl->close();
	$clsids_ini->close() if defined $clsids_ini;
	print "done!\n";
}
sub get_clsid {
	my @temp;
	foreach (@{$_[0]}) {
		my $clsid = section_to_clsid->{$_};
		if ($clsid) {push @temp, $clsid}
	}
	return @temp;
}
sub get_class {
	my $clsid = section_to_clsid->{$_[1]};
	fail('cannot find clsid for class '.$_[1]) unless defined $clsid;
	my $class = clsid_to_class->{$clsid};
	fail('cannot find class for clsid '.$clsid) unless defined $class;
	return $class;
}
sub scan_system {
	my ($stalker_path, $obj, $idx) = @_;
	my $files = get_all_includes($stalker_path, 'system.ltx');
	my $flag;
	foreach my $l (@$files) {
		$l = $stalker_path.'\\'.$l;
	}
TRY:
	$flag = 0;
	foreach my $file (@$files) {
		$file =~ s/\/\//\// if defined $idx;
		next if defined $idx && ($idx =~ /$file/);
		next if $file =~ /environment_1/;
		my $system = read_ini($file);
		if (!defined $system) {
			$flag++;
			last;
		}
		push @{$obj->{sections_list}}, @{$system->{sections_list}};
		foreach my $section (keys %{$system->{sections_hash}}) {
			$obj->{sections_hash}{$section}{class} = $system->{sections_hash}{$section}{class} if defined $system->{sections_hash}{$section}{class};
			push @{$obj->{sections_hash}{$section}{parent}}, @{$system->{sections_hash}{$section}{parent}} if defined $system->{sections_hash}{$section}{parent};
		}
	}
	return if $flag == 0;
	print "\nproblems occured while scanning configs. Try again...\n";
	$files = get_filelist($stalker_path, 'ltx');
	goto TRY;
}
sub read_ini {
	my $fh = IO::File->new($_[0], 'r') or return undef;
	my $self = {};
	$self->{fh} = $fh;
	$self->{sections_list} = [];
	$self->{sections_hash} = ();

	my $section;
	while (<$fh>) {
		$_ =~ qr/^\s*;/ and next;
		if (/^\s*\[(.*)\]\s*:\s*(\w.*)?/) {
			$section = $1;
			fail('duplicate section found while reading '.$_[0]) if defined $self->{sections_hash}->{$section};
			push @{$self->{sections_list}}, $section;
			my %tmp = ();
			$self->{sections_hash}{$section} = \%tmp;
			@{$self->{sections_hash}{$section}{parent}}= ();
			my $parent = $2;
			if ((defined $parent) && ($parent =~ /^([A-Za-z_0-9\.\-@]+)/)) {
				push @{$self->{sections_hash}{$section}{parent}}, $1;
			}
			next;
		} elsif (/^\[(.*)\]\s*:*\s*(\w.*)?/) {
			$section = $1;
			fail('duplicate section '.$section.' found while reading '.$_[0]) if (defined $self->{sections_hash}->{$section} && ($section ne 'postprocess_base'));
			push @{$self->{sections_list}}, $section;
			my %tmp = ();
			$self->{sections_hash}{$section} = \%tmp;
			next;
		}
		if (/^\s*(class)\s*=\s*(\w+)\s*;*/) {
			my ($name, $value) = ($1, $2);
			next unless defined $section;
			if ($value =~ /^\W+(\w+)\W+/) {
				$value = $1;
			}
			if ($name =~ /^\W+(\w+)\W+/) {
				$self->{sections_hash}{$section}{$1} = $value;
			} else {
				$self->{sections_hash}{$section}{$name} = $value;
			}
		}
	}
	$fh->close();
	return $self;
}
1;
#######################################################################
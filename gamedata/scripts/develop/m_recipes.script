--[[ ----------------------------------------------------------------------------------------------
 File       : m_recipes.script
 Description: Artefact transmutation
 Copyright  : 2021 � Prosectors Project
 Author(s)  : Karlan
--]] ----------------------------------------------------------------------------------------------
--//-----------------------------------------------------------------
--// Flags for enable output of the debug information
--//-----------------------------------------------------------------
_DEBUG_ = true --// file under debugging?
_DEBUG_GSC_ = _DEBUG_ and true --// only for scripts by GSC
--//-----------------------------------------------------------------
--//-----------------------------------------------------------------------------------------------
--// Variables
--//-----------------------------------------------------------------------------------------------
local loaded = false
--//--------------------
local skill_green, skill_green_max = 0, 100
local skill_steps = {20, 40, 60, 100}
local recipes_data = {}
local absorption = {}
local ingredients = {}
local text_cache = {}
local type2name = {
	[21]		= "Studen",
	[34]		= "Catcher",
	[25]		= "Liana",
	[24]		= "Fuzz",
	[32]		= "Ameba",
	[10]		= "Bald",
	[15]		= "Lift",
	[22]		= "Electra",
	[12]		= "Meat",
	[11]		= "Gravi",
	[13]		= "Thrasher",
	[23]		= "Zharka",
}

local stored_anom_data = {}
local stored_changable_arts = {}
--//-----------------------------------------------------------------------------------------------
--// Local functions
--//-----------------------------------------------------------------------------------------------
--//-----------------------------------------------------------------
--// 
--//-----------------------------------------------------------------
local function fill_recipes_data()
	local ini = ini_cache("misc\\artefacts\\recipes.ltx")
	assert(ini:section_exist('list'), [[There is no section [list] in recipes.ltx]])
	local id, result = '',''
    for i=0, ini:line_count('list')-1 do
        result, id   = ini:r_line('list',i,'','')
        assert(ini:section_exist(id), [[There is no section [%s] in recipes.ltx]], id)
        local tt = {}
        tt.name             = id
        -- tt.anomaly_class    = ini:r_clsid(id, 'anomaly_class')
        tt.anomaly_type		= ini:r_u32(id, 'anomaly_type')
        tt.ingredients      = parse_names(ini:r_string(id, 'ingredients'))
        tt.time             = parse_names(ini:r_string(id, 'time'), true)
        tt.result           = ini:r_string(id, 'result')
        tt.rejection_prob   = ini:r_u32(id, 'rejection_prob')
        tt.degeneration_prob= ini:r_u32(id, 'degeneration_prob')
		if not is_table(recipes_data[tt.anomaly_type]) then recipes_data[tt.anomaly_type] = {} end
        table.insert(recipes_data[tt.anomaly_type], tt)
		assert(not recipes_data[tt.name])
		recipes_data[tt.name] = tt
    end
--	table.print(recipes_data, 'recipes_data')
end
local function show_hint(text)
	get_hud():AddCustomStatic("karlan_info",true,text)
end
local function change_condition(art, art_id, min_cond, max_cond)
	local rnd = math.random(min_cond, max_cond)
	art:change_condition(rnd/100)
	if rnd > min_cond+((max_cond-min_cond)/2) then
		m_arts.change_stor_take_ids(art_id)
	end
end
local function effect(art_id, anom_id, change, min_cond, max_cond)
	local art = level.object_by_id(art_id)
	if not art then
		stored_changable_arts[art_id] = change
		log('art in offline -> stored [%s] and exit', change)
		return
	end
	local ps = art:get_physics_shell()
	if ps then
		local anom = level.object_by_id(anom_id)
		local rot = anom:get_orientation():getRotation()
		local f = vector():setHP(rot.yaw,rot.pitch+math.pi/2):normalize_safe()
		local ang = math.rad(math.random(0, 359))
		f = f:add(vector():set(math.cos(ang), 0, math.sin(ang)))
		ps:apply_force(f,9000)
	end
	art:get_artefact():Blink(true, true, 1000)
	if is_number(min_cond) and is_number(max_cond) then
		change_condition(art, art_id, min_cond, max_cond)
	else
		local set_params = {}
		for k,v in pairs(m_arts.get_art_params(art)) do
			if k:find('immunity') then
				set_params[k] = v-(math.abs(1-v)*(skill_green/skill_green_max))
			else
				if k == 'radiation_restore_speed' or k == 'psy_health_restore_speed' or k == 'thirst_restore_speed' then
					set_params[k] = v>0 and v-(v*(skill_green/skill_green_max)) or v+(v*(skill_green/skill_green_max))
				else
					set_params[k] = v>0 and v+(v*(skill_green/skill_green_max)) or v-(v*(skill_green/skill_green_max))
				end
			end
		end
		m_arts.set_art_params(art, set_params)
	end
end
--//-----------------------------------------------------------------
--// Callbacks
--//-----------------------------------------------------------------
local function generate_transmute(e, key, i)
	local timer_data = {}
	timer_data.ingredients = ingredients[e.obj_id].ids
	local rnd = math.random(0, 100)
	local rejection_prob = recipes_data[key][i].rejection_prob+recipes_data[key][i].degeneration_prob
	local transmutation_prob = 100 - rejection_prob
	if rnd > transmutation_prob then
		local action = math.max(recipes_data[key][i].rejection_prob,recipes_data[key][i].degeneration_prob)
		if rnd > (transmutation_prob+action) then
			if action == recipes_data[key][i].rejection_prob then
				timer_data.failed = true
			else
			end
		else
			if action == recipes_data[key][i].rejection_prob then
			else
				timer_data.failed = true
			end
		end
	else
		timer_data.result = recipes_data[key][i].result
	end
	log('rnd [%s], tp [%s], timer_data[%s]', rnd, transmutation_prob, timer_data)
--	table.print(timer_data, 'timer_data')
	stored_anom_data[e.obj_id] = timer_data
	absorption[e.obj_id] = nil
	ingredients[e.obj_id] = nil
	--// ��� failed ������ ��������� �� �����������, ������ ���� ����������
	anom_transmut_timer("pp_anom_transmut_timer"..tostring(e.obj_id)):set_gdelayDHMS(0,0,math.random(recipes_data[key][i].time[1], recipes_data[key][i].time[2]),0):start()
	show_hint('anom_transmut_start')
	log('start transmut by recipe [%s]', recipes_data[key][i].name)
end

local function anomaly_update(e)
	if m_timers.timer_exists("pp_anom_transmut_timer"..tostring(e.obj_id)) then return end
    if is_table(absorption[e.obj_id]) and table.size(absorption[e.obj_id]) then
        for i=1, #absorption[e.obj_id] do
            if time_global() > absorption[e.obj_id][i].life then
				absorption[e.obj_id][i].art.in_transmut_time = 0
				absorption[e.obj_id][i].art:SwitchLights(true)
				absorption[e.obj_id][i].art:SwitchAfParticles(true)
				absorption[e.obj_id][i].art:StopVisibleSound()
				-- ph_release_fix_by_bone(obj, 'joint2')
                table.remove(absorption[e.obj_id], i)
                table.remove(ingredients[e.obj_id].sections, i)
                table.remove(ingredients[e.obj_id].ids, i)
--				table.print(absorption, 'absorption anomaly_update')
--				table.print(ingredients, 'ingredients anomaly_update')
				return
            end
        end
		local key = e.obj:get_anomaly().anomaly_type
--		table.print(ingredients[e.obj_id].sections, 'ingredients[%s].sections', e.obj_id)
--		table.print(recipes_data[key], 'recipes_data[%s]', key)
		if table.size(ingredients[e.obj_id].sections) and is_table(recipes_data[key]) and table.size(recipes_data[key]) then
			for i=1, #recipes_data[key] do
				if table.equal(recipes_data[key][i].ingredients, ingredients[e.obj_id].sections) then
					local gen_num = 0
					for j = 1, #ingredients[e.obj_id].ids do
						gen_num = gen_num + ingredients[e.obj_id].ids[j]
					end
					randomseed_execute(sim:seed() + 20 * e.obj_id + level.get_time_days() + 20 * gen_num, generate_transmute, e, key, i)
					return
				end
			end
		end
    end
end
local function anomalies_update ()
	for id,_ in pairs(absorption) do
		local anom = level.object_by_id(id)
		if anom and anom:is_anomaly() then
			anomaly_update{obj = anom, obj_id = id}
		end
	end
end

local function anomaly_enter(e)
    local obj = e.enter_obj
    if obj and obj:is_artefact() then
        local art = obj:get_artefact()
		log('ANOM_ENTER art transmut = flag[%s]/can[%s]/time[%s]/delay[%s]', art.transmut_flag, art.can_transmut, art.in_transmut_time, art.in_transmut_delay)
		if art.transmut_flag then
			art.transmut_flag = false
			if m_timers.timer_exists("pp_anom_transmut_timer"..tostring(e.obj_id)) then show_hint('anom_in_transmut_process') log('anomaly_enter timer exist -> exit') return end
			if READ_IF_EXISTS_SYS('r_float', e.obj:section(), 'min_art_condition_transmut', 0.2) > obj:condition() then show_hint('anom_decline_art') log('anomaly_enter bad art -> exit') return end
			art:Blink(true, true, 1000, true)
			art:SwitchLights(false)
			art:SwitchAfParticles(false)
			-- ph_fix_by_bone(obj, 'joint2')
			art.in_transmut_time = time_global() + art.in_transmut_delay
			if not is_table(absorption[e.obj_id]) then absorption[e.obj_id] = {} end
			if not is_table(ingredients[e.obj_id]) then ingredients[e.obj_id] = {sections = {}, ids = {}} end
			table.insert(absorption[e.obj_id], {section = obj:section(), obj_id = obj:id(), life = art.in_transmut_time, art = art})
			table.insert(ingredients[e.obj_id].sections, obj:section())
			table.insert(ingredients[e.obj_id].ids, obj:id())
--			table.print(absorption, 'absorption anomaly_enter')
--			table.print(ingredients, 'ingredients anomaly_enter')
		end
        log('ANOM_ENTER art transmut = flag[%s]/can[%s]/time[%s]/delay[%s]', art.transmut_flag, art.can_transmut, art.in_transmut_time, art.in_transmut_delay)
    end
end

local function anomaly_exit(e)
	if m_timers.timer_exists("pp_anom_transmut_timer"..tostring(e.obj_id)) then log('anomaly_exit timer exist -> exit') return end
    local obj = e.exit_obj
    if is_table(absorption[e.obj_id]) and table.size(absorption[e.obj_id]) then
		log('ANOM_EXIT art transmut = start cycle absorption %s ingredients %s', absorption, ingredients)
        for i=1, #absorption[e.obj_id] do
            if obj:id() == absorption[e.obj_id][i].obj_id then
				absorption[e.obj_id][i].art.in_transmut_time = 0
				absorption[e.obj_id][i].art:SwitchLights(true)
				absorption[e.obj_id][i].art:SwitchAfParticles(true)
				absorption[e.obj_id][i].art:StopVisibleSound()
				-- ph_release_fix_by_bone(obj, 'joint2')
                table.remove(absorption[e.obj_id], i)
                table.remove(ingredients[e.obj_id].sections, i)
                table.remove(ingredients[e.obj_id].ids, i)
				if #absorption[e.obj_id] == 0 then
					absorption[e.obj_id] = nil
				end
--				table.print(absorption, 'absorption anomaly_exit')
				log('ANOM_EXIT art transmut = remove success absorption %s ingredients %s', absorption, ingredients)
				return
            end
        end
		log('ANOM_EXIT art transmut = no remove absorption %s ingredients %s', absorption, ingredients)
    end
end

local function item_slot(e)
    local obj = e.item
	if obj:get_slot() ~= ARTEFACT_SLOT then return end
    if obj and obj:is_artefact() and has_info(obj:section()..'_studied') then
        local art = obj:get_artefact()
        log('SLOT_ITEM art transmut = flag[%s]/can[%s]', art.transmut_flag, art.can_transmut)
        art.can_transmut = true
        log('SLOT_ITEM art transmut = flag[%s]/can[%s]', art.transmut_flag, art.can_transmut)
    end
end

local function item_spawn(e)
	local obj = e.obj
	if obj:is_artefact() then
		local stored_action = stored_changable_arts[e.obj_id]
		log('item_spawn: art = %s; stored = %s', obj, stored_action)
		if stored_action then
			if stored_action == "change" then
				local set_params = {}
				for k,v in pairs(m_arts.get_art_params(obj)) do
					if k:find('immunity') then
						set_params[k] = v-(math.abs(1-v)*(skill_green/skill_green_max))
					else
						if k == 'radiation_restore_speed' or k == 'psy_health_restore_speed' or k == 'thirst_restore_speed' then
							set_params[k] = v>0 and v-(v*(skill_green/skill_green_max)) or v+(v*(skill_green/skill_green_max))
						else
							set_params[k] = v>0 and v+(v*(skill_green/skill_green_max)) or v-(v*(skill_green/skill_green_max))
						end
					end
				end
				m_arts.set_art_params(obj, set_params)
			else
				change_condition(obj, e.obj_id, READ_IF_EXISTS_SYS('r_u32', stored_action, 'min_reduction_art_transmut', 20), READ_IF_EXISTS_SYS('r_u32', stored_action, 'max_reduction_art_transmut', 40))
			end
			stored_changable_arts[e.obj_id] = nil
		end
	end
end

local function key_press(e)
	local ai = actor:active_item()
	if ai and ai:is_artefact() and dik_to_bind(e.key) == key_bindings.kWPN_ZOOM and not has_info(ai:section()..'_studied') then
		show_hint(string.format(game.translate_string('art_not_studied'), get_string_name(ai:section())))
	end
end

local function info_received(e)
	if e.info:find('_studied') then
	    manager_news.send_new('art_studied_head|clr:lemon', get_string_name(e.info:gsub('_studied', '')), 'artefacts', nil, nil, 10, 'info')
	end
end
--//-----------------------------------------------------------------------------------------------
--// Global functions
--//-----------------------------------------------------------------------------------------------
--//-----------------------------------------------------------------
--// Timer implementation
--//-----------------------------------------------------------------
class "anom_transmut_timer" (m_timers.savable_timer)
function anom_transmut_timer:__init(timer_id) super(timer_id)
    self._class = script_name()..".anom_transmut_timer"
	self.id = tonumber(self._id:match('(%d+)$'))
    log('timer constructed %s/%s', timer_id, self.id)
	self.data = stored_anom_data[self.id]
	if not is_table(self.data.ingredients) then return end
	for i=1,#self.data.ingredients do
		if self.data.failed or self.data.result then
			sim:release_by_id(self.data.ingredients[i])
		else
			sim:set_switch_online(self.data.ingredients[i], false)
			sim:set_switch_offline(self.data.ingredients[i], true)
		end
	end
	if self.data.failed or self.data.result then
		stored_anom_data[self.id].ingredients = nil
	end
	-- manager_news.send_new('anom_transmut_timer_message_header', game.translate_string('anom_transmut_timer_message_start'), 'got_artefact')
end

function anom_transmut_timer:taction()
    log('timer taction %s/%s', timer_id, self.id)
	self.data = stored_anom_data[self.id]
	local anom = sim:object(self.id)
	local res = 2
	if self.data.result then
		local rank = sini:r_u32(self.data.result, 'af_rank')
		if skill_green < skill_steps[rank >= #skill_steps and #skill_steps or rank] then
			skill_green = math.min(skill_green+1, skill_green_max)
		end
		local sobj = sim:create(self.data.result, anom.position, anom.m_level_vertex_id, anom.m_game_vertex_id)
		write_pstor('art_modify_normal', true, sobj)
		m_timers.start_quick_timer(250, effect, sobj.id, anom.id, "change")
		m_ranks.add_transmut_count(actor, self.data.result)
		res = 1
	end
	if is_table(self.data.ingredients) then
		local anom_id, anom_sect = anom.id, anom:section_name()
		local min_cond, max_cond = READ_IF_EXISTS_SYS('r_u32', anom_sect, 'min_reduction_art_transmut', 20), READ_IF_EXISTS_SYS('r_u32', anom_sect, 'max_reduction_art_transmut', 40)
		for i=1,#self.data.ingredients do
			sim:set_switch_online(self.data.ingredients[i], true)
			sim:set_switch_offline(self.data.ingredients[i], false)
			m_timers.start_quick_timer(250, effect, self.data.ingredients[i], anom_id, anom_sect, min_cond, max_cond)
		end
		res = 3
	end
	local key = READ_IF_EXISTS_SYS('r_u32', anom:section_name(), "anomaly_type", raw_u32())
	manager_news.send_new('anom_transmut_timer_message_header', string.format(game.translate_string('anom_transmut_timer_message_finish'), game.translate_string(type2name[key])), 'got_artefact', nil, math.random(2,5), 10, 'sos') 
	event("transmut_end"):trigger{anom=anom,anom_id=self.id,data=self.data,res=res}
	stored_anom_data[self.id] = nil
end
--//-----------------------------------------------------------------
--// Construct article description
--//-----------------------------------------------------------------
function set_article_text(article, name, group, text)
	if not text_cache[article] then
		local recipe = recipes_data[article]
		assert(is_table(recipe), 'is_table(recipe), recipe = [%s], article = [%s]', recipe, article)
		local ing, r, d, t = '\\n'..colors.orange, recipe.rejection_prob, recipe.degeneration_prob, colors.stateblue..recipe.time[1]..'-'..recipe.time[2]
		for i=1,#recipe.ingredients do ing = ing..get_string_name(recipe.ingredients[i])..'\\n' end
		text_cache[article] = string.format(game.translate_string(text), colors.orange..get_string_name(recipe.result)..colors.default, colors.coral..game.translate_string(name)..colors.default, ing..colors.default, t..colors.default, colors.green..tostring(100-r-d)..'%\\n'..colors.default, colors.lemon..r..'%\\n'..colors.default, colors.coral..d..'%\\n'..colors.default)
	end
	return text_cache[article]
end
--//-----------------------------------------------------------------------------------------------
--// Initialization module
--//-----------------------------------------------------------------------------------------------
local function presets()
    assert(loaded, "not init!")
    
    event("anomaly_enter"):register(anomaly_enter)
    event("anomaly_exit"):register(anomaly_exit)
    event("actor_update"):register(anomalies_update, {__period = 2705})
    event("item_slot"):register(item_slot)
    event("item_spawn"):register(item_spawn)
	event("key_press"):register(key_press)
	event("info_received"):register(info_received)
end
--//-------------------------
function init()
    event("presets"):register(presets)
	fill_recipes_data()
    loaded = true
    log('init:[>]')
end
--//-----------------------------------------------------------------------------------------------
--// Serialization module
--//-----------------------------------------------------------------------------------------------
function save(pk)
	pk.stored_anom_data = stored_anom_data
	pk.stored_changable_arts = stored_changable_arts
	if skill_green > 0 then
		pk.skill_green = skill_green
	end
end
function load(pk)
	if is_table(pk.stored_anom_data) then stored_anom_data = pk.stored_anom_data end
	if is_table(pk.stored_changable_arts) then stored_changable_arts = pk.stored_changable_arts end
	if is_number(pk.skill_green) then skill_green = pk.skill_green end
end
--//-----------------------------------------------------------------------------------------------
--// External functions
--//-----------------------------------------------------------------------------------------------
function delete_transmutation(id)
	if m_timers.timer_exists("pp_anom_transmut_timer"..tostring(id)) then
		m_timers.get_timer("pp_anom_transmut_timer"..tostring(id)):stop()
	end
	stored_anom_data[id] = nil
end
function art_in_transmut(id)
	if table.size(stored_anom_data) then
		for k,v in pairs(stored_anom_data) do
			if is_table(v) then
				for i=1,#v do
					if v[i] == id then return true end
				end
			end
		end
	end
	return false
end
function get_recipe(name)
	local res = recipes_data[name]
	assert(is_table(res), 'is_table(res), res = [%s], name = [%s]', res, name)
	return res
end
function get_skills()
	return skill_green, skill_green_max
end
--//-----------------------------------------------------------------------------------------------
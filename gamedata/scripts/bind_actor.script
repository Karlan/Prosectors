--[[ ----------------------------------------------------------------------------------------------
 File       : bind_actor.script
 Description: Actor binder
 Copyright  : 2017 � Prosectors Project
 Author     : Karlan (�� ���������� GSC) 
--]] ----------------------------------------------------------------------------------------------
--//-----------------------------------------------------------------
--// Flags for enable output of the debug information
--//-----------------------------------------------------------------
_DEBUG_ = true --// file under debugging?
_DEBUG_GSC_ = _DEBUG_ and false --// only for scripts by GSC
--//-----------------------------------------------------------------
function init(npc) _G.g_bind_actor = this.actor_binder(npc) npc:bind_object(g_bind_actor) end
--//-----------------------------------------------------------------
local function disable_ui_info()	--// ���������� ����� ��� ����, ���� ���������� ��������� � ui
	if has_info('ui_pda') then give_info('ui_pda_hide') end
	if has_info('ui_talk') then give_info('ui_talk_hide') end
	if has_info('ui_trade') then give_info('ui_trade_hide') end
	if has_info('ui_car_body') then give_info('ui_car_body_hide') end
	if has_info('ui_inventory') then give_info('ui_inventory_hide') end
end
function verify_difficulty()
	local gd_init	= read_pstor('gd_idx')
	local gd_cur	= level.get_game_difficulty()
	if gd_init ~= gd_cur then
		if gd_init == 4 then
			level.set_game_difficulty(gd_init)
		elseif gd_cur == 4 then
			level.set_game_difficulty(gd_cur-1)
		end
	end
end
--//-----------------------------------------------------------------
class "actor_binder" (object_binder)
--//-----------------------------------------------------------------
function actor_binder:__init(obj) super(obj)
    _G.actor    = self.object 
    _G.actor_id = self.object:id()
    --//--------------------------
    self.is_first_start = true
    self.is_start       = false
    self.am             = m_signals.get_mgr() --// timers support
    event("actor_bind"):once(true):trigger()
end
----------------------------------------------------------------------------------------------------------------------
function actor_binder:net_spawn(data)
    if not object_binder.net_spawn(self,data) then return false end
	debug.g_spy:start("actor_binder:net_spawn")
    self.is_start = true
    disable_ui_info()
    level.show_indicators()
    if not db.storage[actor_id].pstor then
        db.storage[actor_id].pstor = {}
    end
    actor_stats.add_to_ranking(actor_id)
	local previous_level = read_pstor('lvlid')
	local level_change = previous_level ~= sim:level_id()
    event("actor_spawn"):once(true):trigger{actor=actor,binder=self,se_actor=data,is_first_start=self.is_first_start,level_change=level_change}
	if level_change then
		write_pstor('lvlid',sim:level_id())
		local visited_levels = read_pstor('vis_lvls') or {}
		local first = not table.index(visited_levels,sim:level_id())
		if first then
			table.insert(visited_levels,sim:level_id())
			write_pstor('vis_lvls',visited_levels)
		end
		event("actor_level_change"):once(true):trigger{is_first_start=self.is_first_start,level_id=sim:level_id(),previous_level_id=previous_level,is_first_visit=first}
	end
	if self.is_first_start then	--// ��������� ��������� ������� ���������
		write_pstor('gd_idx',level.get_game_difficulty())
	else
		verify_difficulty()
	end
	debug.g_spy:finish("actor_binder:net_spawn")
    return true
end
----------------------------------------------------------------------------------------------------------------------
function actor_binder:net_destroy()
    self:del_callbacks()
    event("actor_destroy"):once(true):trigger()
	_G.actor = nil
end
function actor_binder:del_callbacks()
    log("actor_binder:del_callbacks:[>]")
    actor:set_callback(callback.inventory_info,         nil) --// ���������/���������� ����
    actor:set_callback(callback.article_info,           nil) --// ��������� ������
    actor:set_callback(callback.on_item_take,           nil) --// ��������� ��������
    actor:set_callback(callback.on_item_drop,           nil) --// ������ ��������
    actor:set_callback(callback.on_item_ruck,           nil) --// ��������� �������� � �������
    actor:set_callback(callback.on_item_slot,           nil) --// ��������� �������� � �����
    actor:set_callback(callback.on_item_belt,           nil) --// ��������� �������� �� �����
    actor:set_callback(callback.on_item_remove_ruck,    nil) --// ������������ �������� � �������
    actor:set_callback(callback.on_item_remove_slot,    nil) --// ������������ �������� � �����
    actor:set_callback(callback.on_item_remove_belt,    nil) --// ������������ �������� �� �����
    actor:set_callback(callback.use_object,             nil) --// ������������� ��������
    actor:set_callback(callback.trade_sell_buy_item,    nil) --// ���� ����������� ������
    actor:set_callback(callback.task_state,             nil) --// ���������/���������� �����
    actor:set_callback(callback.level_border_enter,     nil) --// ����� ����� �� ������� ����
    actor:set_callback(callback.level_border_exit,      nil) --// ����� ����� � ������� ���� (������������ �� ���������� ���������)
    actor:set_callback(callback.take_item_from_box,     nil) --// ������ �������� �� �����
    actor:set_callback(callback.on_key_press,           nil) --// ������� �������
    actor:set_callback(callback.on_key_hold,            nil) --// ��������� �������
    actor:set_callback(callback.on_key_release,         nil) --// ���������� �������
    actor:set_callback(callback.on_start_dialog,        nil) --// ���� ������ ������� (���������� �������� ������� ����������, ������ ������ level.get_talker())
    actor:set_callback(callback.on_stop_dialog,         nil) --// ���� ��������� ������� (���������� ����� ������� �������� � ����������, ��������� �������� ��������� ����������)
    actor:set_callback(callback.on_post_save,           nil) --// ��������� ����� ���������� ���� ��������
    actor:set_callback(callback.hit,                    nil) --// ��� �������
    actor:set_callback(callback.death,                  nil) --// ������� �� ������
    actor:set_callback(callback.on_switch_torch,        nil) --// ������� �� ������������ ������
    actor:set_callback(callback.on_ph_capture,          nil) --// ������� ����������� � ������ ������������ ��������
    actor:set_callback(callback.on_ph_release,          nil) --// ������� ����������� ����� �������������� ���������� ������������ ��������
    actor:set_callback(callback.on_weapon_fire,         nil) --// ������� ������
    actor:set_callback(callback.on_pickup_item_showing, nil) --// ����������� ������ �������� ��������
    actor:set_callback(callback.on_travel,				nil) --// ������������� ������������ ������ �� ������ ����. ���������� ����� ������. �������� - ����� �������� � ������� ��������.
    actor:set_callback(callback.on_level_map_click,		nil) --// ���� �� ����� � ���.
    actor:set_callback(callback.on_map_spot_click,		nil) --// ���� �� ����� � ���.
end
----------------------------------------------------------------------------------------------------------------------
function actor_binder:reinit()
    db.storage[actor_id] = {["pstor"] = {}}
    self.st = db.storage[actor_id]
    event("actor_stor"):trigger() 
    self:add_callbacks()
end
function actor_binder:add_callbacks()
    log("actor_binder:add_callbacks:[>]")
    actor:set_callback(callback.inventory_info,         self.info_callback,        self)
    actor:set_callback(callback.article_info,           self.article_callback,     self)
    actor:set_callback(callback.on_item_take,           function(self, obj, pos)    event("item_take"):trigger{item=obj,item_id=obj:id(),section=obj:section(),pos=pos}     end,   self)
    actor:set_callback(callback.on_item_drop,           function(self, obj, slot, transfer)   event("item_drop"):trigger{item=obj,item_id=obj:id(),section=obj:section(),slot=slot,transfer=transfer}   end,   self)
    actor:set_callback(callback.on_item_ruck,           function(self, obj, place)  event("item_ruck"):trigger{item=obj,item_id=obj:id(),section=obj:section(),place=place} end,   self)
    actor:set_callback(callback.on_item_slot,           function(self, obj, place)  event("item_slot"):trigger{item=obj,item_id=obj:id(),section=obj:section(),place=place} end,   self)
    actor:set_callback(callback.on_item_belt,           function(self, obj, place)  event("item_belt"):trigger{item=obj,item_id=obj:id(),section=obj:section(),place=place} end,   self)
    actor:set_callback(callback.on_item_remove_ruck,    function(self, obj, place)  event("item_remove_ruck"):trigger{item=obj,item_id=obj:id(),section=obj:section(),place=place} end,   self)
    actor:set_callback(callback.on_item_remove_slot,    function(self, obj, place, slot)  event("item_remove_slot"):trigger{item=obj,item_id=obj:id(),section=obj:section(),place=place,slot=slot} end,   self)
    actor:set_callback(callback.on_item_remove_belt,    function(self, obj, place)  event("item_remove_belt"):trigger{item=obj,item_id=obj:id(),section=obj:section(),place=place} end,   self)
    actor:set_callback(callback.use_object,             function(self, obj, eat)    event("item_use"):trigger{item=obj,item_id=obj:id(),section=obj:section(),eat=eat}      end,   self)
    actor:set_callback(callback.trade_sell_buy_item,    self.on_trade_action,      self)
    actor:set_callback(callback.task_state,             self.task_callback,        self)
    actor:set_callback(callback.level_border_enter,     function(self) event("level_border_enter"):trigger() end,   self)
    actor:set_callback(callback.level_border_exit,      function(self) event("level_border_exit"):trigger() end,   self)
    actor:set_callback(callback.take_item_from_box,     self.take_item_from_box,   self)
    actor:set_callback(callback.on_key_press,           function(self, key, input_disabled)         event(input_disabled and "key_press_dis" or "key_press"):trigger{key=key} end, self)
    actor:set_callback(callback.on_key_hold,            function(self, key)                         event("key_hold"):trigger{key=key}                      end,    self)
    actor:set_callback(callback.on_key_release,         function(self, key)                         event("key_release"):trigger{key=key}                   end,    self)
    actor:set_callback(callback.on_start_dialog,        function(self, obj)                         event("start_dialog"):trigger{obj=obj}                  end,    self)
    actor:set_callback(callback.on_stop_dialog,         function(self, obj)                         event("stop_dialog"):trigger{obj=obj}                   end,    self)
    actor:set_callback(callback.on_post_save,           self.on_post_save,         self)
    actor:set_callback(callback.saw_object,             function(self, saw_obj, element) event("actor_saw_obj"):trigger{item=saw_obj} if saw_obj then set_saw_obj_id(saw_obj:id(), element) end end,   self)
    actor:set_callback(callback.hit,                    self.hit_callback,      self)
    actor:set_callback(callback.death,                  function(self, victim, who, bone) event("actor_death"):trigger{who=who,bone=bone} end, self)
    actor:set_callback(callback.on_switch_torch,        function(self, light_on) event("switch_torch"):trigger{light_on=light_on} end, self)
    actor:set_callback(callback.on_ph_capture,          function(self, obj)         event("obj_capture_init"):trigger{item=obj,item_id=obj:id(),section=obj:section()}      end, self)
    actor:set_callback(callback.on_ph_release,          function(self, obj)         event("obj_capture_release"):trigger{item=obj,item_id=obj:id(),section=obj:section()}   end, self)
    actor:set_callback(callback.on_weapon_fire,         function(self, obj, ammo)	event("actor_weapon_fire"):trigger{wpn=obj,ammo=ammo} end, self)
    actor:set_callback(callback.on_pickup_item_showing,	function(self, obj)			event("pickup_item_showing"):trigger{item=obj} end, self)
    actor:set_callback(callback.on_travel,				function(self, delta)		event("actor_travel"):trigger{time=delta} end, self)
    actor:set_callback(callback.on_level_map_click,		function(self, pos, name, mb)	event("map_click"):trigger{name=name,pos=pos,mb=mb} end, self)
    actor:set_callback(callback.on_map_spot_click,		function(self, obj_id, _type, hint)	event("map_spot"):trigger{obj_id=obj_id,type=_type,hint=hint} end, self)
end
--------------------------------------------------------------------------------------------------------------------
function actor_binder:hit_callback(actor, amount, direction, who, bone_id, hit_type, weapon_id, impulse)
    -- log("amount = %s, hit_type = %s, who = %s, bone = %s", amount, HIT_TYPE_BY_ID[hit_type], who:name(), actor:get_bone_name(bone_id))
    event("actor_hit"):trigger{obj=actor,victim=actor,amount=amount,dir=direction,who=who,bone=bone_id,hit_type=hit_type,weapon_id=weapon_id,is_actor=true}
end
----------------------------------------------------------------------------------------------------------------------
function actor_binder:take_item_from_box(box, item)
    event("item_take_from_box"):trigger{box=box,box_id=box:id(),box_sid=story_id,item=item,item_id=item:id()}
end
----------------------------------------------------------------------------------------------------------------------
function actor_binder:on_trade_action(item, sell_bye, money, first_trader, second_trader)
    local act, npc = get_actor_npc_talk(first_trader, second_trader)
    event("trade_action"):trigger{item=item,item_id=item:id(),sell_bye=sell_bye,money=money,actor=act,npc=npc,obj=npc}
end
----------------------------------------------------------------------------------------------------------------------
function actor_binder:info_callback(actor, info_id, recieve)
	if recieve then
		event("info_received"):trigger{actor=actor,info=info_id}
	end
	info_actions.execute_info(info_id,recieve)
end
----------------------------------------------------------------------------------------------------------------------
function actor_binder:article_callback(npc, group, name, _type) --// ���� ������ 4 ��������� ������ (��������� - ���: ������������, ������, ����, �������)
    if dev.precache_frame > 0 then return end --// Karlan: ����� ���� �����, ��� ��������� ������������ ������� ������ �� ������ � ������� ������ ���� �� �����-������ ������������ ������� ������� ���-������ ��� �� 30 ������, �� ���� ���������� ������������ �������� ���� ���� ��� ���������
    event("article_received"):trigger{npc=npc,group=group,name=name,type=_type}
end
----------------------------------------------------------------------------------------------------------------------
function actor_binder:task_callback(_task, _objective, _state) --// todo: �������� ���������� �������� ������ � ���� �������
	local canceled = manager_task.task_callback(_task:get_id(), _objective:get_idx(), _state)
    if _objective:get_idx() == 0 then
        if _state == task.fail then
			if canceled then
				manager_news.send_task(actor, "cancel", _task, _objective)
				actor:get_actor():remove_task(_task)	return
			else
				manager_news.send_task(actor, "fail", _task, _objective)
			end
        elseif _state == task.completed then
            manager_task.reward_by_task(_task)
            manager_news.send_task(actor, "complete", _task, _objective)
        else
            manager_news.send_task(actor, "new", _task, _objective)
        end
    else
        if _task:get_objective(0):get_state() == task.in_progress then
            manager_news.send_task(actor, "update", _task, _objective)
        end
    end
    event("task_received"):trigger{task=_task,objective=_objective,state=_state}
end
----------------------------------------------------------------------------------------------------------------------
function actor_binder:update(delta)
--	debug.g_spy:start("actor_binder:update")
    _G.watchdog = make_string('Update is worked: start(%s)', delta) --// DEBUG [!]
    local time = time_global()
    if self.is_start then
        self.is_start = false
        event("update_1st"):once(true):trigger{time=time,delta=delta,binder=self}
    end
    _G.watchdog = make_string('Update is worked: self.is_start(%s)', delta) --// DEBUG [!]
    self.am:call("on_actor_update", delta) --// timers support
    _G.watchdog = make_string('Update is worked: self.am:call(%s)', delta) --// DEBUG [!]
    event("actor_update"):trigger{time=time,delta=delta,binder=self}
    _G.watchdog = make_string('Update is worked: finish(%s)', delta) --// DEBUG [!]
--	debug.g_spy:finish("actor_binder:update")
end
----------------------------------------------------------------------------------------------------------------------
function actor_binder:on_post_save()
    log('on_post_save')
    event("storage_save"):trigger()
end
----------------------------------------------------------------------------------------------------------------------
function actor_binder:on_before_load()
    self.is_first_start = false
    event("storage_load"):trigger()
end
----------------------------------------------------------------------------------------------------------------------
function actor_binder:save(packet)
    --// ORIGINAL METHODS // PLEASE, NOW DON'T USE //--
    self.am:call("on_actor_save", packet) --// timers support
    event("actor_save"):trigger{packet=packet} --// ������: ����� ����-�� ��������� ����-�� �������?
end
----------------------------------------------------------------------------------------------------------------------
function actor_binder:load(reader)
    self:on_before_load() --// fake callback
    --// ORIGINAL METHODS // PLEASE, NOW DON'T USE //--
    self.am:call("on_actor_load", reader) --// timers support
    event("actor_load"):trigger{packet=reader} --// ������: ����� ����-�� ��������� ����-�� �������?
end
----------------------------------------------------------------------------------------------------------------------
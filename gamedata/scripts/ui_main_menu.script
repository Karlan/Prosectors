--[[ -------------------------------------------------------------------------------------------
 File		: ui_main_menu.script
 Description: Main Menu
 Copyright	: 2022 © Prosectors Project
 Author(s)	: Serhiy Vynnychenko
 Edited		: Rulix, Karlan
--]] -------------------------------------------------------------------------------------------

class "main_menu" (CUIScriptWnd)

function main_menu:__init() super()
	self:SetWindowName("main_menu")
	self:InitControls()
	self:InitCallBacks()
end

function main_menu:__finalize()
end

function main_menu:stop_window(dlg)
	dlg:GetHolder():start_stop_menu(self,true)
	dlg:GetHolder():start_stop_menu(dlg,true)
	dlg.owner	= nil
end

function main_menu:InitControls()
	self:Init(0,0,1024,768)
	local xml = CScriptXmlInit()
	xml:ParseFile("ui_mm_main.xml")

	xml:InitAutoStaticGroup("background", self)
	self.shniaga = xml:InitMMShniaga("shniaga_wnd",self);

	self.message_box = CUIMessageBoxEx()
	self:Register(self.message_box, "msg_box")

	self.version = xml:InitStatic	("static_version",self)
	self.authors = xml:InitStatic	("static_authors",self)
	local mm						= _G.main_menu.get_main_menu()
	local version_str				= mm:GetGSVer()
	if string_table.has_string('ui_mm_version') then
		version_str					= version_str.." "..game.translate_string('ui_mm_version')
	end
	if not actor then
		logc("* MM Version: %s",version_str)
	end
	self.version:SetText(version_str)
	self.authors:SetText(mm:GetProsectors())

	self:InitScale		(xml)
end

function main_menu:Rescale(kx,tkx)
	if self:GetWndPos().x < 0 then
		self.version:SetWndPos	(-self:GetWndPos().x + self.version:GetWndPos().x, self.version:GetWndPos().y)
		self.authors:SetWndPos	(-self:GetWndPos().x + self.authors:GetWndPos().x, self.authors:GetWndPos().y)
	end
end

function main_menu:OnShow(status)
	self.shniaga:SetVisibleMagnifier(status)
	local btn = self.shniaga:FindStatic('btn_lastsave',3)
	if btn then btn:SetHintText(string.format(game.translate_string('ui_mm_last_save_is'),get_console():get_string("load_last_save"))) end
end

function main_menu:InitCallBacks()
	-- new game
	self:AddCallback("btn_novice",		ui_events.BUTTON_CLICKED,				self.OnButton_new_novice_game,	self)
	self:AddCallback("btn_stalker",		ui_events.BUTTON_CLICKED,				self.OnButton_new_stalker_game,	self)
	self:AddCallback("btn_veteran",		ui_events.BUTTON_CLICKED,				self.OnButton_new_veteran_game,	self)
	self:AddCallback("btn_master",		ui_events.BUTTON_CLICKED,				self.OnButton_new_master_game,	self)
	self:AddCallback("btn_legend",		ui_events.BUTTON_CLICKED,				self.OnButton_new_legend_game,	self)
	-- options
	self:AddCallback("btn_options",		ui_events.BUTTON_CLICKED,				self.OnButton_options_clicked,	self)
	-- load
	self:AddCallback("btn_load",		ui_events.BUTTON_CLICKED,				self.OnButton_load_clicked,		self)
	-- save
	self:AddCallback("btn_save",		ui_events.BUTTON_CLICKED,				self.OnButton_save_clicked,		self)
	-- quit
	self:AddCallback("btn_quit",		ui_events.BUTTON_CLICKED,				self.OnButton_quit_clicked,		self)
	self:AddCallback("btn_quit_to_mm",	ui_events.BUTTON_CLICKED,				self.OnButton_disconnect_clicked,self)
	self:AddCallback("btn_ret",			ui_events.BUTTON_CLICKED,				self.OnButton_return_game,		self)
	self:AddCallback("btn_lastsave",	ui_events.BUTTON_CLICKED,				self.OnButton_last_save,		self)
	self:AddCallback("btn_credits",		ui_events.BUTTON_CLICKED,				self.OnButton_credits_clicked,	self)
	-- message box
	self:AddCallback("msg_box",			ui_events.MESSAGE_BOX_OK_CLICKED,		self.OnMsgOk,					self)
	self:AddCallback("msg_box",			ui_events.MESSAGE_BOX_YES_CLICKED,		self.OnMsgYes,					self)
	self:AddCallback("msg_box",			ui_events.MESSAGE_BOX_NO_CLICKED,		self.OnMsgOk,					self)
end
function main_menu:OnMsgOk()
	self.mbox_mode = 0
	self.shniaga:SetVisibleMagnifier(true)
end

function main_menu:OnMsgYes()
	if self.mbox_mode == 1 then
		self:LoadLastSave()
	end
	if self.mbox_mode == 2 then
		self:OnMessageQuitWin()
	end
	if self.mbox_mode == 3 then
		self:OnMessageQuitGame()
	end
	self.mbox_mode = 0
end

function main_menu:LoadLastSave()
	get_console():execute("load_last_save")
end

function main_menu:OnButton_last_save()
	if not valid_saved_game(get_console():get_string("load_last_save")) then
		self.message_box:Init	("message_box_invalid_saved_game")
		self:GetHolder():start_stop_menu(self.message_box, true)
		return
	end
	if not (alife() and actor and actor:alive()) then
		self:LoadLastSave();
		return
	end
	self.mbox_mode			= 1
	self.message_box:Init	("message_box_confirm_load_save")
	self:GetHolder():start_stop_menu(self.message_box, true)
end

function main_menu:OnButton_credits_clicked()
	game.start_tutorial("credits_seq")
end

function main_menu:OnButton_quit_clicked()
	self.mbox_mode			= 2
	self.message_box:Init("message_box_quit_game")
	self.message_box:SetText("ui_mm_quit_windows_message")
	self:GetHolder():start_stop_menu(self.message_box, true)
end

function main_menu:OnButton_disconnect_clicked()
	self.mbox_mode			= 3
	self.message_box:Init("message_box_quit_game")
	self:GetHolder():start_stop_menu(self.message_box, true)
end

function main_menu:OnMessageQuitGame()
	get_console():execute("disconnect")
end

function main_menu:OnMessageQuitWin()
	get_console():execute("quit")
end

function main_menu:OnButton_return_game()
	get_console():execute("main_menu off")
end

function main_menu:OnButton_new_novice_game()
	get_console():execute("g_game_difficulty gd_novice")
	self:StartGame()
end

function main_menu:OnButton_new_stalker_game()
	get_console():execute("g_game_difficulty gd_stalker")
	self:StartGame()
end

function main_menu:OnButton_new_veteran_game()
	get_console():execute("g_game_difficulty gd_veteran")
	self:StartGame()
end

function main_menu:OnButton_new_master_game()
	get_console():execute("g_game_difficulty gd_master")
	self:StartGame()
end

function main_menu:OnButton_new_legend_game()
	get_console():execute("g_game_difficulty gd_legend")
	self:StartGame()
end

function main_menu:StartGame()
	get_console():execute("start server(all/single/alife/new)")
	get_console():execute("main_menu off")
end

function main_menu:OnButton_save_clicked()
	if actor then
		if (get_option_value("save_near_campfire") and not actor:get_actor():on_save_position()) or actor:get_actor():get_save_lock() > 0 then
			self.message_box:Init("message_box_invalid_saved_game")
			self.message_box:SetText("st_game_save_deny")
			self:GetHolder():start_stop_menu(self.message_box, true)
			return
		end
		if get_option_value("save_check_enemies") and actor:get_actor():around_enemies() then
			self.message_box:Init("message_box_invalid_saved_game")
			self.message_box:SetText("st_game_save_deny_not_safe")
			self:GetHolder():start_stop_menu(self.message_box, true)
			return
		end
	end
	if self.save_dlg == nil then
		self.save_dlg = ui_save_load_dialog.save_dialog()
	end

	self.save_dlg.owner = self
	self:GetHolder():start_stop_menu(self.save_dlg, true)
	self:GetHolder():start_stop_menu(self, true)
end

function main_menu:OnButton_load_clicked()
	if self.load_dlg == nil then
		self.load_dlg = ui_save_load_dialog.load_dialog()
	end
	self.load_dlg.owner = self
	self:GetHolder():start_stop_menu(self.load_dlg, true)
	self:GetHolder():start_stop_menu(self, true)
end

function main_menu:OnButton_options_clicked()
	if self.opt_dlg == nil then
		self.opt_dlg = ui_mm_opt_main.options_dialog()
	end

	self.opt_dlg.owner = self
	self.opt_dlg:UpdateControls()
	self:GetHolder():start_stop_menu(self.opt_dlg, true)
	self:GetHolder():start_stop_menu(self, true)
end

function main_menu:OnKeyboard(dik, keyboard_action)
	CUIScriptWnd.OnKeyboard(self,dik,keyboard_action)

	if keyboard_action == ui_events.WINDOW_KEY_PRESSED then
		if dik == DIK_keys.DIK_ESCAPE then
			if is_level_loaded() and actor:alive() then
				get_console():execute("main_menu off")
			end
		end
		if dik == DIK_keys.DIK_Q then
			if not alife() then	--	//PROSECTORS_DEBUG
				self:OnMessageQuitWin()
			else
				self:OnButton_quit_clicked()
				self.shniaga:SetVisibleMagnifier(false)
			end
		end
		if 	dik == DIK_keys.DIK_L and self:GetStatic("btn_lastsave") then
			self:OnButton_last_save()
			self.shniaga:SetVisibleMagnifier(false)
		end
		if 	dik == DIK_keys.DIK_S and is_level_loaded() then
			get_console():execute("save key_save")
		end
		if dik == DIK_keys.DIK_NUMPAD1 then --// печатаем флажок ошибки изолированной от системы дебага функцией
			if _G.watchdog then
				log1(make_string('WATCHDOG = %s', _G.watchdog))
				get_console():execute("flush")
			end
		end
		if event then
			event("mm_key_press"):trigger{key=dik, menu=self}
		elseif dik == DIK_keys.DIK_F1 and ui_cheat_main then
			ui_cheat_main.start_menu(self)
		end
	end
	return true
end

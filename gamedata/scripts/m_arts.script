--[[ ----------------------------------------------------------------------------------------------
 File       : m_arts.script
 Description: Artefacts additional functional
 Copyright  : 2020 � Prosectors Project
 Author(s)  : Karlan, Rulix
--]] ----------------------------------------------------------------------------------------------
--//-----------------------------------------------------------------
--// Flags for enable output of the debug information
--//-----------------------------------------------------------------
_DEBUG_ = true --// file under debugging?
_DEBUG_GSC_ = _DEBUG_ and false --// only for scripts by GSC
--//-----------------------------------------------------------------
local outfit_damage_factor = 1 / 2
--//-----------------------------------------------------------------------------------------------
--// Variables
--//-----------------------------------------------------------------------------------------------
local loaded = false
--//--------------------
local hit_data = {}
local stor_take_ids = {}
local captured_art = nil

local param_deviation = 35 --// Karlan: ������� ���������� ��������� ���������
local cameffs = {"shell_shock","hit_front","hit_right","hit_back","hit_left","hit_front_left","hit_back_left","hit_front_right","hit_back_right"}

local spawn_prob_add = {
	l02_garbage			= {0, 20, 5},		l03_agroprom	= {0, 10, 10},		l04_darkvalley	= {0, 15, 5},
	l04u_labx18			= {-10, 0, 0},		l08u_brainlab	= {-15, 0, 0},
	l18_foggy_thicket	= {-10, 30, 20},	l06_rostok		= {-5, 0, 0},		l07_military	= {0, 30, 10},
	l09_deadcity		= {-10, 40, 5},		l10_radar		= {-10, 0, 30},		l11_pripyat		= {-20, 0, 50},
	l13_generators		= {-30, -10, 60},	l15_red_forest	= {-10, 30, 20},	l16_limansk		= {-10, 60, 5},
	l17_hospital		= {-20, -5, 50},
}
local anom_no_art = {
	[clsid.zone_flame_s]        = true,
	[clsid.zone_minefield_s]    = true,
	[clsid.zone_radio_s]        = true,
	[clsid.zone_nogravity_s]    = true,
}
local art_params = {'health_restore_speed','radiation_restore_speed','satiety_restore_speed','power_restore_speed','bleeding_restore_speed','psy_health_restore_speed',
'thirst_restore_speed','alcohol_restore_speed','somnolence_restore_speed','additional_weight2',
'burn_immunity','strike_immunity','shock_immunity','wound_immunity','radiation_immunity','telepatic_immunity','chemical_burn_immunity','explosion_immunity','fire_wound_immunity'}

local spawn_independent_sections = {
	af_dummy_spring		= 1,
	af_dummy_dummy		= 1,
	af_dummy_battery	= 1,
	af_dummy_pellicle	= 1,
	af_dummy_glassbeads	= 1,
	af_fuzz_kolobok		= 1,
}
local spawn_independent_levels = {
	l01_escape = 0.3, l14_marsh = 0.6, k01_darkscape = 0.8, l02_garbage = 0.6, l03_agroprom = 0.8, l04_darkvalley = 1,
	l05_bar = 0.3, l06_rostok = 0.4, l08_yantar = 0.8, l07_military = 0.8, l09_deadcity = 0.8,
	l10_radar = 1, l11_pripyat = 0.2, l15_red_forest = 1, l16_limansk = 0.4, l17_hospital = 0.2, l18_foggy_thicket = 1,
	l03u_agr_underground = 0.2, l04u_labx18 = 0.3, l08u_brainlab = 0.3, l10u_labx14 = 0.1, l17u_catacomb = 0.3, l18u_logovo = 0.1,
--	l11u_pripyat = 0.3, l13_generators = 1, l13u_warlab = 0.4
}
local gd_art_damage_factor = {
	[game_difficulty.novice]	= 2.0,
	[game_difficulty.stalker]	= 1.6,
	[game_difficulty.veteran]	= 1.4,
	[game_difficulty.master]	= 1.0,
	[game_difficulty.legend]	= 1.0,
}
--//-----------------------------------------------------------------------------------------------
--// Local functions
--//-----------------------------------------------------------------------------------------------
local function fill_hit_data(sect)
	if not hit_data[sect] and sini:line_exist(sect, 'parent_anomaly') then
		local anom = parse_names(sini:r_string(sect, 'parent_anomaly'))
		assert(#anom>=1, 'Invalid parent_anomaly line in section [%s]', sect)
		anom = (#anom>1) and anom[math.random(#anom)] or anom[1] --// Karlan: �������� ����� ���������
		assert(sini:section_exist(anom), 'Parent anomaly section [%s] does not find in system ini!', anom)
		local power_base = sini:r_float(anom, 'max_start_power')
		local power_second = 0
		if READ_IF_EXISTS(sini,'r_bool',anom,'use_secondary_hit') then
			power_second = 2.0 * sini:r_float(anom, 'secondary_hit_power') * math.min(20,1000/sini:r_u32(anom, 'secondary_hit_time'))
		end
		local zn_hit_factor = READ_IF_EXISTS(sini,'r_float',anom,'artefact_hit_factor',1)
		local af_hit_factor = READ_IF_EXISTS(sini,'r_float',sect,'hit_factor',1)
		local power = (power_base + power_second) * zn_hit_factor * af_hit_factor
		log("fill_hit_data(%s|%s) power [%s] (%s + %s) * %s * %s",sect,anom,power,power_base,power_second,zn_hit_factor,af_hit_factor)
		hit_data[sect] = {
			particle	= sini:r_string(anom, 'blowout_particles'),
			sound		= sound_object(sini:r_string(anom, 'blowout_sound')),
			particle_hit= READ_IF_EXISTS(sini,'r_string',anom,'hit_big_particles'),
			sound_hit	= READ_IF_EXISTS(sini,'r_string',anom,'hit_sound'),
			hit			= {
				type	= hit[sini:r_string(anom, 'hit_type')],
				power	= power,
				impulse	= power*sini:r_float(anom, 'hit_impulse_scale')*1.25,
				throw	= READ_IF_EXISTS(sini,'r_float',anom,'throw_in_impulse_alive'),
			},
		}
	end
	return hit_data[sect]
end
local hit_mult_base	= 0.68
local hit_mult_var	= 0.07
local function art_hit_mult(art)
	return math.min(1,0.27+0.75*art:condition())
end

local of_pending,lc_prob
local hit_drop

local function hit_object(obj,tgt,data,a_power,init)
	if data then
		local multiplier = hit_mult_base + hit_mult_var * math.random()
		local divider = math.random(100, 200)/100
		local pos = tgt:center()
		if data.sound_hit then m_sound.play_3d(data.sound_hit,pos,obj) end
		if data.particle_hit then particles_object(data.particle_hit):play_at_pos(pos) end
		local h = hit()
--		h:bone("bip01_spine")
		h.type = data.hit.type
		h.power = data.hit.power*a_power*multiplier
		h.draftsman = obj
		if tgt == actor then
			h.power = h.power*gd_art_damage_factor[level.get_game_difficulty()]
		end
		if data.hit.throw then
			h.impulse = data.hit.throw*0.15*tgt:mass()/divider
			h.direction = obj:position():sub(tgt:position()):normalize_safe():add(vector():set(0,0.3,0)):normalize()
		else
			h.impulse = data.hit.impulse*tgt:mass()/divider	--// Rulix: ����� �������� hit_impulse_up_factor?
			h.direction = pos:sub(obj:center()):normalize_safe()
		end
		local max_hit
		if tgt:is_actor() then
			max_hit = obj:name() == 'esc_af_medusa' and 0.8	--// Rulix: �������, ����� �������� �������� �� ������
		elseif tgt:is_entity_alive() and tgt:alive() and (tgt:is_stalker() and tgt:id()%2==0 or is_story_object(tgt) or is_uniq_name(tgt)) then
			max_hit = math.max( 0, is_story_object(tgt) and tgt.health-0.5 or tgt.health-0.1 )
		end
		if max_hit and h.power*tgt.immunities:get(h.type) > max_hit then
			h.power = max_hit / tgt.immunities:get(h.type)
		end
		log("~Hit [%s]: power %s impulse %s mass %s",tgt:name(),h.power,h.impulse,tgt:mass())
		if tgt:is_actor() then
			local frame = device().frame
			local wound_power = 1
			local outfit = actor:get_current_outfit()
			if outfit then
				-- ������� ���� �������, ����� �� �����
				if not of_pending then
					local id = outfit:id()
					local outfit_factor = h.type == hit.strike and cfg_get_number(sini,outfit:section(),'strike_protection_art')
					local stor_protec = outfit_factor and outfit:get_outfit().strike_protection
					if outfit_factor then outfit:get_outfit().strike_protection = stor_protec*outfit_factor end
					local immun_factor = outfit:get_outfit().hit_immunity_factor
					outfit:get_outfit().hit_immunity_factor = immun_factor * outfit_damage_factor
					level.add_call(function()
						if device().frame > frame then
							verify_warning(of_pending)
							local outfit = level.object_by_id(id)
							outfit:get_outfit().hit_immunity_factor = immun_factor
							if stor_protec then outfit:get_outfit().strike_protection = stor_protec end
							of_pending = nil
							return true
						end end
					,function() end)
					of_pending = true
				end
				--// add wound if wound_protection < 20% and condition < 55%
				local wound_prot	= outfit:get_outfit().wound_protection*outfit:condition() --outfit:get_outfit():get_default_protection(hit.wound)
				local cnd_factor	= 1 - (outfit:condition()-0.3) / (0.55-0.3)
				wound_power			= math.max(wound_power - (wound_prot/0.2),0) * math.min(cnd_factor,1)
--				logc("#outfit [%s][%s] wound_prot [%s] cnd_factor [%s]: (%s * %s) = [%s]",outfit:name(),outfit:condition(),wound_prot,cnd_factor,(1 - (wound_prot/0.2)), math.min(cnd_factor,1),wound_power)
			end
			if wound_power > 0.01 then
				wound_power			= math.max(wound_power * a_power * 1.25, 0.2)
				local wound_size	= actor:get_actor():add_wound(wound_power,hit.wound)
--				logc("--wound_power [%s] wound_size [%s] a_power [%s]",wound_power,wound_size,a_power)
			end
			lc_prob = lc_prob or actor:get_actor().condition.last_chance_probability
			actor:get_actor().condition.last_chance_probability = 0
			level.add_call(function()
				if device().frame > frame then
					if lc_prob then actor:get_actor().condition.last_chance_probability = lc_prob ; lc_prob = nil end
					return true
				end end
			,function() end)
		end
		tgt:hit(h)
		if h.impulse > 0 then
			local function throw(id,f)
				local obj = level.object_by_id(id)
				local ps = obj and obj:get_physics_shell()
				if ps then ps:apply_force(f.x,f.y,f.z) end
			end
			hit_drop = true
			local force = vector():invert(h.direction):mul(data.hit.impulse*2000)
			m_timers.start_quick_timer(250,throw,obj:id(),force)
		end
	end
	if not (tgt:is_entity_alive() and tgt:alive()) then return end
	local mult = 50 * sini:r_float("alife", "time_factor")	--// ���������� ����������� ���������� ��������� �������
	local eart = obj:get_artefact()
	local restore = eart.radiation_restore_speed
	if not math.is_zero(restore) then
		if math.is_positive(restore) then
			restore = math.max(restore - sini:r_float("actor_condition", "radiation_v")*2,restore/3)
			send_hit(tgt,hit.radiation,restore*mult,obj)
		else	-- ��������� ��������
			tgt.radiation = (restore*mult*0.5)
		end
	end
	restore = eart.power_restore_speed
	if math.is_negative(restore) then
		tgt.power = restore*mult
	else
		tgt.power = -tgt.power*math.random(50)/100
	end
	restore = eart.bleeding_restore_speed
	if math.is_negative(restore) then
		tgt.conditions:add_wound(-restore*mult,data and data.hit.type or hit.wound)
	end
	if not tgt:is_actor() then return end
	restore = eart.psy_health_restore_speed
	if math.is_negative(restore) then
		local h = hit()
		h.type = hit.telepatic
		h.power = 1
		h.draftsman = obj
		local immun = tgt:get_actor():hit_test(h,false)
		tgt.psy_health = math.max(restore*mult*2*immun,-tgt.psy_health*0.95)
	elseif restore > 0 then
		tgt.psy_health = (restore*mult*1)
	end
	if (hit_drop or math.random() < 0.25) and init then
		if obj:id() == captured_art then m_timers.start_quick_timer(100, function() actor:get_actor():detach_capture_object() end) else actor:mark_item_dropped(obj) end
		drop_active_item()
		change_stor_take_ids(obj:id(), actor_id)	-- ����������, ��� ��� �������� �����
		get_hud():AddCustomStatic("karlan_info",true,"st_damn_it")
	end
	--// Karlan->Rulix: �������� ��� ���-�� ������������ � ����� �������� ����� �������, �������� ������ ��������� � ����������� �� �������� � ���� ����, � �� � �� ������ ���������� ��� � ���� ��������
	level.add_cam_effector('camera_effects\\'..cameffs[math.random(#cameffs)]..'.anm', generate_cam_effector_id(), false, "")
	level.add_pp_effector('fire_hit.ppe', generate_pp_effector_id(), false)
--	m_sound.play_2d('actor\\pain_'..math.random(1,8))
end

local function blowout(art,initiator,power,pos)
	hit_drop = nil
	local id = art:id()
	local data = fill_hit_data(art:section())
	local radius = data.radius or 4.0
	local init_id = initiator and initiator:id()
	local pos = pos or art:position()
	particles_object(data.particle):play_at_pos(pos)
	data.sound:play_no_feedback(art, sound_object.s3d, 0, pos, 1)
	if data.particle_hit then particles_object(data.particle_hit):play_at_pos(pos) end
	for iid,t in pairs(db.storage) do
		if iid ~= id and (t.stype == modules.stype_stalker or t.stype == modules.stype_mobile or t.stype == modules.stype_item or iid == actor_id) then
			local obj = level.object_by_id(iid)
			if not obj:parent() then
				local dist = obj:position():distance_to(pos)
				if dist <= radius then
					local hit_power = iid==init_id and 1 or math.min(1,1.2-dist/radius)
					hit_object(art,obj,data,hit_power*power,iid==init_id)
				end
			end
		end
	end
end

local function modify_art_param(name, value, weak, reward_mode)
	local percent = 0
	if value~=0 and value~=1 then
		local p = (name:find('immunity') and 1-value or value)*(param_deviation/100)
		p = p * math.random(-100,weak and 0 or 100)/100	--// Rulix: ����� �������, ��� � ������� ����� �� ������� ������ ������ ��������
		if weak and name:find('immunity') then
			p = p * -1
		end
		percent = percent+p
		if math.abs(percent) < EPS_S then percent = 0 end --// Karlan: � ������ ���������� ������������ � EPS, � ������� � EPS_S
	end
	local res_val = (value + percent)
	if not reward_mode then
		--// Karlan: ��� �������� ����� ������� �������� � *_max_power
		if (name == 'radiation_restore_speed') and cfg_get_number(gini, level.name(), 'radiation_max_power', 0) > 0 then
			local rad_v = sini:r_float('actor_condition', 'radiation_v')
			local p = math.max(0.001 * math.random(), rad_v*0.15)
			if math.random() < 0.5 then p = p*0.5 end
			if math.random() < 0.07 then p = -0.75*p end
			if value < 0 then
				res_val = p < 0 and math.min(res_val,p) or math.min(res_val+p,res_val*0.5)
			elseif value > 0 then
				res_val = p > 0 and math.max(res_val,p) or math.max(res_val+p,res_val*0.5)
			else
				res_val = p
			end
		end
		if (name == 'psy_health_restore_speed') and (value >= 0) and cfg_get_number(gini, level.name(), 'psy_max_power', 0) > 0 then
			local psy_v = sini:r_float('actor_condition', 'psy_health_v')
			local p = psy_v * math.random(3,15)/10
			if math.random() < 0.1 then p = -1*p end
			res_val = res_val-p
		end
		if (name == 'chemical_burn_immunity') and (value >= 1) and cfg_get_number(gini, level.name(), 'acidic_max_power', 0) > 0 then
			local p = 0.2 * math.random()
			res_val = res_val+p
		end
	end
	if math.abs(res_val) < EPS_S then res_val = 0 end
	return res_val
end
--//-----------------------------------------------------------------
--// Callbacks
--//-----------------------------------------------------------------
local function artefact_take(art,initiator,power,pos)
	local id = art:id()
	local stor = stor_take_ids[id]
	stor_take_ids[id] = true
	write_pstor('art_charge_cnd', nil, art)
	if not (stor or level.main_input_receiver() and initiator==actor) then
		blowout(art,initiator,power*art_hit_mult(art),pos)
		local is_charged = remove_pstor("art_charged",art)
		if not is_charged then
			m_ranks.add_art_count(initiator,art)
		end
	end
	if initiator == actor and is_number(stor) and stor ~= actor_id then
		-- ����� ��������� ����� ���
		local npc = level.object_by_id(stor)
		if npc and npc:is_stalker() and npc:alive() and npc:relation(actor) ~= game_object.enemy and distance_between(actor,npc) < 30 then
			local gw = -(20 + art:get_artefact():GetAfRank()*50)
			ai_abuse.add_abuse(npc,math.random(40,80)/10)
			npc:change_goodwill(gw, actor)
			local gulag = m_gulag.get_npc_gulag(npc)
			if gulag and m_gulag.is_gulag_online(gulag.name) then
				gulag:change_goodwill(math.ceil(gw/4), actor)
			end
			actor:change_character_reputation(-50)
			if remove_pstor("art_taker_heal",npc) then	-- ����� ������� �������, � ����� ���� ��������
				m_sound.stop_sound_play(npc,2)
				change_npc_goodwill(npc, actor, 150-600)
				local comm_gw = 50 * npc:sympathy()
				actor:change_community_goodwill(npc:character_community(),-comm_gw)
			elseif ai_wounded.is_wounded(npc) then
				write_pstor('art_taker_heal', false, npc)
			end
		end
	end
	return not stor
end
local function item_take(e)
	if not e.item:is_artefact() then return end
	artefact_take(e.item,actor,1,e.pos)
end
local function item_capture(e)
	if not e.item:is_artefact() then return end
	captured_art = e.item_id
	artefact_take(e.item,actor,1)
end
function npc_take_art(npc, art)
	if artefact_take(art,npc,1) and (hit_drop or math.random() < (ai_wounded.is_wounded(npc,true) and 0.75 or 0.35)) then
		change_stor_take_ids(art:id(), npc:id())	-- ����������, ��� ��� �������� ���
		return true
	end
end

local danger_show, danger_flash = nil, nil
local function make_test_hit(art,tgt)
	if stor_take_ids[art:id()] then return 0 end
	local data = fill_hit_data(art:section())
	local multiplier = hit_mult_base + hit_mult_var * 1
	local h = hit()
--	h:bone("bip01_spine")
	h.type = data.hit.type
	h.power = data.hit.power*art_hit_mult(art)*multiplier * gd_art_damage_factor[level.get_game_difficulty()]
	h.draftsman = art
	local outfit		= actor:get_current_outfit()
	local outfit_factor	= h.type == hit.strike and outfit and cfg_get_number(sini,outfit:section(),'strike_protection_art')
	local stor_protec	= outfit_factor and outfit:get_outfit().strike_protection
	if stor_protec then outfit:get_outfit().strike_protection = stor_protec*outfit_factor end
	local res = tgt:get_actor():hit_test(h,true)
	if stor_protec then outfit:get_outfit().strike_protection = stor_protec end
	return res
end
function update_hit_danger(e)
	local obj = e.item or level.object_looking_at()
	if not obj then
		danger_show = nil
	elseif danger_show then
		local damage = make_test_hit(obj,actor)
		local factor = (damage-0.2)/0.8
--		logc("^art damage[%s] = [%s] factor = [%s]",obj:name(),damage,factor)
		local art_static = get_main_window():GetStatic('art_danger')
		local clr_min = fcolor():set(GetARGB(255,116,228,0))
		local clr_mid = fcolor():set(GetARGB(255,184,164,0))
		local clr_max = fcolor():set(GetARGB(255,192,4,0))
		art_static:SetColor(fcolor():lerp(clr_min,clr_mid,clr_max,math.clamp(factor,0,1)):get())
		if factor <= 1 then
			art_static:StopColorFlashing(); danger_flash = nil
		elseif not danger_flash then
			art_static:SetColorFlashing(GetARGB(150,0,0,0), 0.33, false, true, true, 'ui_flash_warn'); danger_flash = true
		end
	end
	if not danger_show then
		get_main_window():GetStatic('art_danger'):Show(false)
		event("actor_update"):unregister(update_hit_danger)
	end
end
local function saw_obj(e)
	if not (e.item and e.item:is_artefact()) then
		if danger_show then
			danger_show = nil
			update_hit_danger()
		end
	else
		if not danger_show then
			danger_show = true
			local art_static = get_main_window():GetStatic('art_danger')
			art_static:Show(true)
			art_static:ResetColorAnimation()
			event("actor_update"):register(update_hit_danger, {__period = 1000, __skip_first_call = true})
		end
		update_hit_danger(e)
	end
end
--//-----------------------------------------------------------------------------------------------
--// Global functions
--//-----------------------------------------------------------------------------------------------
function get_art_params(obj)
	local stor = read_pstor('_art', {}, obj)
	assert(is_table(stor))
	return stor
end

function get_art_params_full(obj)
	local res = table.copy(read_pstor('_art', {}, obj))
	if is_function(obj.is_server_object) then
		local section = obj:section_name()
		local abs_section = cfg_get_string(sini,section,'hit_absorbation_sect')
		for _,varname in ipairs(art_params) do
			if res[varname] == nil then
				res[varname] = cfg_get_number(sini,abs_section,varname) or cfg_get_number(sini,section,varname)
			end
		end
	else
		local eart = obj:get_artefact()
		for _,varname in ipairs(art_params) do
			if res[varname] == nil then
				local rec = eart[varname] and eart or eart.immunities
				res[varname] = rec[varname]
			end
		end
	end
	return res
end

function set_art_params(obj,params)
	if obj.is_server_object then
		assert(is_table(params))
		write_pstor('_art', params, obj)
		return
	end
	local eart = obj:get_artefact()
	local stor = params or read_pstor('_art', nil, obj)
	if is_table(stor) then	-- load
--		log("*Load art params for (%s) : %s",obj,stor)
		for varname,value in pairs(stor) do
			local rec = eart[varname] and eart or eart.immunities
			rec[varname] = value
		end
	else			-- setup
		local seed = remove_pstor('art_modify_seed',obj) or (sim:seed() + obj:id() + 521)
		randomseed_execute(seed,function()
			local weak = not remove_pstor('art_modify_normal',obj) and has_info('tutorial_end')
			local reward = remove_pstor('art_modify_reward',obj)
			stor = {}
			log("SETUP art params for (%s) seed %s reward [%s] weak [%s]",obj,seed,reward,weak)
			for _,varname in ipairs(art_params) do
				local rec = eart[varname] and eart or eart.immunities
				local cur = rec[varname]
				local mod = modify_art_param(varname, cur, weak, reward)
				if cur ~= mod then
--					log("#set [%s] : %s -> %s",varname,rec[varname],mod)
					stor[varname] = mod
					rec[varname] = mod
				end
			end
		end)
	end
	write_pstor('_art', stor, obj)
end
--//-----------------------------------------------------------------
--// External
--//-----------------------------------------------------------------
function change_stor_take_ids(obj, add)
    local id = obj
    if is_userdata(id) then id = get_id(obj) end
    stor_take_ids[id] = add and add or nil
end
--//-------------------------
function npc_should_take_art(npc,art,mine)
	if mine then
		return stor_take_ids[art:id()] == npc:id()
	end
	if stor_take_ids[art:id()] ~= actor_id then
		return true
	end
	return get_npc_general_goodwill(npc,actor) < -30
end
function is_art_taker(obj)
	local id = is_userdata(obj) and get_id(obj) or obj
	for _,val in pairs(stor_take_ids) do
		if val == id then
			return true
		end
	end
	return false
end
--//-------------------------
function artefact_death(art,parent)
	-- // TODO: ���������� ���������
	log("artefact_death(%s|%s)",art,parent)
	if is_number(art) then
		art = level.object_by_id(art)
		if not art then return end
	end
	if parent and parent:is_actor() then
		m_timers.start_quick_timer(5000, this.artefact_death, art:id())
		local flags = art:get_inventory_item_flags()
		flags:set(global_flags.FCanTake,false)
		art:set_inventory_item_flags(flags)
		parent:drop_item(art,5)
		return
	end
	if not parent or parent:is_stalker() and not parent:alive() then
		blowout(art,nil,0.7)
	end
	sim:release_by_id(art:id())
end
--//-------------------------
--// ��� ����������, �� ������� ���������
function spawn_independent_artefacts(amount)
--	local vertex = math.random(game_graph():vertices_count())
	local rc = 0
	local game_vertex,game_vertex_id
	for i = 1, amount do
		local level = random_weighed(spawn_independent_levels)
		local game_verts = game_vertex_table[level]
		repeat
			game_vertex_id = math.random(game_verts[1],game_verts[2])
			game_vertex = game_graph():vertex(game_vertex_id)
			verify_warning(game_vertex:level_id()==sim:level_id(level),'%s %s',level,game_vertex_id)
			rc = rc + 1
		until m_anoms.inside_level_boundaries(game_vertex:level_point(),1,level) or rc > 1000
		local spawn_section = random_weighed(spawn_independent_sections)
		local spawned_obj = sim:create(spawn_section, game_vertex:level_point(), game_vertex:level_vertex_id(), game_vertex_id)
		spawned_obj = sim:object(spawned_obj.id)
		sim:assign_death_position(spawned_obj,game_vertex_id,nil)
		local position = spawned_obj.position ; position.y = position.y + 0.3
		spawned_obj:set_position(position)
		log("spawned ind '%s' on level '%s' pos [%s][%s] dist [%s] rc %s",spawn_section,level,spawned_obj.m_level_vertex_id,game_vertex_id,game_vertex:level_point():distance_to(spawned_obj.position):round(1),rc)
		clog("spawned ind [%s] '%s' on level '%s' pos [%s][%s] dist [%s] rc %s",game.get_game_time(),spawn_section,level,spawned_obj.m_level_vertex_id,game_vertex_id,game_vertex:level_point():distance_to(spawned_obj.position):round(1),rc)
		write_pstor('art_modify_seed', math.random(65536), spawned_obj)
		write_pstor('art_modify_normal', true, spawned_obj)
--		add_map_spot(spawned_obj.id, 'secondary_task_location_complex_take_reward', spawned_obj:name())
	end
end
--//-------------------------
--// �� ��������, ��� ���� ������� ������� � ������ ������ �� �����, ������� �� �� ����� ��� ������ ����
function spawn_artefacts_on_current_level(amount, surge)
	-- // Rulix: ����� ��������� ������������� ����������
	local spawn_arts = {}
	local prob_add = spawn_prob_add[level.name()]
	local function get_artefact_to_spawn(section,verify)
		if spawn_arts[section] == nil then
			spawn_arts[section] = false
			if cfg_get_number(sini, section, "max_artefact_count", 1) > 0 then
				local arts = parse_spawns(sini:r_string(section, 'artefacts'))
				if #arts > 0 then
					local sum = 0
					for i,t in ipairs(arts) do
						local af_rank = cfg_get_number(sini,t.section,'af_rank',0)
						if prob_add and is_number(prob_add[af_rank]) then
							t.prob = math.max(0,t.prob + prob_add[af_rank]/100)
						end
						sum = sum + t.prob
					end
					if sum ~= 1 then
						for i,t in ipairs(arts) do
							t.prob = t.prob/sum
						end
					end
					spawn_arts[section] = arts
				end
			end
		end
		if spawn_arts[section] == false then
			return
		end
		if verify then
			return true
		end
		local rnd = math.random()-EPS;
		local prob_threshold = 0;
		for _,t in ipairs(spawn_arts[section]) do
			prob_threshold = prob_threshold + t.prob;
			if rnd < prob_threshold then
				return t.section
			end
		end
	end
	local count, run = 0, 0
	local anoms = {}
	for name,obj in pairs(m_anoms.get()) do
		if not anom_no_art[obj:clsid()] and (obj:get_anomaly().zone_state ~= 4 or obj:get_anomaly():is_on_off_mode()) then
			if get_artefact_to_spawn(obj:section(),true) then
				table.insert(anoms,obj)
			end
		end
	end
	log('spawn_artefacts_on_current_level START target = %s, anoms = %d', amount, #anoms)
	clog('spawn_artefacts_on_current_level [%s] %s START target = %s, anoms = %d',level.name(),game.get_game_time(), amount, #anoms)
	table.sort(anoms,function(a,b) return a:level_vertex_id() < b:level_vertex_id() end)
	table.shuffle(anoms)
	::proceed_spawn::
	run = run + 1
	for _,obj in ipairs(anoms) do
		local art_count	= 1	--cfg_get_number(sini, obj:section(), "min_artefact_count", 1)
--		local max_count	= cfg_get_number(sini, obj:section(), "max_artefact_count", 1)
--		art_count = art_count >= max_count and max_count or math.random(art_count,max_count)
		local radius = obj:clsid() == clsid.zone_hairs_s and 0.8 or obj:get_shape_radius()
		local pos = obj:position()
		local lvid = obj:level_vertex_id()
		local vpos = level.vertex_position(lvid)
		for c=1,art_count do
			local spawn_section = get_artefact_to_spawn(obj:section())
			if not spawn_section then warning("cannot pick art [%s] [%s]",obj:section(),sim:level_name()) break end
			local spawned_obj
			--// Rulix: ������� ������ �� �� �����, ����� ������ ����������� ������������ ��� ����� � ������ ����-���� � ��������
			--// � ��������, ���� ���� �������� ��� �������, ��������� �� ����� ������ ����
			for i=1,3 do
				local spawn_pos = vector():set(pos):add(vector():set(math.random(-10,10)*0.1*radius,0,math.random(-10,10)*0.1*radius))
				local lv = level.valid_vertex_position(spawn_pos) and level.vertex_id(spawn_pos) or -1
				if lv > 0 and valid_level_vertex_id(lv) and math.abs(vpos.y-level.vertex_position(lv).y) > 2.0 then
					lv = level.position_vertex(lvid,spawn_pos)
				end
				if lv > 0 and valid_level_vertex_id(lv) and math.abs(vpos.y-level.vertex_position(lv).y) < 2.0 then
					spawn_pos = level.vertex_position(lv)
					spawn_pos.y = math.max(spawn_pos.y + 0.5,pos.y)
					spawned_obj = sim:create(spawn_section, spawn_pos, lv, level.game_vertex(lv))
					break
				end
			end
			if not spawned_obj then
				local spawn_pos = vector():set(pos)
				if level.position_inside_vertex(pos,lvid) then
					spawn_pos.y = math.max(level.vertex_position(lvid).y + 0.5,pos.y)
				else	-- �������� ��� �� �����
					spawn_pos.y = spawn_pos.y + radius*0.5
				end
				spawned_obj = sim:create(spawn_section, spawn_pos, lvid, obj:game_vertex_id())
			end
--			add_map_spot(spawned_obj.id, 'secondary_task_location_complex_eliminate_squad', spawned_obj:name()..' '..obj:section())
			clog('*** spawn [%s|%s] in [%s] lvid [%s]',spawn_section,spawned_obj.id,obj:name(),lvid)
			write_pstor('art_modify_seed', math.random(65536), spawned_obj:cast())
			write_pstor('art_modify_normal', true, spawned_obj:cast())
			count = count + 1
			if count >= amount then break end
		end
		if count >= amount then break end
	end
	if count < amount and run < 5 then
--		goto proceed_spawn
	end
	log('spawn_artefacts_on_current_level FINISH result = %s', count)
	clog('spawn_artefacts_on_current_level FINISH result = %s', count)
end

function is_working(art)
	if art:id() == captured_art then
		return true
	end
	local parent = art:parent()
	if not (parent and parent:is_actor()) then
		return false
	end
	local item = parent:active_item()
	return item and item:id() == art:id() or parent:is_on_belt(art)
end

local wf_buf = {}
function work_factor(sect)
	if not wf_buf[sect] then
		wf_buf[sect] = sini:r_float(sect,'cond_dec_work_factor')
	end
	return wf_buf[sect]
end

--// ��������������� ��������� ���������, ����� �� ��������� � �������� ������������ ��������
local function get_restore_speed(cond_dec,anom_section)
	local dec = 8 + cond_dec/4.5	-- 5
	local restore_speed = 1/(dec*60*60)
	if string.find(anom_section,'_weak') then	--cfg_get_number(sini,anom_section,'artefact_restore',1.0)
		restore_speed = restore_speed * 0.66
	elseif string.find(anom_section,'_strong') then
		restore_speed = restore_speed * 1.33
	end
	return restore_speed
end
function try_restore_art_condition(art,delta,anoms)
	local around
	if art:is_game_object() then
		local cnd = art:condition()
		if cnd < 0.1 then	-- ����������� ���������
			return false
		end
		local anom_type = art:get_artefact().anomaly_type	--cfg_get_number(sini,art:section(),'anomaly_type')
		if anom_type == INVALID_ID then
			return false
		end
		local pos,dist,radius = art:center()
		for name,anom in pairs(m_anoms.get()) do
			local zone = anom:get_anomaly()
			if zone.anomaly_type == anom_type and zone.zone_state ~= 4 then
				dist = anom:position():distance_to(pos)
				radius = zone.radius + 0.5
				if dist < radius then	-- ������
					if stor_take_ids[art:id()] then
						local restore_start_cnd = read_pstor('art_charge_cnd', nil, art)
						if restore_start_cnd == nil and cnd < 0.9 then
							write_pstor('art_charge_cnd', cnd, art)	--// �������� �������
							log("-charge start [%s][%s]",art:name(),read_pstor('art_charge_cnd', nil, art))
						elseif restore_start_cnd and cnd - restore_start_cnd > 0.1 then
							log("-charge finish (online) [%s] [%s]->[%s]",art:name(),restore_start_cnd,cnd)
							write_pstor('art_charge_cnd', nil, art)	--// ���������� ������
							write_pstor('art_charged', true, art)
							stor_take_ids[art:id()] = nil
						end
					end
					--// RESTORE
					if cnd < 1 then
						local restore_speed = get_restore_speed(art:condition_dec(),anom:section())
--						log("Restore(online)(%s|%s) [%s] -> [%s] delta %s restore_speed %s",art:name(),anom:section(),cnd,cnd+restore_speed*delta,delta,restore_speed*3600)
						art:set_condition(cnd + restore_speed*delta)
					end
					return true
				elseif dist < radius + 5 then -- ������
					around = true
				end
			end
		end
	else
		local iitem = art:get_cse_alife_inventory_item()
		local cnd = iitem.condition
		if cnd < 0.1 then	-- ����������� ���������
			return false
		end
		local anom_type = cfg_get_number(sini,art:section_name(),'anomaly_type')
		if not anom_type then
			return false
		end
		local pos,dist,radius = art.position
		for _,anom in ipairs(anoms) do
			local zone = anom:get_cse_alife_anomalous_zone()
			if zone and zone.anomaly_type == anom_type then
				dist = anom.position:distance_to(pos)
				radius = zone:shape_radius() + 0.5
				if dist < radius then	-- ������
					--// RESTORE
					if cnd < 1 then
						local restore_speed = get_restore_speed(cfg_get_number(sini,art:section_name(),'cond_dec',0),anom:section_name())
--						log("Restore(offline)(%s|%s) [%s] -> [%s] delta %s restore_speed %s",art:name(),anom:section_name(),cnd,cnd+restore_speed*delta,delta,restore_speed*3600)
						iitem.condition = math.clamp(cnd + restore_speed*delta, 0, 1)
						if stor_take_ids[art.id] then
							local restore_start_cnd = read_pstor('art_charge_cnd', nil, art)
							if restore_start_cnd and iitem.condition - restore_start_cnd > 0.1 then
								log("-charge finish (offline) [%s] [%s]->[%s]",art:name(),restore_start_cnd,iitem.condition)
								write_pstor('art_charge_cnd', nil, art)	--// ���������� ������
								stor_take_ids[art.id] = nil
							end
						end
					end
					return true
				elseif dist < radius + 5 then -- ������
					around = true
				end
			end
		end
	end
	return around
end

function surge_art_effect()
	local del = {}
	for i,obj in ipairs(sim:get_free_items()) do
		if IsArtefact(obj) then
			if obj.online then
				local art = level.object_by_id(obj.id)
				if art then
					art:get_artefact():Blink(false, true, 2000)
					art:get_artefact():SwitchLights(false)
					art.visible = false
				end
				obj.m_flags:set(global_flags.flCanSave,false)
				table.insert(del,obj.id)
			else
				sim:release(obj)
			end
		end
	end
	m_timers.start_quick_timer(3000,function(del) for i,id in ipairs(del) do sim:release_by_id(id) end end,del)
end

--//-----------------------------------------------------------------------------------------------
--// Initialization module
--//-----------------------------------------------------------------------------------------------
local function presets(e)
	assert(loaded, "not init!")

	event("item_take"):register(item_take)
	event("obj_capture_init"):register(item_capture)
	event("obj_capture_release"):register(function(e) captured_art = nil end)
	event("actor_saw_obj"):register(saw_obj)

	if e.is_first_start then	--// ����� � ������ ����
		m_timers.start_quick_timer(prosectors_freeze_start[script_name()],
			function()
				clog('initial spawn [%s] %s',level.name(),game.get_game_time())
--				sim:release_artefacts()
				sim:iterate_objects(
					function(obj)
						local anom = obj:get_cse_alife_anomalous_zone()
						if anom and math.random() > 0.94 then
							anom:spawn_artefacts(0,math.random(25,80)/100)
						end
					end
				)
--[[			local rnd_cnt = {}
				for glevel in game_graph():levels() do
					rnd_cnt[glevel.level:name()] = math.random(4)
				end]]
			end
		)
	end
end
--//-------------------------
function init()
	event("presets"):register(presets)
	event("object_release"):register(function(e) stor_take_ids[e.obj_id] = nil end)	-- ��� ����� ������ �����������, ��� ��� �������� ����� ����������� �� ������
	loaded = true
end
--//-----------------------------------------------------------------------------------------------
--// Serialization module
--//-----------------------------------------------------------------------------------------------
function save(pk)
    if not next(stor_take_ids) then pk.init = true end
    pk.stor_take_ids = stor_take_ids
--    table.print(pk, 'pk')
end
function load(pk)
    stor_take_ids = pk.stor_take_ids
end
--//-----------------------------------------------------------------------------------------------
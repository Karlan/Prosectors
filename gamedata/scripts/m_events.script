--[[ ----------------------------------------------------------------------------------------------
 File       : m_events.script 
 Description: Event-Driven Model
 Copyright  : 2017 � Prosectors Project
 Author     : ������ (�� ����������: Artos, xStream)
--]] ----------------------------------------------------------------------------------------------

--[[
--// ����������� �� �������� �������:
~> [0] - ���������� ������ ������� ������! �� ������� � �� ���������!
~> [1] - ���������� ������ � ��������� ��������� (������, �������, ...). �� ���������� ���������.
~> [2] - ������������(��������������) ��� ������ ����, �� �� ������� ���� ��� �������.
~> [3] - ������������(��������������) ��� ����, ����������� ��������� �������/����� �� ���� ����.
~> [4] - ������������(��������������) ��� ����, ����������� ��������� �������/����� � ��������� ��������.
~> [-] - ��������� �� �������������. �������� �� ���������, ����������� ������ ������ � ������������!
~> [?] - ���������� �� ����������� ���������� �������� ����������.
~> [x] - ������, ����������� � ���������� (�.�. ��������������)

--//------------------------------------------------------------------------------|
--//------  |  MODULE NAME  |  MODULE MARKER  |     MODULE DESCRIPTION    | ------|
--//------------------------------------------------------------------------------|
--//------  | m_events      |       [0]       | event-driven system       | ------|
--//------  | m_storage     |       [0]       | universal storage system  | ------|
--//------  | m_inventory   |       [0]       | inventory functions       | ------|
--//------  | m_ammo        |       [3]       | operation with ammunition | ------|
--//------  | m_items       |       [3]       | operation with items      | ------|
]]
--//-----------------------------------------------------------------
--// VARIABLEs
--//-----------------------------------------------------------------
local flag_init = false
local events = {}
local modules_list = {}
--//-----------------------------------------------------------------
--// Flags for enable output of the debug information
--//-----------------------------------------------------------------
_DEBUG_ = false --// file under debugging?
_DEBUG_GSC_ = _DEBUG_ and false --// only for scripts by GSC
--//-----------------------------------------------------------------
--//-----------------------------------------------------------------------------------------------
--// Local Functions
--//-----------------------------------------------------------------------------------------------
local function load_module(fname, no_assert)
    prefetch(fname)
    local module = _G[fname]
    if module then
        assert(type(module) == 'table', "load_module:='"..tostring(fname).."'~wrong, must be a table")
    else
        if not no_assert then
            log("!#ERROR: load_module: cannot find module "..fname)
            assert(false) 
        end
    end
    return module
end

local function init_module(module,fname)
    if module then
        assert(type(module) == 'table', "Initialize_Module:='"..tostring(fname).."'~wrong, must be a table")
        log('trying call function %s.init() in _G: <exist = [%s] type = [%s]>', fname, module.init~=nil, type(module.init))
        if type(module.init) == 'function' then
            module.init()
        end
    else
        log("!#ERROR: init_module: cannot find module "..fname)
        assert(false) 
    end
end

--// ������������� ���� � ��������� ��������� ������
local function grab_modules(_dir, modules_path, dir_include)
    local exclude_modules = {} --// ������, ������� �� ����� ����������
    local include_modules = { --// ������ ��������� ������ ����������, ��������� ������� �������� ����
        --// name's/folder's [includes]
        bind_zones      = true,
        ai_modules      = true,
        ai_animation    = true,
        ai_kamp         = true,
        ai_combat_ignore = true,
        debug_func      = true,
        --// pattern's
        ['m_']          = true,
        ['manager_']    = true,
        ['sr_']         = true,
    }

    for file in lfs.dir(_dir) do
        if file ~= "." and file ~= ".." then
            local name,ext = file:match('(.*)%.(.-)$')
            if not name and not ext then --// Karlan: recurse
                grab_modules(_dir .. file .. '\\', modules_path, dir_include or include_modules[file] == true)
            end
            if ext == "script" then
                assert(name ~= 'storage' and name ~= 'event', 'you try use protected name for module') --// Karlan: �������� ����� ������� ������?
                verify_warning(modules_path[name] == nil, 'duplicate script [%s]: [%s] and [%s]',file,_dir,modules_path[name]) --// Rulix: ����� ���������
				modules_path[name] = _dir
                local prefix, prefix_greedy = name:match('^(.-_)'), name:match('^(.*_)') --// Karlan: ������, ���� ������ �������, � ������� ���, �� ���� �������� ���������� ��� �������� ��������, ��� ����������� �������� �� ��������� ������� + ��������� ������� � �����������
                if not exclude_modules[name] and (dir_include or include_modules[prefix] or include_modules[prefix_greedy] or include_modules[name]) then
                    local module = load_module(name)
                    if PROSECTORS_DEBUG then
                    if module._DEBUG_ then module.reboot = function() debug.reboot(name) end end --// �� ��������� ��� ��� ��� ��� �������, �� �� ��� ���� ��������, ���� ��������� ������, ��� ��� ��� ��� ��� ������, ������������� ��� ������, ��� ��� ���� ������������ � ������, ���� ��������� ��, ��� ������ ��������
                    if module._DEBUG_ then module.clog = function(str, ...) wfile(name..'.log', str, ...) end end
                    end
                    table.insert(modules_list,name)
                    -- log('module [%s] is [%s], and successfully connected to the system', name, true)
                -- else
                    -- log('module [%s] is [%s], and NOT connected to the system', name, include_modules[name] or false)
                end
            end
        end
    end
end

local function is_equal(ev, func, userobj, ...)
    if rawequal(func, ev.func) then
        local arg = table.safe_pack(...)
        if (arg.n > 0) and (ev.arg.n > 0) then
            if not table.equal(arg, ev.arg, 1) then
                return false
            end
        end
        if userobj ~= nil then
            if type(ev.uo) ~= type(userobj) then return false end
            if is_table(userobj) then
                return table.equal(userobj, ev.uo, 1)
            end
            return rawequal(userobj, ev.uo)
        end
        return true 
    end
	return false
end
--//-----------------------------------------------------------------------------------------------
--// Global Functions
--//-----------------------------------------------------------------------------------------------
function iterate_modules(_func)
	assert(#modules_list > 0, 'modules was not loaded!')
	for i,name in ipairs(modules_list) do
		_func(_g[name],name)
	end
end
function init_language()
	local text_fname	= "text_"..get_console():get_string("g_text_language")
	_G._ltext			= load_module(text_fname,false)
	_G[text_fname]		= nil
--	logc("--- init_language [%s]",text_fname)
end
--//-----------------------------------------------------------------
--// Initialize Module
--//-----------------------------------------------------------------
function init()
    if flag_init then return end
    _G.event = this.event
    m_storage.load_storage_1() --// Karlan: �� ����� ���� ����� �� �������������� ������ ����������, ��������� ��� ����� �� �����, �� ��� ���� �������, ����� ���������� �� �������, ��������� ��� ������ ������ LUA_TTABLE (�� ����� ���� nil ����������������� � LUA_TTABLE, ������� ����� ����� ����������� � nil, ���� ������ �� ����������), ��� ��� �������� ����� ������� ��������� �������
    event("actor_spawn"):register(presets_trigger,{prior = math.huge})
    flag_init = true
end
function presets_trigger(e)
    event("presets"):once(true):trigger(e)
end
function start()
	init_language()
	grab_modules(GAME_SCRIPTS,{})	--// Load
	iterate_modules(init_module)	--// Init
--	clog("modules_list: %s",modules_list)
end

class "event"
function event:__init(name)
    if name and name ~= "" then
        if not events[name] then  
            events[name] = {}
            log("*event:init:=["..name.."] ~> added:[+]") 
        end
    self.__evt_name = name
    else
        log("!event:init:name can not be empty: [%s]", "error!")
        exit()
    end
end

function event:name()
    return self.__evt_name 
end

function event:register(func, userobj, ...)
    --// Karlan: todo: �� �������� ����� � ��������� ���� ������ �������� ��� ������� � ������ ������� ���, ����� ��� ��������� ��� �������, � �� ������
    if type(func) == 'function' then
        self:unregister(func)
        local pos = #events[self.__evt_name] + 1 --// GeJorge @ ������� ������������� �������, ����� ����� ���� ����������� �� ��� ����������� ������ ����������
        table.insert(events[self.__evt_name], {func = func, uo = userobj, _func = function(data, ...) return func(data, userobj, self, ...) end, pos = pos, arg = table.safe_pack(...)}) --// GeJorge @ ������� pos
        --// GeJorge in @ ���������� ����������� ������� �� ��������������
        local function sort_rule(a, b)
           local a_prior = is_table(a.uo) and a.uo.prior or 1
           local b_prior = is_table(b.uo) and b.uo.prior or 1
           return a_prior > b_prior or a_prior == b_prior and a.pos < b.pos
        end
        table.sort(events[self.__evt_name], sort_rule)
        --// GeJorge out
--        logt("*event:register:=["..self.__evt_name.."],#events=["..tostring(#events[self.__evt_name]).."]:[+]")
    else
        warning("bad argument #1 to \"event:register\" (function excepted, got %s)", type(func))
    end
end

function event:registered(func, userobj, ...)
    log("*event:registered:=["..self.__evt_name.."],#events=["..tostring(#events[self.__evt_name]).."]:[?]")
    for _,v in ipairs(events[self.__evt_name]) do
        if is_equal(v, func, userobj, ...) then
            return true
        end
    end
    return false
end

function event:unregister(func, userobj, ...)
    for i,v in ipairs(events[self.__evt_name]) do
        if is_equal(v, func, userobj, ...) then
            table.remove(events[self.__evt_name], i)
--            logt("*event:unregister:=["..self.__evt_name.."],#events=["..tostring(#events[self.__evt_name]).."]:[x]")
            --// Karlan: �� ����� �� �������, ��� ��� ���� ���� ����������� ��������� ��� ���������� ������� �� ������ ��� ����������� �� ��������� ����������
        end
    end
end

-- local trigger_dbg = {actor_spawn=true,presets=true}
function event:trigger(data)
	local _ret
    if next(events[self.__evt_name]) then
        if self.__break ~= true then
            for i,v in ipairs(events[self.__evt_name]) do
                if v.uo and is_table(v.uo) and v.uo.__period then --// Karlan: ��� ����������� �� ���� ������� ���� ����������� ������� ������ ������������
                    v.uo.__time_last = v.uo.__time_last or 0
                    if time_global() > v.uo.__time_last then
                        if not is_number(v.uo.__period) then
                            v.uo.__period = math.random(v.uo.__period[1], v.uo.__period[2])
                        end
                        v.uo.__time_last = time_global() + v.uo.__period
						if not v.uo.__skip_first_call then
							v._func(data)
						else
							v.uo.__skip_first_call = false
						end
                    end
                else
					local ret = v._func(data) --// make __skip_first_call if need
					if ret ~= nil then _ret = ret end
                end
                if (_DEBUG_ and is_table(v.uo) and v.uo.__print) 
                -- or trigger_dbg[self.__evt_name] 
                then
                    log("*event:trigger:=(%s/%s):[%s]", self.__evt_name, tostring(#events[self.__evt_name]), "<")
                end
                if self.__remove then
                    table.remove(events[self.__evt_name], i)
                end
            end
        end
        if self.__once == true then 
            events[self.__evt_name] = nil
        end
    end
	if _ret ~= nil then
		return _ret
	end
end

function event:once(is_once)
    self.__once = is_once == true
    return self
end

function event:stop()
    self.__break = true
    return self 
end

function event:remove() --// ���������� ��������
    self.__remove = true
    return self 
end

function event:clear(name)
    if name and events[name] then 
        log("*event:clear:=[%s]:#(%s):[%s]", name, #events[name], "x") 
        events[name] = nil
    end
end

if not flag_init then init() end
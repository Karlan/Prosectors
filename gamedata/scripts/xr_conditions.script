--[[ ----------------------------------------------------------------------------------------------
 File       : xr_conditions.script
 Description: common preconditions for logic
 Copyright  : 2004 � GSC Game World
 Author     : GSC Game World
 Editors    : Karlan
--]] ----------------------------------------------------------------------------------------------

--[[
Karlan->ALL: � ������� ������, ��� ��� ��� �� ����� �� �����, � � ���������� �� ���� ����� �� �����, ��� ���
� 100% ������� ����������� ������� ������ � ������ ��� ����� �� ������, ���� ����-�� �� ���� �� ����
����������� ������, �� � �������� ����� ��� ����� �������, ����� �������� ���������� ������ ���������� (���� �������) �
��������� ������� ���� ��� ������ ��� ������ ����������, � � ��������� ������� ��� ���������� ������ � ���, �������
������ ��� ������ nil ������

��������� ���������� �������� ������, � ���� ��� ��� � ���������, �� ������ ����, � �� ��� ���� ����� ��� ���� �����
������ ����-������, ����� ���� ������������� has_info �� ����� ������� ������, � ���� �� ��� ������������ ������, ���
�� ����� �������� � ������ � ����������� �� ������� ������� �������
]]
-- ������ ������� � ���� ����� ������������ ��� ������� xr_logic: {=������� !�������}
-- ���� � ������� ���������� ���������� ���������, ��:  {=�������(�����1:�����2:...) !�������(�����1:�����2:...)}
-- ������: function f(npc). � ������ �������� ����������: function f(npc, p).
-- ������ ���������� ����� �������� � ���� xr_conditions_impl.script, � �� � ���� ����.

_DEBUG_ = true
-- ----------------------------------------------------------------------------------------------------
-- ����� �������
-- ----------------------------------------------------------------------------------------------------
-- ����� �� �� ��� "������ �����" ��� ���?
function black_screen(npc)
    return device().precache_frame > 1
end

function check_npc_name(npc , p) --// Karlan: for CS temp compat (function created for treasure trading in dialogs)
	log("check_npc_name: npc = [%s], params = [%s]", npc, p)
	if not npc then return false end
	local name_exist = false
	for k,v in pairs(p) do
		if string.find( npc:name(), v ) ~= nil then
			name_exist = true
		end
	end
	return name_exist
end

function is_rain(npc)
    return use_rain_path()
end

function is_heavy_rain(npc)
	if rain_override ~= nil then
		return rain_override
	end
    return level.rain_factor() > 0.5
end

function under_heavy_rain(npc)
	if not is_heavy_rain() then
		return false
	end
	local obj = npc:get_stalker() or npc:get_base_monster()
	return obj and obj:trace_rain(true,true) > 0.5 or false
end

function has_active_task(npc, p)
    return manager_task.task_active(p[1],true)
end

function day_time_between(npc, p)
	return in_hours_interval(p[1],p[2])
end

function hours_multiple(npc, p)
	return (level.get_time_hours() + (p[2] or 0)) % p[1] == 0
end

function days_multiple(npc, p)
	return (level.get_time_days() + (p[2] or 0)) % p[1] == 0
end

--// ����� �� 2 ���������, ����������� �� ������� : 1� = p[1] �����, 2� = p[2] �����
function hours_switch(npc, p)
	local shift	= p[3] or (get_id(npc)+44)%24
	local time	= level.get_time_hours() + level.get_time_days()*24 + shift
	time		= time%(p[1]+p[2])
	return		time < p[1]
end

function days_switch(npc, p)
	local shift	= p[3] or (get_id(npc)+88)%30
	local time	= level.get_time_days() + shift
--	logc("^days_switch[%s][%s][%s] shift [%s] time [%s](%s) res [%s]=[%s]",npc:name(),p[1],p[2],shift,time,level.get_time_days(),time%(p[1]+p[2]),time%(p[1]+p[2]) < p[1])
	time		= time%(p[1]+p[2])
	return		time < p[1]
end

local start_time = time_global()

function zone_online_time(npc, p)
	return db.zone_by_name[npc:name()] ~= nil and time_global() - start_time >= p[1]*1000
end

function gulag_online(npc, p)
	return m_gulag.is_gulag_online(p[1] or npc:name())
end

function is_online(npc)
	assert(npc.is_server_object)
	return npc.online
end

-- ��������, ��� ����� ���
function actor_alive(npc)
    if actor and actor:alive() then
        return true
    end
    return false
end

-- ��������, ��� ����� �����
function actor_dead(npc)
    if actor and not actor:alive() then
        return true
    end
    return false
end

function is_wounded(npc)
    return ai_wounded.is_wounded(npc)
end

function dist_to_actor_le(npc, p)
    local pos = not npc.is_server_object and npc:position() or npc.position
    return pos:distance_to(actor:position()) <= p[1]
end

function dist_to_actor_ge(npc, p)
    local pos = not npc.is_server_object and npc:position() or npc.position
    return pos:distance_to(actor:position()) >= p[1]
end

-- �������� ���� ��� ��������� �� ������� <= ��������
-- ���������: [sid,dist]
function distance_to_obj_le(npc, p)
    local npc1 = level.object_by_sid(p[1])
    if npc1 then
        return npc:position():distance_to(npc1:position()) <= p[2]
    end
    return false
end

-- �������� ���� ��� ��������� �� ������� >= ��������
-- ���������: [sid,dist]
function distance_to_obj_ge(npc, p)
    local npc1 = level.object_by_sid(p[1])
    if npc1 then
        return npc:position():distance_to(npc1:position()) >= p[2]
    end
    return false
end

-- �������� ���� ��� npc ��������� � �������� ����
-- !!! �������� ������ �� SPACE RESTRICTOR !!!
-- ���������: [sid1:sid2:...]
-- !!! ����������� �������� ��� �������� � offline'e !!!
-- !!! ��� �������� ������������ one_obj_in_zone !!!
--// ������: ��� � �������� ����������� ��������, ������
-- function obj_in_zone(zone, p)
    -- local npc1, i, v = 0, 0, 0
    -- for i, v in pairs(p) do
        -- npc1 = level.object_by_sid(v)
        -- if npc1 and zone:inside(npc1:position()) then
            -- return true
        -- end
    -- end
    -- return false
-- end

function one_obj_in_zone(zone, p)
    --local def_offline = (p[2] ~= "false") -- default (true) result if npc in offline
    local obj1 = level.object_by_sid(p[1])

    if obj1 then -- npc is online
        return zone:inside(obj1:position())
    else -- npc is offline
        return (p[2] ~= "false") -- default (true) result if npc in offline
    end
end

function actor_in_zone(npc, p)
    local zone = db.zone_by_name[p[1]]
    if zone == nil then
        return false
    end
    return obj_in_zone(actor, zone)
end
function actor_out_zone(npc, p)
    local zone = db.zone_by_name[p[1]]
    if zone == nil then
        return false
    end
    return not obj_in_zone(actor, zone)
end

-- true, ���� �������� npc <= ��������� ��������
-- false � ��������� ������
function health_le(npc, p)
    return p[1] and npc.health < p[1]
end

-- true, ���� �������� �������� <= ��������� ��������
-- false � ��������� ������
function heli_health_le(obj, p)
    return p[1] and obj:get_helicopter():GetfHealth() < p[1]
end

function heli_flaming(npc)
	verify(npc:binded_object(),"%s",npc)
    return npc:binded_object():is_flaming()
end

function gulag_state(npc, p)
    if m_gulag.getGulagState(p[1]) == p[2] then
        return true
    end
    return false
end

function npc_community(npc, p)
    if p[1] == nil then
        abort("Wrong number of params in npc_community")
    end

	local comm = npc:character_community()
	for _,v in ipairs(p) do
		if v == comm then
			return true
		end
	end
	return false
end

function npc_rank(npc, p)
    if p[1] == nil then
        abort("Wrong number of params in npc_rank")
    end

	local rank = m_ranks.get_obj_rank_name(npc)
	for _,v in ipairs(p) do
		if v == rank then
			return true
		end
	end
	return false
end

function npc_profile(npc, p)
    if p[1] == nil then
        abort("Wrong number of params in npc_profile")
    end

    if npc:profile_name() == p[1] then
        return true
    end
    return false

end


-- �������� ���� ��� ���� ��� ������� ���-�� �� npc ��������� � ������.
-- ��������� ��� story_id ����������. ����� �������� ��������� story_id.
function hitted_by(npc, p)
    local npc1
    local hitter_id = read_pstor('hitter_id', INVALID_ID, npc)
    for _,v in pairs(p) do
        npc1 = level.object_by_sid(v)
        if npc1 and hitter_id == npc1:id() then
            return true
        end
    end
    -- local t = db.storage[npc:id()].hit
    -- if t then
        -- for i, v in pairs(p) do
            -- npc1 = level.object_by_sid(v)
            -- if npc1 and t.who == npc1:id() then
                -- printf("_bp: hitted_by(%d)", v)
                -- return true
            -- end
        -- end
    -- end
    return false
end

-- ��������, ��� ������ ������ ��������� - ��������
function best_pistol(npc)
    local pistol = npc:item_in_slot(PISTOL_SLOT)
    if pistol ~= nil then
        return true
    else
        return false
    end
--[[
    local wpn = npc:best_weapon()
    if wpn == nil then
        return false
    end
    wpn = get_clsid(wpn)
    if wpn == nil then
        return false
    end
    if wpn == clsid.wpn_hpsa then return true
    elseif wpn == clsid.wpn_hpsa then return true
    elseif wpn == clsid.wpn_pm then return true
    elseif wpn == clsid.wpn_fort then return true
    elseif wpn == clsid.wpn_walther then return true
    elseif wpn == clsid.wpn_usp45 then return true
    else return false end
]]
end

-- �������� ���� ��� �������� ��� ���� ���-�� �� npc ��������� � ������.
-- ��������� ��� story_id ����������. ����� �������� ��������� story_id.
function killed_by(npc, p)
	local entity = npc:get_stalker() or npc:get_base_monster()
	if not entity then return false end
	local killer_id = entity:killer_id()
	for i, sid in ipairs(p) do
		local npc1 = level.object_by_sid(sid)
		if npc1 and killer_id == npc1:id() then
			return true
		end
	end
	return false
end

-- �������� (�� story_id ��� �����) ����, ��� ����������� npc ��� � � �������. (���� ���� p[2], ����������� � � �������)
-- TODO: ��������� ��������, ����� �������� ������������ ��������� ��� ��������, �������
--       �� ������ �������������.
function is_alive(npc, p)
    local npc1
	if is_number(p[1]) then
		if p[2] then
			npc1 = sim:story_object(p[1])
			npc1 = npc1 and npc1:get_cse_alife_monster_abstract()
		else
			npc1 = level.object_by_sid(p[1])
		end
	else
		npc1 = level.object_by_name(p[1])
		if not npc1 and p[2] then
			npc1 = sim:object(p[1])
			npc1 = npc1 and npc1:get_cse_alife_monster_abstract()
		end
	end
    return npc1 and npc1:alive()
end

-- �������� (�� story_id ��� �����) ����, ��� ����������� npc ����� ��� �� ���������� ��� �� � �������. (���� ���� p[2], ����������� � � �������)
function is_dead(npc, p)
	return not is_alive(npc, p)
end

--// Karlan: is_dead_all �������������� � CS, �� ��� � ������ ���� ���������

-- �������� (�� story_id ��� �����) ����, ��� ���� �� ���� �� ����������� ��������� ����� ��� �� ���������� ��� �� � �������
-- TODO: ��������� ��������, ����� �������� ������������ ��������� ��� ��������, �������
--       �� ������ �������������.
function is_dead_one(npc, p)
	local npc1
	for i, v in pairs(p) do
		if is_number(v) then
			npc1 = level.object_by_sid(v)
		else
			npc1 = level.object_by_name(v)
		end
		if not npc1 or not npc1:alive() then
			printf("_bp: is_dead_one(%d) = true", v)
			return true
		end
	end
	return false
end

function is_sid(npc, p)
	return npc:story_id() == p[1]
end

function is_name(npc, p)
	return npc:name() == p[1]
end

function is_obj_destroyed(npc, p)
	local name_or_sid = p[1]
	local obj = nil
	if is_number(name_or_sid) then
		obj = level.object_by_sid(name_or_sid)
	else
		obj = level.object_by_name(name_or_sid)
	end
	return obj == nil or (obj:get_destroyable_physics_object() and obj:get_destroyable_physics_object():is_destroyed())
end

function is_actor_captured(npc, p)
	local name_or_sid = p[1]
	if not name_or_sid then
		return actor:get_actor():ph_captured()
	end
	local obj = nil
	if is_number(name_or_sid) then
		obj = level.object_by_sid(name_or_sid)
	else
		obj = level.object_by_name(name_or_sid)
	end
	return obj and actor:get_actor():ph_captured(obj) or false
end

-- ----------------------------------------------------------------------------------------------------
-- ������� ��� ������ � combat_ignore_cond
-- ----------------------------------------------------------------------------------------------------
-- ������� ���� �� ���������� ������ ��� ������ ��������� ����������
-- ��� combat_ignore
function fighting_dist_ge(npc, p, enemy)
    return enemy:position():distance_to( npc:position() ) >= p[1]
end

-- ������� ���� ����?
function fighting_actor(npc, p, enemy)
    return enemy == actor
end

-- �������� (�� story_id) ����, ��� ����� ������ ���� ���� �� ����-�� ���� �� ������
function check_fighting(npc, p, enemy)
    if enemy:alive() then
        local sid = enemy:story_id()
        for i, v in ipairs(p) do
            if sid == v then
                return true
            end
        end
    end
    return false
end

function enemy_gulag(npc, p, enemy)
    if enemy:alive() and enemy:id() ~= actor_id then
        local g = m_gulag.get_npc_gulag(enemy)
        if g ~= nil then
            local n = g.name

            for i, v in ipairs(p) do
                if n == v then
                    return true
                end
            end
        end
    end
    return false
end

function enemy_in_zone(npc, p, enemy)
	local pos = enemy:position()
	local zone
	for i, v in ipairs(p) do
		if is_number(v) then
			zone = level.object_by_sid(v)
		else
			zone = level.object_by_name(v)
		end
		if zone and zone:inside(pos) then
			return true
		end
	end
	return false
end

function val_nap1_check_fighting(npc, p)
    local enemy, npc1
    for i, v in ipairs(p) do
        npc1 = level.object_by_sid(v)
        if npc1 then
            enemy = npc1:best_enemy()
            if enemy and (enemy:id() == npc:id() or enemy:id() == actor_id) then
                return true
            end
        end
    end
    return false
end

-- true, ���� ������ � ����� ������ (��� story id) �� ���������� ��� � ��� ��� ������
function gulag_empty(npc, p)
    if is_string(p[1]) then
        return (m_gulag.getGulagPopulationComed(p[1]) == 0)
    end
    warning("~~>WARNING: {gulag_empty} error: arg is not string (%s, type = [%s])", p[1], type(p[1]))
    return false
end

-- true, ����  � ��������� ������ ������ ������ ��� ����.
function gulag_population_le(npc, p)
    if is_string(p[1]) and tonumber(p[2]) then
        return m_gulag.getGulagPopulation(p[1]) <= tonumber(p[2])
    end
    warning("~~>WARNING: {gulag_population_le} error: arg is not string or number (%s, type = [%s])", p[1], type(p[1]))
    return false
end

-- true, ����  � ��������� ������ ������ ������ ��� ����.
function gulag_population_ge(npc, p)
    if (is_string(p[1]) or tonumber(p[1])) and tonumber(p[2]) then
        return m_gulag.getGulagPopulation(p[1]) >= tonumber(p[2])
    end
    warning("~~>WARNING: {gulag_population_ge} error: arg is not string or number (%s, type = [%s])", p[1], type(p[1]))
    return false
end

-- true, ����  � ��������� ������ ������ ������ ������ ��� ����.
function gulag_population_comed_le(_, p)
    if (is_string(p[1]) or tonumber(p[1])) and tonumber(p[2]) then
        return m_gulag.getGulagPopulationComed(p[1]) <= tonumber(p[2])
    end
    warning("~~>WARNING: {gulag_population_comed_le} error: arg is not string or number (%s, type = [%s])", p[1], type(p[1]))
    return false
end

-- true, ����  � ��������� ������ ������ ������ ������ ��� ����.
function gulag_population_comed_ge(npc, p)
    if (is_string(p[1]) or tonumber(p[1])) and tonumber(p[2]) then
        return m_gulag.getGulagPopulationComed(p[1]) >= tonumber(p[2])
    end
    warning("~~>WARNING: {gulag_population_comed_ge} error: arg is not string or number (%s, type = [%s])", p[1], type(p[1]))
    return false
end

-- ���������� ���������� ������� ���������� � ������(����� � �� ��������).
function gulag_population_active(npc, p)
    local gulag = m_gulag.get_gulag_by_name( p[1] )

    if gulag == nil then
        warning("~~>WARNING: {gulag_population_active} error: gulag is nil (%s)", p[1])
        return 0
    end

    local val = 0

    for k,v in pairs(gulag.Object) do
        if ( v == true or v:alive() and not ai_wounded.is_heavy_wounded_by_id(k) ) and gulag.Object_begin_job[k] == true then
            val = val + 1
        end
    end
    return val
end

function gulag_inactive(npc, p)
     local gulag = m_gulag.get_gulag_by_name( p[1] )

    if gulag == nil then
        warning("~~>WARNING: {gulag_inactive} error: gulag is nil (%s)", p[1])
        return false
    end

    local val = 0

    for k,v in pairs(gulag.Object) do
        if ( v == true or
		v:alive() and not ai_wounded.is_heavy_wounded_by_id(k) ) and
            gulag.Object_begin_job[k] == true
        then
            val = val + 1
        end
    end
    return val == 0
end

function gulag_population_active_le(npc, p)
     local gulag = m_gulag.get_gulag_by_name( p[1] )

    if gulag == nil then
        warning("~~>WARNING: {gulag_population_active_le} gulag is nil (%s)", p[1])
        return false
    end

    local val = 0

    for k,v in pairs(gulag.Object) do
        if ( v == true or
		v:alive() and not ai_wounded.is_heavy_wounded_by_id(k) ) and
            gulag.Object_begin_job[k] == true
        then
            val = val + 1
        end
    end
    return val <= p[2]
end

-- true, ���� � ��������� ������ ������ ������ ��� ����� ��������
function gulag_casualities_ge(npc, p)
    if (is_string(p[1]) or tonumber(p[1])) and tonumber(p[2]) then
        return m_gulag.getCasualities(p[1]) >= tonumber(p[2])
    end
    warning("~~>WARNING: {gulag_casualities_ge} error: arg is not string or number (%s, type = [%s])", p[1], type(p[1]))
    return false
end

-- true, ���� � ����� � ��������� ���� ��������� �������
-- false, ���� ����, ���� �� ������ ������ ��������
function actor_has_item(npc, p)
    return p[1] ~= nil and actor:object( p[1] ) ~= nil
end

-- ���������� true, ���� � ������� ����� ��������� ������ ��������� ������.
function signal(npc, p)
    if p[1] then
        local st   = db.storage[npc:id()]
        local sigs = st[st.active_scheme].signals
--        printf( "xr_conditions.signal: npc=%s, scheme=%s", npc:name(), tostring(st.active_scheme) )
        return sigs ~= nil and sigs[p[1]] == true
    else
        return false
    end
end

-- ���������� true, ���� �������� ���������� �������� ����� ������ ���������� �����
function counter_greater(npc, p)
    if p[1] and p[2] then
        local c = xr_logic.pstor_retrieve(actor, p[1], 0)
        return c > p[2]
    else
        return false
    end
end

-------------------------------------------------------------------------------------------------------
-- ������� ��������� kamp
function _kamp_talk(npc)
    if ai_kamp.kamp_stalkers[npc:id()] then
        return ai_kamp.kamp_stalkers[npc:id()]
    end
    return false
end

function _used(npc)
    return npc:is_talking()
end

-------------------------------------------------------------------------------------------------------

function has_enemy(npc)
    return npc:best_enemy() ~= nil
end

function has_danger(npc)
    return npc:best_enemy() ~= nil or db.storage[npc:id()].danger_flag == true
end

function see_enemy(npc)
    local enemy = npc:best_enemy()

    if enemy ~= nil then
        return npc:see(enemy)
    end
    return false
end

function talking(npc)
    return actor:is_talking()
end

function see_actor(npc)
    return npc:alive() and npc:see(actor)
end

function actor_enemy(npc)
	if npc:relation(actor) == game_object.enemy then
		return true
	end
	local entity = npc:get_stalker() or npc:get_base_monster()
	if entity and entity:killer_id() == actor_id then
		return true
	end
	return false
end

function actor_faced(npc)
    return npc:position():sub(actor:position()):normalize():similar(actor:direction(),math.pi*0.2) == 1
end

function actor_moving(npc)
	return bit_and(actor:get_state(),global_flags.mcAnyMove) ~= 0
end

function gar_dm_set_sympathy(npc)
	npc:set_sympathy(0)
	return false
end

-- function gar_dm_nearest_bandit(npc)
    -- return gar_dm_nearest_bandit(npc, 1)
-- end

function gar_dm_2nd_nearest_bandit(npc)
    return gar_dm_nearest_bandit(npc, 2)
end

--// Karlan to ALL: this is at most a makeshift
function gar_dm_nearest_bandit(npc, which)
    --printf("_bp: gar_dm_nearest_bandit: npc='%s', which=%d", npc:name(), which)
    local dists = {}
    local n = 1

    local gar_dm_bandit1 = level.object_by_sid(101)
    if gar_dm_bandit1 and gar_dm_bandit1:alive() then
        dists[n] = { who = gar_dm_bandit1, dist = distance_between(actor, gar_dm_bandit1) }
        n = n + 1
    end

    local gar_dm_bandit2 = level.object_by_sid(102)
    if gar_dm_bandit2 and gar_dm_bandit2:alive() then
        dists[n] = { who = gar_dm_bandit2, dist = distance_between(actor, gar_dm_bandit2) }
        n = n + 1
    end

    local gar_dm_bandit3 = level.object_by_sid(103)
    if gar_dm_bandit3 and gar_dm_bandit3:alive() then
        dists[n] = { who = gar_dm_bandit3, dist = distance_between(actor, gar_dm_bandit3) }
        n = n + 1
    end

    if n <= which then
        --printf("_bp: gar_dm_nearest_bandit: n (%d) <= which (%d): return false", n, which)
        return false
    end

    table.sort(dists, function(a,b) return a.dist < b.dist end)

    local i_am_the_one = dists[which].who:id() == npc:id()
    --printf("_bp: gar_dm_nearest_bandit: dist[which].who='%s', npc='%s': return %s", dists[which].who:name(), npc:name(), i_am_the_one)

    return i_am_the_one
end

function trade_buy_good(npc)
    return db.storage[npc:id()].trade.buy_good
end

function trade_deal_good(npc)
    return db.storage[npc:id()].trade.deal_good
end

function trade_deal_bad(npc)
    return db.storage[npc:id()].trade.deal_bad
end

function trading(npc)
    return db.storage[npc:id()].trade.trading
end

function hit_by_actor(npc, p, data)
	if data and data.who then
		return data.who:id() == actor_id or false
	end
	return read_pstor('hitter_id', INVALID_ID, npc) == actor_id
end

function killed_by_actor(npc)
	local entity = npc:get_stalker() or npc:get_base_monster()
	local killer_id = entity and entity:killer_id()
	local is_actor = killer_id == actor_id
	printf("killed_by_actor: <%s>", is_actor)
	return is_actor
end

function actor_has_weapon (npc)
    local obj = actor:active_item ()
    if obj == nil or IsWeapon(obj) == false then return false end
    return true
end

function heavy_wounded(npc)
    return ai_wounded.is_heavy_wounded_by_id( npc:id() )
end

function is_day (npc)
    return is_nominal_day()
end

function mob_was_hit(npc)
    local h = npc:get_monster_hit_info()
    if h.who and h.time ~= 0 then
        return true
    end
    return false
end


------------------------------------------------------------------------------------
-- Special functions for Garbage
------------------------------------------------------------------------------------
function gar_boars_nest2_dead(npc)
    return gulag_population_le(npc, {"gar_boars_nest2",0})
end

function gar_damaged_bandits(npc)
    return gulag_population_active(npc, {"gar_bandit_agr"}) <= 5
end

function gar_not_damaged_bandits(npc)
    return  not this.gar_damaged_bandits (npc)
end

function gar_bandits_seryi_die()
    return gulag_population_active(npc, {"gar_seryi_bandits"}) == 0
end

function gar_bandits_seryi_not_die()
    return not gar_bandits_seryi_die()
end


------------------------------------------------------------------------------------
-- Special functions for Dark Valley
------------------------------------------------------------------------------------

-- ���������: [dist]
function val_escort_captive_dist_to_guards_ge(npc, p)
    local d2 = p[1] * p[1]

    local g1 = level.object_by_sid(407)
    if g1 and g1:alive() then
        if npc:position():distance_to_sqr(g1:position()) < d2 then
            --printf("xr_cond <captive_dist_to_guards>: FALSE")
            return false
        end
    end

    local g2 = level.object_by_sid(408)
    if g2 and g2:alive() then
        if npc:position():distance_to_sqr(g2:position()) < d2 then
            --printf("xr_cond <captive_dist_to_guards>: FALSE")
            return false
        end
    end

    --printf("xr_cond <captive_dist_to_guards>: TRUE")
    return true
end

function val_escort_captive_dist_to_guards_le(npc, p)
    local d2 = p[1] * p[1]

    local g1 = level.object_by_sid(407)
    if g1 and g1:alive() then
        if npc:position():distance_to_sqr(g1:position()) > d2 then
            --printf("xr_cond <captive_dist_to_guards>: FALSE")
            return false
        end
    end

    local g2 = level.object_by_sid(408)
    if g2 and g2:alive() then
        if npc:position():distance_to_sqr(g2:position()) > d2 then
            --printf("xr_cond <captive_dist_to_guards>: FALSE")
            return false
        end
    end

    --printf("xr_cond <captive_dist_to_guards>: TRUE")
    return true
end

function val_fighting_sacrifice_victim(npc, p, enemy)
    -- FIXME: use SID!!!
    return enemy:name() == "val_sacrifice_victim"
end

function val_fighting_prisoner(npc, p, enemy)
    -- FIXME: use SID!!!
    return enemy:name() == "val_prisoner_captive"
end

------------------------------------------------------------------------------------
-- Special functions for Pripyat
------------------------------------------------------------------------------------
function pri_followers_can_hear(npc)
    local gulag = m_gulag.get_gulag(823)

	if has_info('pri_wave7_end') then return false end --// Karlan: �� ��������

    return not has_info("pri_followers_start") or
           gulag ~= nil and gulag:get_population() > 0 and
           not gulag:npc_is_enemy_to_anybody(actor)
end

function pri_monolith_combat_ignore(npc, p, enemy)
    if enemy.clsid == clsid.snork_s and enemy:alive() then
        local g = m_gulag.get_npc_gulag(enemy)
        return g ~= nil and (g.name == "pri_snork_nest1" or g.name == "pri_snork_nest2")
    else
        return false
    end
end

function pri_follower_see_ambush (npc)
    local gulag = m_gulag.get_gulag_by_name("pri_wave7")

    if gulag == nil then
        return true
    end

    for k,v in pairs(gulag.Object) do
        if v ~= nil and npc:best_enemy() and npc:best_enemy():id() == v:id()
            then
                -- printf("[plecha] %s see %s or has enemy %s", string.exformat(npc), string.exformat(v), string.exformat(npc:best_enemy()))
                return true
        end
    end
    return false
end


------------------------------------------------------------------------------------
-- Special functions for Escape
------------------------------------------------------------------------------------
function esc_blokpost_night(npc)
    if m_gulag.getGulagState("esc_blokpost") == 1 then
        return true
    end
    return false
end

function esc_bandits_die(npc)
    if has_info("esc_kill_bandits_quest_kill") or has_info("esc_kill_bandits_noquest_kill") then
        return true
    end
    return false
end

------------------------------------------------------------------------------------
-- Special functions for BAR - ROSTOK
------------------------------------------------------------------------------------
local bar_abuser_id = nil --// ������ �������

function bar_dolg_alarm(npc)
    if m_gulag.getGulagState("bar_dolg_general") == 2 then
        return true
    end
    if m_gulag.getGulagState("bar_dolg_veterans") == 2 then
        return true
    end
    return false
end

function bar_arena_fight_3_end ()
    return has_info("bar_arena_fight_3_stalker_1_die") and has_info("bar_arena_fight_3_stalker_2_die")
end

function bar_arena_fight_4_end ()
    return has_info("bar_arena_fight_4_stalker_1_die") and has_info("bar_arena_fight_4_stalker_2_die") and has_info("bar_arena_fight_4_stalker_3_die")
end

function bar_arena_fight_5_end ()
    return has_info("bar_arena_fight_5_stalker_1_die") and has_info("bar_arena_fight_5_stalker_2_die")
end

function bar_arena_fight_6_end ()
    return has_info("bar_arena_fight_6_stalker_1_die") and has_info("bar_arena_fight_6_stalker_2_die") and has_info("bar_arena_fight_6_stalker_3_die") and has_info("bar_arena_fight_6_stalker_4_die") and has_info("bar_arena_fight_6_stalker_5_die") and has_info("bar_arena_fight_6_stalker_6_die")
end

function bar_arena_fight_8_end ()
    return has_info("bar_arena_fight_8_stalker_1_die") and has_info("bar_arena_fight_8_stalker_2_die") and has_info("bar_arena_fight_8_stalker_3_die")	-- and has_info("bar_arena_fight_8_stalker_4_die")
end

function bar_territory_exclude(_, p, npc, who_or_amount, _, who)
	if npc:is_monster() then return true end
	who = is_userdata(who_or_amount) and who_or_amount or who
	if get_factions_community_relation("dolg", npc:character_community()) < 0 then --// todo: ��� ���� ����� ���� ���������� ����������� � ����, �� �� � �����?
		return true
	end
	if not who or who ~= actor and who:character_community() == "dolg" then
		return true
	end
	local kill = p and p[1] ~= nil
	if not kill and not who:is_actor() then
		return true
	end
	if npc:id() == bar_abuser_id then
		if who:is_actor() then
			--// ������������� �������� �� �� ��� ������ ��������� �����
		end
		return true
	end
	-- if get_factions_community_goodwill("dolg", npc:id()) < 0 then
		-- return true
	-- end
	if who:is_actor() and manager_task.get_random_task():is_task_target(npc,"kill_stalker") then --// ��� �� ���� ����� �������� ��� ����������, �� ������� ���� ��� ����������� �������� ���������� �������� ������
		return true
	end
	bar_abuser_id = who:id()
	return false
end

function killer_is_actor(_,_, npc, who_or_amount, _, who)
	local killer = is_userdata(who_or_amount) and who_or_amount or who
	return killer:id() == actor_id
end
------------------------------------------------------------------------------------
-- Special functions for Military
------------------------------------------------------------------------------------
function mil_actor_enemy (npc)
    local npc = level.object_by_sid (707)
    if npc == nil or npc:alive () == false then
       npc = level.object_by_sid (702)
       if npc == nil or npc:alive () == false then
          npc = level.object_by_sid (728)
          if npc == nil then
             printf ("NOT LEAVED NPC !!!")
             return
          end
        end
    end
    return npc:relation (actor) == game_object.enemy
end

function mil_actor_enemy_freedom (npc)
    if relation_registry.community_goodwill ("freedom", actor_id) < -500 then
       return true
    end
    return false
end

------------------------------------------------------------------------------------
-- Special functions for Deadcity
------------------------------------------------------------------------------------
--[[function cit_combat_ignore(npc, p, enemy) --// Karlan: ������� ������������, �� �������, ����-�� ���� �� ��������� ����������� �� ���� ��� ������� ��� ��� ����
    if enemy and enemy:alive() then
        if enemy:id() == actor_id then
            return p[1] ~= nil and not has_info(p[1])
        elseif enemy:story_id() == 1200 then
            return true
        else
            local g = m_gulag.get_npc_gulag(enemy)
            local gulags = {cit_kanaliz1 = 1, cit_kanaliz2 = 1, cit_kanaliz3 = 1}
            return g ~= nil and gulags[g] == 1
        end
    end
    return false
end]]


-------------------------------------------------------------------------------------
-- Special for Agroprom
-------------------------------------------------------------------------------------

function agr_nii_pop_check(npc)
    if (gulag_population_active(npc, {"agr_nii"}) < 6) then
        return true
    end
    return false
end


-------------------------------------------------------------------------------------
-- Special for rostok
-------------------------------------------------------------------------------------
function rostok_gulag_inactive(npc)
    local gulag = m_gulag.get_gulag_by_name("bar_freedom_attack")

    if gulag == nil then
        return true
    end

    local val = 0

    for k,v in pairs(gulag.Object) do
        local job_prior = gulag.Job[gulag.ObjectJob[k]].prior

        if ( v == true or
           ( v:alive() and not ai_wounded.is_heavy_wounded_by_id(k) and
            job_prior == 5)) and gulag.Object_begin_job[k] == true
        then
            val = val + 1
        end
    end
    if val == 0 then
        return true
    end
    return false
end

function agr_factory_hold_inactive (npc)
     local gulag = m_gulag.get_gulag_by_name("agr_factory_hold")

    if gulag == nil then
        return true
    end

    local val = 0

    for k,v in pairs(gulag.Object) do
        if ( v == true or
		v:alive() and not ai_wounded.is_heavy_wounded_by_id(k) ) and
            gulag.Object_begin_job[k] == true
        then
            val = val + 1
        end
    end
    if val == 0 then
        return true
    end
    return false
end


function rostok_gulag_2_inactive(npc)
     local gulag = m_gulag.get_gulag_by_name("bar_freedom_chasers_attack")

    if gulag == nil then
        return true
    end

    local val = 0

    for k,v in pairs(gulag.Object) do
        if ( v == true or
		v:alive() and not ai_wounded.is_heavy_wounded_by_id(k) ) and
            gulag.Object_begin_job[k] == true
        then
            val = val + 1
        end
    end
    if val == 0 then
        return true
    end
    return false
end

function rostok_dist_to_actor_le(npc, p)
	return dist_to_actor_le(npc, p)
end


-------------------------------------------------------------------------------------
-- Special for Radar
-------------------------------------------------------------------------------------
local rpt_data = {
	{3, 20, 6},
	{3, 8, 17},
	{5, 10, 18},
	{2, 22, 7},
	{4, 15, 2},
}
local function rpt_idx()
	local idx = read_pstor("rpt_idx")
	if not idx then
		idx = write_pstor("rpt_idx", randomseed_execute(sim:seed()+119*math.floor(level.get_time_days()/6),math.random,#rpt_data))
	end
	return rpt_data[idx]
end
function rad_pass_time()
	local data = rpt_idx()
	if data[2] < data[3] then
		return (level.get_time_days() % data[1] == 0) and in_hours_interval(data[2], data[3])
	else	--// ��������, ���������� � ������������ ����, ������������ �� ���������
		local hours = level.get_time_hours()
		if hours >= data[2] then
			return (level.get_time_days() % data[1] == 0)
		elseif hours < data[3] then
			local sub		= game.CTime()
			sub:setHMS		(24, 0, 0)
			local bt		= game.get_game_time() - sub
			local _,_,day	= bt:get()	--// �������� ����� ����������� ���
			return (day % data[1] == 0)
		end
		return false
	end
end
function get_rad_pass_time()
	return unpack(rpt_idx())
end
function reset_rad_pass_time()
	remove_pstor("rpt_idx")
end

function rad_pass(npc, p)	--// p[1]: 0 = any, 1 = good, 2 = bad
	if p[1] == 0 then
		if not (actor:object('good_psy_helmet') or actor:object('bad_psy_helmet')) then
			return false
		end
	elseif not actor:object(p[1] == 1 and 'good_psy_helmet' or 'bad_psy_helmet') then
		return false
	end
	if p[2] ~= 1 and not rad_pass_time() then
		return false
	end
	return true
end
-------------------------------------------------------------------------------------
-- Special for Sarcofag
-------------------------------------------------------------------------------------
function actor_has_decoder(npc)
    return actor:object("decoder") ~= nil
end


function actor_on_level(npc, p)
	local lvl = level.name()
	for _,v in ipairs(p) do
		if v == lvl then
			return true
		end
	end
	return false
end

function can_send_tutorial()
	if actor:is_talking() then
		return false
	end
	if game.has_active_tutorial() or level.main_input_receiver() or get_console().visible or device().precache_frame > 0 then
		return false
	end
	if actor:get_bleeding() > 0.01 or actor.radiation > 0.01 or actor.health < 0.45 then
		return false
	end
	return true
end

-------------------------------------------------------------------------------------
function squad_exist(npc, p) return true end
function is_squad_enemy_to_actor(npc, p) return false end

function is_upgrading(npc, p)
--	pda.upgrade_closed = not pda.upgrade_closed
--	return not pda.upgrade_closed
	-- return pda.upgrade_closed
	-- return has_info('ui_trade_hide') or has_info('ui_talk_hide')
	return false
end

function has_active_tutorial()
	return game.has_active_tutorial()
end
-------------------------------------------------------------------------------------
--// PROSECTORS
-------------------------------------------------------------------------------------
function gulag_is_partial_friend(obj, p)
    if m_gulag.getGulagGoodwill(p[1], actor) >= (p[2] or (FRIENDS/2)) then
        return true
    end
    return false
end

function story_object_exists(npc, p)
	local obj = sim:story_object(p[1])
	return obj ~= nil
end

function trade_goodwill_ge(npc, p)
	return manager_trade.get_trade_goodwill(npc) >= p[1]
end

function community_goodwill_ge(npc, p)
	return get_factions_community_goodwill(p[1], actor_id) >= p[2]
end

function has_repair_item(npc)
	return toboolean(m_repair.get_repair_item_id(npc:id()))
end

function in_repair_process(npc)
	return m_timers.timer_exists("karlan_repair_timer"..tostring(npc:id()))
end

function in_repair_process_wpn(npc)
	local item_id = m_repair.get_repair_item_id(npc:id())
	local item = item_id and npc.inventory:object_by_id(item_id)
	if not (item and item:is_weapon_magazined()) then
		return false
	end
	return m_timers.timer_exists("karlan_repair_timer"..tostring(npc:id()))
end

function order_box_is_emty(npc)
	local box_name = npc:section() == 'inventory_box' and npc:name() or npc:name():gsub('spot','order')
	local box = level.object_by_name(box_name)
	if box then
		return box:object_count() == 0
	end
	return false
end

function has_map_spot_on_order_box(npc)
	local box_name = npc:section() == 'inventory_box' and npc:name() or npc:name():gsub('spot','order')
	local box = level.object_by_name(box_name)
	if box then
		return has_map_spot(box:id(), "order_box")
	end
	return false
end

function npc_at_waypoint(npc, p)
	local point_info = nil
	if p[3] == "true" then
		point_info = path_parse_waypoints(p[1])
		point_info = point_info[p[2]]
	end
	return stalker_at_waypoint(npc, p[1], p[2], point_info)
end

function wind_velocity_le(obj, p)
	return level.get_wind_velocity() < p[1]
end

function wind_velocity_ge(obj, p)
	return level.get_wind_velocity() >= p[1]
end

local function get_barometer_weather()
	if m_surge.surge_is_run() then
		return "surge"
	end
	if level.env_has_tb() then
		return "thunderbolt"
	end
	if level.rain_factor() > 0.1 then
		return "rain"
	end
	if level.env_has_flare() then
		return "clear"
	else
		return "cloudy"
	end
end

function barometr_cond(npc, p)
	return get_barometer_weather() == p[1]
end

function box_is_emty(_, p)
	assert(is_string(p[1]), 'bad string in arguments (%s)',p)
	local box = level.object_by_name(p[1])
	if box then
		return box:object_count() == 0
	end
	return false
end

function days_passed(obj, p)
	return game.get_game_time():diffSec(sim:start_game_time())/3600/24 > p[1]
end

hour_in_interval = day_time_between -- terra compat
<?xml version="1.0" encoding="windows-1251" ?>
<string_table>
	<string id="minimap_tutorial">
		<text>Hold down ($$ACTION_scores$$) to show the mini-map.</text>
	</string>
	<string id="binoc_tutorial">
		<text>To use binoculars, select them ($$ACTION_wpn_5$$) and aim to zoom in.</text>
	</string>
	<string id="binocular">
		<text>Binoculars</text>
	</string>
	<string id="encicloped_tutor">
		<text>Gameplay tutorials are recorded in the %c[UI_orange]Tutorial%c[default] section of your PDA's encyclopedia. You can refer to them at any time.\nTo close the prompt, press ($$ACTION_quit$$).\nYou can disable hints in the game settings.</text>
	</string>
	<string id="crouch">
		<text>Crouching</text>
	</string>
	<string id="crouch_tutorial">
		<text>To crouch, press ($$ACTION_crouch$$) or ($$ACTION_crouch_toggle$$). To crouch low, press ($$ACTION_accel$$) while crouching. Keep in mind: crouching allows you to regenerate stamina faster and control your weapon more easily when firing. And a low crouch allows you to move around while making as little noise as possible.</text>
	</string>
	<string id="hide_weapon">
		<text>Freeing your hands</text>
	</string>
	<string id="hide_weapon_tutorial">
		<text>Some stalkers won't want to talk if you point your gun in their face. To hide your weapon, press ($$ACTION_wpn_hide$$). This action will be useful for you even more than talking to people: power consumption during movement increases depending on the weight of the weapon in your hands, and heavy weapons will hinder your movement, slowing down your running speed.</text>
	</string>
	<string id="jump">
		<text>Jumping</text>
	</string>
	<string id="jump_tutorial">
		<text>To jump, press ($$ACTION_jump$$). The height to which you can jump will depend on the weight you are carrying and your health. Jumping with a heavier weight uses considerably more energy.</text>
	</string>
	<string id="overload">
		<text>Overloading</text>
	</string>
	<string id="overload_tutorial">
		<text>The more you carry, the faster you lose stamina and the slower you recover. Eventually, if you carry too much - you won't be able to move fast and will have to rest two steps into the third. If you're exhausted, it makes sense to crouch in place and drop your backpack for a quicker recovery.</text>
	</string>
	<string id="radiation">
		<text>Radiation</text>
	</string>
	<string id="radiation_tutorial">
		<text>When a character is in an area with a high radiation background or or is exposed to a radioactive artifact, they will show signs of radiation damage. The character's health begins to decrease. To remove the effects of radiation damage, you can use antirads or vodka. Protective suits, gas masks and radioprotectors are used to protect against radiation, and special radio-protective containers are used to neutralize artifact radiation.</text>
	</string>
	<string id="relation">
		<text>Relationships</text>
	</string>
	<string id="relation_tutorial">
		<text>When a player helps someone, the stalker's attitude towards him and the character's reputation in general improves. If you steal or shoot at your own, however, the relationship will deteriorate. Characters with a negative attitude towards you will refuse to trade with you and, if you do, are more likely to try to steal from you. Friends, on the other hand, will offer more favorable conditions for trading, discounts on services, and will turn a blind eye to your petty misdeeds, and can protect you from thieves while you sleep.</text>
	</string>
	<string id="sprint">
		<text>Sprinting</text>
	</string>
	<string id="sprint_tutorial">
		<text>To sprint, press the ($$ACTION_sprint_toggle$$) key while moving. During a sprint, the character runs faster and jumps farther, but tires more quickly.\nRunning with a weapon in your hand will expend more energy depending on its weight, and heavy weapons will also reduce your speed. You will not be able to sprint while reloading weapons or using items.</text>
	</string>
	<string id="tutorial">
		<text>Training</text>
	</string>
	<string id="weakness">
		<text>Fatigue</text>
	</string>
	<string id="weakness_tutorial">
		<text>When you run, jump, or carry a lot of weight - you start to get tired. If you are too tired, you will move very slowly. To rest, you need to stand still. If you drop your heavy backpack on the ground and crouch, you will recover stamina very quickly. Also remember that hunger lowers your recovery speed, and extreme thirst limits your maximum stamina.</text>
	</string>
	<string id="weapon">
		<text>Weapon state</text>
	</string>
	<string id="weapon_tutorial">
		<text>Every weapon has a %c[UI_orange]condition parameter%c[default]. As its state deteriorates, accuracy decreases, a badly worn weapon will begin to jam, and a blunted knife will do less damage. A weapon's durability gradually decreases from use, and if a few bullets hit the weapon it can render it completely unusable.\nYou can repair your weapons with mechanics or traders, or you can do it yourself with a toolbox, and you will need a sharpening kit for your knife.</text>
	</string>
	<string id="wound">
		<text>Open wounds</text>
	</string>
	<string id="wound_tutorial">
		<text>When the character is hit, he may start to bleed. Bleeding causes the character to constantly lose health. The more serious the bleeding (a green icon means slight bleeding, a red icon indicates a serious hemorrhage), the faster the rate at which he loses health. Bleeding will eventually stop with time, but even serious hemorrhages can be stopped immediately using bandages.</text>
	</string>
	<string id="pda_diary_tutorial">
		<text>In the dead stalker's PDA, you find his observations of the anomalies in the tunnel. It turns out that he discovered a pattern in their operation. You can read this information in your PDA under %c[UI_orange]Logs%c[default].</text>
	</string>
	<string id="tut_torch">
		<text>Flashlight</text>
	</string>
	<string id="torch_tutorial">
		<text>The flashlight has several focus modes. Hold down ($$ACTION_torch$$) to switch through modes.</text>
	</string>
	<string id="tut_backpack">
		<text>Backpack</text>
	</string>
	<string id="backpack_tutorial_1">
		<text>%c[UI_orange]A backpack%c[default] in the Zone is mandatory to carry loot and necessary equipment, without it you can only carry a small amount of stuff in the pockets of your suit.</text>
	</string>
	<string id="backpack_tutorial_2">
		<text>The backpack has a nominal volume, but, although it is not made out of rubber, you can stuff it in excess of this norm. If you are constantly in the %c[pda_red]red%c[default] zone of the backpack volume - it will start to wear out, and with further use it can tear at the most inopportune moment.</text>
	</string>
	<string id="backpack_tutorial_3">
		<text>If you do not fill the backpack to the brim - it will last forever, the Resource parameter tells you how many times you can fill the backpack beyond the norm.</text>
	</string>
	<string id="backpack_tutorial_4">
		<text>You can drop your backpack at any time to free yourself from unnecessary weight and increase mobility in combat. This can be done in the inventory, or if the backpack has a quick drop function, by pressing ($$ACTION_drop_backpack$$). An empty backpack can be used to create your own backpack in any convenient location.</text>
	</string>
	<!-- +++++-->
	<string id="tut_trade">
		<text>Trading</text>
	</string>
	<string id="trade_tutorial_1">
		<text>Merchants don't like doing business with strangers, and in order to get better deals and access to an expanded range of items, you need to work first.\nBusiness favor increase when you run errands and also when you trade: the value depends on the amount you've purchased and the value of the loot you've sold.\nClick the '%c[UI_orange]Haggle%c[default]' button in the trade menu to monitor the progress of the relationship and demand better deals.</text>
	</string>
	<string id="trade_tutorial_2">
		<text>Merchants don't like it when you come in just to look - when you open their trade inventory and don't buy or sell anything. Each time you do this, your favor with them drops slightly.</text>
	</string>
	<string id="trade_tutorial_3">
		<text>Bargaining also makes sense when exchanging with a stalker, - if he is your friend, he will definitely offer a better price. You can sell things to Stalkers even if they don't have enough money to pay the full price - the difference will be converted into better relations.\nWhen selling a weapon to a stalker%c[UI_orange], you must also sell some ammunition to him%c[default], so he can have something to shoot with, otherwise the stalker will not use the weapon until he finds the ammunition he needs.</text>
	</string>
	<string id="tut_repair">
		<text>Equipment repair</text>
	</string>
	<string id="repair_tutorial_1">
		<text>Mechanics and some merchants can repair your equipment for a fee. But most mechanics won't take orders from just anyone - you'll have to reach a certain level of personal relationship or faction favor first.</text>
	</string>
	<string id="repair_tutorial_2">
		<text>In order for %c[ui_yellow]Sidorovich%c[default] to open a repair service for you, you need to gain %c[pda_blue]180%c[default] favor points.\nDifferent mechanics differ in their skill level, specialization and, of course, the price of their services.</text>
	</string>
	<string id="repair_tutorial_3">
		<text>Also in the Zone there are %c[UI_orange]field repair kits%c[default]. To carry out repairs in the field you need to acquire a toolbox and the appropriate repair kits. There are two types of kits: One for repairing weapons, another for repairing armor and helmets. The field repair process is described in more detail in the manual you will find in the toolbox interface.</text>
	</string>
	<string id="tut_storage">
		<text>Stashes and safe storage</text>
	</string>
	<string id="storage_tutorial_1">
		<text>Finders keepers - that's the rule in the Zone. Therefore, any unattended items will eventually find a new owner.\nFor safe storage of property, traders provide %c[UI_orange]storage%c[default] services with a daily fee.</text>
	</string>
	<string id="storage_tutorial_2">
		<text>For a reasonable price you get a stash of a certain volume, where your belongings will be kept safe. The degradation of food and other perishable items in the storage room will be a little slower than usual.\nTo unlock this service from a merchant, you need to gain %c[pda_blue]240%c[default] favor points.</text>
	</string>
	<string id="storage_tutorial_3">
		<text>If you don't agree to pay for storage, you can organize your own storage. If you place it far enough away from stalker camps and traveled paths, no one will find it.\nUse an empty backpack or duffel bag to create a stash.</text>
	</string>
	<string id="tut_satiety_thirst">
		<text>Satiety and Thirst</text>
	</string>
	<string id="satiety_thirst_tutorial_1">
		<text>Your character needs food and drink. Mild hunger or thirst will not harm your health, but your recovery rate will be slightly reduced. Severe hunger will make you more tired and slower to recover, while severe thirst will limit your maximum stamina. Eventually, if you don't drink anything for a long time, you will die.</text>
	</string>
	<string id="satiety_thirst_tutorial_11">
		<text>Your satiety consumption is directly related to how actively you move and how loaded your character is.\nThirst builds up faster the more energy you expend.</text>
	</string>
	<string id="satiety_thirst_tutorial_2">
		<text>Eating briefly increases your health regeneration, while drinking briefly increases your strength regeneration. Keep in mind that consuming food increases thirst, so you should not eat too much.</text>
	</string>
	<string id="satiety_thirst_tutorial_3">
		<text>Consuming %c[pda_blue]alcohol%c[default] increases drowsiness, and drinking in large doses can be dangerous to your health. Don't forget to eat a snack, this will help mitigate intoxication.</text>
	</string>
	<string id="tut_psy_health">
		<text>Psi-health</text>
	</string>
	<string id="psy_health_tutorial">
		<text>Psi-health:</text>
	</string>
	<string id="tut_somnolence">
		<text>Sleep</text>
	</string>
	<string id="somnolence_tutorial_1">
		<text>Your character cannot stay awake indefinitely, and after a while he will get tired and want to sleep. The longer you procrastinate sleeping, the more drowsiness will take its toll. Your eyes will start to droop, your running speed will slow down, and your hands will become less confident in holding a weapon. Eventually, your character will collapse on the spot and sleep longer than usual.</text>
	</string>
	<string id="somnolence_tutorial_2">
		<text>To sleep, you can use a sleeping bag or one of the equipped sleeping spots that are marked on your map. You won't be able to sleep if the character doesn't want to sleep. In this case, vodka will help, and energy drinks, on the contrary, will allow you to stay awake longer. You will not be able to fall asleep if you are very hungry, and if satiety drops to 0 during sleep, you will wake up.</text>
	</string>
	<string id="somnolence_tutorial_3">
		<text>Be careful where you sleep - the Zone is full of people who like to grab other people's goods. Beware of stalkers with a bad reputation, so that you do not get robbed - it is better to sleep near friendly characters or away from people.</text>
	</string>
	<string id="somnolence_tutorial_4">
		<text>In case of critical fatigue, you can simply fall off your feet and fall asleep on the ground, in which case you risk losing health. But you will not die in your sleep: if your character feels unwell, he will wake up.</text>
	</string>
	<string id="tut_helmet">
		<text>Helmets and gas masks</text>
	</string>
	<string id="helmet_tutorial_1">
		<text>The head is your most vulnerable place, so you should not neglect its protection. And if a simple helmet can only save you from a bullet or a blow to the head, then models with a built-in gas mask or closed breathing system protect against radiation and other harmful effects.</text>
	</string>
	<string id="helmet_tutorial_2">
		<text>Some protective suits have a built in sealed helmet and will not allow the use of another. The built-in helmet can also be removed and put on.\nTo put on or take off a helmet/gas mask, press ($$ACTION_wear_mask$$).</text>
	</string>
	<string id="filter_tutorial">
		<text>%c[UI_orange]To filter air%c[default] from toxic gases and noxious fumes, you will need to attach a %c[UI_orange]filter%c[default] with the type corresponding to your gas mask: first put on your helmet, then use the filter in your inventory. To remove a filter, right-click on the helmet. Filters have a limited resource, their validity time is listed in the description in game minutes.</text>
	</string>
	<string id="tut_detectart">
		<text>Artifact Detectors</text>
	</string>
	<string id="detectart_tutorial_1">
		<text>You have an %c[UI_orange]artifact detector%c[default].\nTo equip it, press ($$ACTION_show_detector$$).\nThis device emits a special signal that awakens anomalous artifact activity, detects bursts of energy, and signals when an artifact is detected. It is powered by a standard lithium battery.</text>
	</string>
	<string id="detectart_tutorial_2">
		<text>Advanced detector models have increased power, better sensitivity, and can detect rarer artifacts.\nTo get a better look at the detector readings, you can zoom in on the detector by pressing ($$ACTION_wpn_zoom$$) or ($$ACTION_wpn_func$$).</text>
	</string>
	<string id="detectart_tutorial_3">
		<text>The detector is also used to %c[UI_orange]pull an artifact%c[default]from an anomaly's area. To turn the pull mode on or off, press ($$ACTION_wpn_zoom_switch$$). To pull the target, you need to point the detector at it, after which the artifact will start moving towards you. During this process, the device battery will be consumed faster.</text>
	</string>
	<string id="tut_quick_slots">
		<text>Quick slots</text>
	</string>
	<string id="quick_slots_tutorial">
		<text>You have gained access to the quick-slot cells. Drag and drop the items you need into the corresponding cells in your inventory. To use them, press ($$ACTION_quick_menu$$) (the selected items will be displayed), and press the appropriate key ($$ACTION_wpn_1$$,$$ACTION_wpn_2$$,$$ACTION_wpn_3$$,$$ACTION_wpn_4$$). Alternatively, you can assign separate buttons for each cell in the settings. The number of available cells depends on the size of the pockets of the suit you are wearing.</text>
	</string>
	<string id="tut_weapon_first">
		<text>Weapons</text>
	</string>
	<string id="weapon_first_tutorial_1">
		<text>You can equip two basic weapons ($$ACTION_wpn_2$$,$$ACTION_wpn_3$$) and an additional binoculars or pistol ($$ACTION_wpn_5$$).\nTo %c[UI_orange]hold%c[default] your breath while aiming, press ($$ACTION_jump$$) or ($$ACTION_hold_breath$$). This will help stabilize your aim, reducing sway and reducing weapon recoil, but will use up stamina.</text>
	</string>
	<string id="weapon_first_tutorial_2">
		<text>Crouching also helps to reduce sway, but leaning sideways does the opposite. Such negative factors as poor health, fatigue and drowsiness will also have a bad effect on the weapon sway.</text>
	</string>
	<string id="tut_monster_aura">
		<text>Anomalous mutant aura</text>
	</string>
	<string id="monster_aura_tutorial">
		<text>Some inhabitants of the Zone have acquired the properties of devastating effects: radiation, psi waves or poisonous vapors that poison the air around them. Being near to %c[pda_red]even a dead%c[default] mutant can be dangerous to your health.</text>
	</string>
	<string id="tut_item_discharge">
		<text>Device charge</text>
	</string>
	<string id="item_discharge_tutorial_1">
		<text>Flashlights, detectors, and other devices you use will use up battery power over time. And when exposed to radiation in radioactive areas, they begin to use charge much faster.</text>
	</string>
	<string id="item_discharge_tutorial_2">
		<text>If there is little charge left in the battery, the instrument will become worse, and when it runs out of power, it will shut down.\nKeep a close eye on the battery level by changing the batteries in a timely manner.</text>
	</string>
	<string id="tut_artefact">
		<text>Artifacts</text>
	</string>
	<string id="artefact_tutorial_1">
		<text>%c[UI_orange]Artifacts%c[default] can be compared to a capacitor charged with the energy of the anomaly that spawned it. When a person first touches the artifact, it partially discharges, striking everything around it with a blast of its anomalous energy. The type of damage is exactly the same as that of the parent anomaly.\nThe higher the rank of an artifact, the more damage it will do, so you must have the appropriate protection to collect more valuable artifacts.</text>
	</string>
	<string id="artefact_tutorial_2">
		<text>New artifacts are spawned during an Emission, destroying any old artifacts lying on the ground.\nThe special artifact type called %c[ui_yellow]Blank%c[default] is not tied to anomalies, and the principle of their origin is unknown.</text>
	</string>
	<string id="artefact_tutorial_3">
		<text>Artifacts that are not recharged by their parent anomaly lose their charge over time, and when they run out, they are annihilated, releasing their remaining energy in an explosion. Therefore, to avoid being hit, you should get rid of obsolete artifacts in advance.\nYou can recharge artifacts in their parent anomalies by throwing an artifact into one. This is not a quick process, so choose your location and conditions wisely. Artifacts with a condition of less than %c[ui_yellow]10 percent%c[default] are considered unrecoverable.</text>
	</string>
	<string id="artefact_tutorial_4">
		<text>Artifacts have randomly generated properties, to select an artifact to stash or sell, right click on the icon and select %c[UI_orange]separate%c[default], for the reverse operation select the separated artifact and select %c[UI_orange]group%c[default].</text>
	</string>
	<string id="tut_containerrad">
		<text>Artifacts, Part 2</text>
	</string>
	<string id="containerrad_tutorial_1">
		<text>To protect yourself from an artifact's radiation, and to protect the artifacts themselves from degradating, special protective %c[ui_yellow]containers are used%c[default]. Containers come in different numbers of cells and also have different characteristics for protection from radiation and preservation of anomalous properties of the artifact.</text>
	</string>
	<string id="containerrad_tutorial_2">
		<text>Special %c[ui_yellow]artifact detectors%c[default] make it easier to find and grab artifacts. They register their anomalous radiation and show their location, and serve to retrieve them from anomalous areas.</text>
	</string>
	<string id="containerrad_tutorial_3">
		<text>Many artifacts have useful properties. You can use them by placing them in special activation %c[ui_yellow]cells%c[default](artifact belt) attached to protective suits.\nIf you don't have any cells (artifact belts) on your suit, you can take an artifact in your hand. To do this, place it in the appropriate slot and press ($$ACTION_ARTEFACT$$).\nWhen used, the artifact discharges faster.</text>
	</string>
	<string id="tut_forbidden_box">
		<text>Shared storage</text>
	</string>
	<string id="forbidden_box_tutorial">
		<text>Other stalkers won't like to see a stranger rummaging through their shared storage. If you see a %c[pda_red]red%c[default] interaction prompt, it means you're not allowed to take items from the container. You will be able to access the crate if you help the camp or befriend its inhabitants.</text>
	</string>
	<string id="tut_usable_item">
		<text>Items to use</text>
	</string>
	<string id="usable_item_tutorial">
		<text>Using any item takes a certain amount of time, during which you cannot run or jump.\nBut you can cancel use by pressing ($$ACTION_sprint_toggle$$) or ($$ACTION_wpn_hide$$), or by selecting a weapon.</text>
	</string>
	<string id="tut_save_near_campfire">
		<text>Saving at campfires</text>
	</string>
	<string id="save_near_campfire_tutorial_1">
		<text>The option '%c[UI_orange]Saving at campfires%c[default]' is activated.\nTo save the game, you will need to save near a burning campfire or lamp.\nIn addition, you will be able to save in certain safe zones.\nThe house icon that appears on the left side of the screen will notify you that you can save the game in that location.</text>
	</string>
	<string id="save_near_campfire_tutorial_2">
		<text>During the game, you will be able to acquire %c[UI_orange]maps%c[default] of campfire locations in each territory. You can mark them on the map by clicking on the special icon in the form of a flame on the map in the handheld.</text>
	</string>
	<string id="tut_campfire_wet">
		<text>Wet campfires</text>
	</string>
	<string id="campfire_wet_tutorial">
		<text>In order to ignite a fire that got wet during the rain, you can use ignition fluid.\nTo do this, right-click on the item in your inventory and select '%c[UI_orange]Apply%c[default]' and then, pointing at the bonfire, press ($$ACTION_wpn_fire$$) or ($$ACTION_use$$).</text>
	</string>
	<string id="tut_zoom_switch">
		<text>Switching the aiming mode</text>
	</string>
	<string id="zoom_switch_tutorial">
		<text>When you have a scope mounted on your weapon, you can aim without using the scope. To change the aiming mode, press ($$ACTION_wpn_zoom_switch$$).</text>
	</string>
	<string id="ladder_tutorial">
		<text>Moving up a ladder:\n($$ACTION_forward$$) to move up\n($$ACTION_back$$) to move down.</text>
	</string>
	<string id="next_ammo_type">
		<text>($$ACTION_wpn_reload$$)</text>
	</string>
</string_table>

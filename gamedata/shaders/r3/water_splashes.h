// Hash functions shamefully stolen from:
// https://www.shadertoy.com/view/4djSRW

float hash12(float2 p)
{
	float3 p3 = frac(float3(p.xyx) * .1031);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.x + p3.y) * p3.z);
}

float2 hash22(float2 p)
{
	float3 p3 = frac(float3(p.xyx) * float3(.1031, .1030, .0973));
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.xx + p3.yz) * p3.zy);
}

float3 NoiseBasedRainSplashes(float2 tc)
{
	static const float density = saturate(rain_data.x);

	if (density <= 0.f)
		return 0.0;

	float2 p0 = floor(tc * 25);

	float2 circles = 0;

	for (int j = -WATER_SPLASHES_MAX_RADIUS; j <= WATER_SPLASHES_MAX_RADIUS; ++j)
	{
		for (int i = -WATER_SPLASHES_MAX_RADIUS; i <= WATER_SPLASHES_MAX_RADIUS; ++i)
		{
			float2 pi = p0 + float2(i, j);
		#ifdef WATER_SPLASHES_DOUBLE_HASH
			float2 hsh = hash22(pi);
		#else
			float2 hsh = pi;
		#endif
			float2 p = pi + hash22(hsh);

			float t = frac(0.75 * timers.x + hash12(hsh));
			float2 v = p - tc * 25;
			float d = length(v) - (float(WATER_SPLASHES_MAX_RADIUS) + 1.0) * t;

			float h = 1e-3;
			float d1 = d - h;
			float d2 = d + h;
			float p1 = sin(31. * d1) * smoothstep(-0.6, -0.3, d1) * smoothstep(0., -0.3, d1);
			float p2 = sin(31. * d2) * smoothstep(-0.6, -0.3, d2) * smoothstep(0., -0.3, d2);
			circles += 0.5 * normalize(v) * ((p2 - p1) / (2. * h) * (1. - t) * (1. - t));
		}
	}

	float c = float((WATER_SPLASHES_MAX_RADIUS * 2 + 1));
	circles /= c * c;

	float3 n = float3(circles, sqrt(1.0f - dot(circles, circles)));
	return n * density * density;
}
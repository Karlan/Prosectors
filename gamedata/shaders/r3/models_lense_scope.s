function normal	(shader, t_base, t_second, t_detail)
	shader:begin	("model_def_lplanes","model_scope_lense")
		: fog		(true)
		: zb		(true,false)
		: blend		(true,blend.srcalpha,blend.invsrcalpha)
		: aref		(true,0)
		: sorting	(2,true)
		: distort	(true)
end

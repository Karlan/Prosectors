#include "common.h"
uniform float4		c_resolution;		// Screen resolution (x-Width,y-Height, zw - 1/resolution)

//////////////////////////////////////////////////////////////////////////////////////////
// Vertex
v2p_TL main ( v_TL_positiont I )
{
	v2p_TL O;

//	O.HPos = P;

	{
		I.P.xy += 0.5f;
//		O.HPos.x = I.P.x/1024 * 2 - 1;
//		O.HPos.y = (I.P.y/768 * 2 - 1)*-1;
		O.HPos.x = I.P.x * c_resolution.z * 2 - 1;
		O.HPos.y = (I.P.y * c_resolution.w * 2 - 1)*-1;
		O.HPos.zw = I.P.zw;
	}

	O.Tex0 = I.Tex0;
	// O.Color = float4(I.Color.bgr*4, 1.0f);	//	swizzle vertex colour
	O.Color = unpack_D3DCOLOR(I.Color);	//	swizzle vertex colour
	O.Color = float4(O.Color.rgb*2, 1.0f);
	
 	return O;
}
// author: Karlan, Prosectors Project
#include "common.h"

struct	inWM
{
	float4	P		: POSITION;
	float4	N		: NORMAL;
	float2	tc0	: TEXCOORD0;
	float4	c0	: COLOR; 
};

struct	outWM
{
	float2 	tc0	: TEXCOORD0;
	float3	v2point		: TEXCOORD1;
	float3	norm		: TEXCOORD2;
	float4	tctexgen: TEXCOORD3;
	float3	Pv		: TEXCOORD4;
	float4	c0	: COLOR;
	float4 	HPos	: SV_Position;	// Clip-space position 	(for rasterization)
	float	fog		: FOG;

};

uniform float4x4	m_texgen;
//////////////////////////////////////////////////////////////////////////////////////////
// Vertex
outWM main ( inWM I )
{
	outWM O;
	
	I.N		=	unpack_D3DCOLOR(I.N);
		I.c0 = unpack_D3DCOLOR(I.c0);		//	swizzle vertex colour
	float3          N         = unpack_bx2(I.N);
	O.HPos = mul( m_VP, I.P );
	O.Pv = mul( m_V, I.P );
	O.v2point        = I.P-eye_position        ;
	O.fog       = saturate( calc_fogging  (I.P));
	O.tc0 = I.tc0;
	
	        float3         L_rgb	= I.c0.xyz;												// precalculated RGB lighting
        float3         L_hemi	= v_hemi(N)*(I.N.w*0.5+0.5);											// hemisphere
        float3         L_sun	= lerp(L_sun_color,L_sun_color*dot(N,-L_sun_dir_w),0.65f);	//v_sun(N);	//*v.color.w;	// sun
        float3         L_final	= L_rgb + L_hemi + L_sun + L_ambient;
		
	// O.Color = I.Color.bgra;		//	swizzle vertex colour
	O.tctexgen = mul( m_texgen, I.P);
O.norm = I.N;
O.c0		= float4		(L_final*0.9,I.c0.a);
 	return O;
}
#include "common.h"
//////////////////////////////////////////////////////////////////////////////////////////
uniform float4 c_resolution;

#define SMAA_HLSL_4_1

#define SMAA_INCLUDE_VS 1
#define SMAA_RT_METRICS c_resolution.zwxy
//////////////////////////////////////////////////////////////////////////////////////////
#include "smaa.h"

//Struct
struct p_smaa
{
	float4 hpos			: SV_Position;
	float2 tc0			: TEXCOORD0;        // Texture coordinates         (for sampling maps)
	float4 offset[3]	: TEXCOORD1;
};

//////////////////////////////////////////////////////////////////////////////////////////
// Vertex
p_smaa main(v2p_screen I)
{
	p_smaa O;
	O.hpos.x 	=  (I.HPos.x * c_resolution.z * 2 - 1);
	O.hpos.y 	= -(I.HPos.y * c_resolution.w * 2 - 1);
	O.hpos.zw 	=	I.HPos.zw;
	
    O.tc0 		= 	I.tc0;

	SMAAEdgeDetectionVS(I.tc0, O.offset);  

    return O; 
}
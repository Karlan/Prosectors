#include "common.h"
#include "skin.h"

// Vertex to Pixel struct
struct vf
{
	float2 tc0		: TEXCOORD0;
	float2 tc_tan	: TEXCOORD1;
	float3 v_pos	: POSITION;
	float3 v_N		: NORMAL;
	float4 hpos		: SV_Position;
};

uniform float4 m_hud_params;

vf	_main (v_model v)
{
	vf o;

	o.tc0		= v.tc.xy;			// Texture coordinates
	o.hpos		= mul(m_WVP, v.P);	// Homogenous position
	// Pass pos & normal / for A pixel
	o.v_pos		= mul(m_WV, v.P);	// Position in view space
	o.v_N		= mul(m_WV, v.N);	// Normal in view space

	// Pass V_tangent / for B pixel
	float3 T	= mul(m_WV, v.T);
	float3 B	= mul(m_WV, v.B);

	float3 V	= -o.v_pos;
	V.z			*= m_hud_params.z;	// HUD view correction
	o.tc_tan	= normalize(float3(dot(V, T), dot(V, B), dot(V, o.v_N))).xy;

	return o;
}

//Skinning
#ifdef SKIN_NONE
vf main(v_model v) { return _main(v); }
#endif

#ifdef SKIN_0
vf main(v_model_skinned_0 v) { return _main(skinning_0(v)); }
#endif

#ifdef SKIN_1
vf main(v_model_skinned_1 v) { return _main(skinning_1(v)); }
#endif

#ifdef SKIN_2
vf main(v_model_skinned_2 v) { return _main(skinning_2(v)); }
#endif

#ifdef SKIN_3
vf main(v_model_skinned_3 v) { return _main(skinning_3(v)); }
#endif

#ifdef SKIN_4
vf main(v_model_skinned_4 v) { return _main(skinning_4(v)); }
#endif

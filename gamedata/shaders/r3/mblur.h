#ifndef	MBLUR_H
#define MBLUR_H
uniform float4x4 m_current;
uniform float4x4 m_previous;
uniform float4 m_blur; // scale_x / 12, scale_y / 12, on / off, wpn on / off

static const int MBLUR_SAMPLES = 12;
static const float MBLUR_CLAMP = 0.001f;

float3 mblur(float2 UV, float3 pos, float3 c_original)	
{
	if (m_blur.z < 1)
		return c_original;
	
	pos.z += (saturate(0.001 - pos.z)*10000.f); // KD87
	
	const float4 pos4 = float4(pos, 1.0);
	const float4 p_current = mul(m_current, pos4);
	const float4 p_previous = mul(m_previous, pos4);
	
	float2 p_velocity = {0.0f, 0.0f};
	if (m_blur.w < 1 || pos.z >= 1.4) // todo: better use stenciling
	{
		p_velocity = ((p_current.xy / p_current.w) - (p_previous.xy / p_previous.w)) * m_blur.xy;
		p_velocity = clamp(p_velocity,-MBLUR_CLAMP,+MBLUR_CLAMP);
	}
	
	// For each sample, sum up each sample's color in "Blurred" and then divide
	// to average the color after all the samples are added.
	float3 accum = {0.0f, 0.0f, 0.0f};
	[unroll(MBLUR_SAMPLES)] for (int i = 0; i < MBLUR_SAMPLES; i++) accum += s_image.Sample(smp_rtlinear, p_velocity * float(i) + UV).rgb;
	
	accum /= MBLUR_SAMPLES;
	
	return lerp(c_original, accum, 1.0);
}
#endif
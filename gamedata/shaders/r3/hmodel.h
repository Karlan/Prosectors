#ifndef        HMODEL_H
#define HMODEL_H

#include "common.h"

TextureCube		env_s0;
TextureCube		env_s1;
//TextureCube		sky_s0;
//TextureCube		sky_s1;

uniform float4	env_color;        // color.w  = lerp factor
uniform float3x4	m_v2w;

#define SMALLSKY_CORRECTION .5f
#define HSUN_P0 0.04h
#define HSUN_P1 0.14h

void hmodel
(
	out float3 hdiffuse, out float3 hspecular, out float4 hsun,
	float m, float h, float s, float3 Pnt, float3 normal
)
{
        // hscale - something like diffuse reflection
	float3	nw		= mul( m_v2w, normal );
	float	hscale	= h;	//. *        (.5h + .5h*nw.y);

#ifdef         USE_GAMMA_22
			hscale	= (hscale*hscale);        // make it more linear
#endif

	// reflection vector
	float3	v2PntL	= normalize( Pnt );
	float3	v2Pnt	= mul( m_v2w, v2PntL );
	float3	vreflect= reflect( v2Pnt, nw );
	float	hspec	= .5h + .5h * dot( vreflect, v2Pnt );

#ifdef CGIM
	float SunDirBoost = saturate(dot(nw, -L_sun_dir_w));
	SunDirBoost = saturate(SMALLSKY_CORRECTION + (1.0f - SMALLSKY_CORRECTION) * SunDirBoost);
#else
	static const float SunDirBoost = 1.0f;
#endif

	// material	// sample material
	//float4	light	= tex3D( s_material, float3(hscale, hspec, m) );
//	float4	light	= s_material.Sample( smp_material, float3( hscale, hspec, m ) ).xxxy;
	float4	light	= s_material.SampleLevel( smp_material, float3( hscale, hspec, m ), 0 ).xxxy;
//	float4	light	= float4(1,1,1,1);

	// diffuse color
//	float3	e0d		= texCUBE( env_s0, nw );
//	float3	e1d		= texCUBE( env_s1, nw );
//	float3	e0d		= env_s0.Sample( smp_rtlinear, nw );
//	float3	e1d		= env_s1.Sample( smp_rtlinear, nw );
	float3	e0d		= env_s0.SampleLevel( smp_rtlinear, nw, 0 );
	float3	e1d		= env_s1.SampleLevel( smp_rtlinear, nw, 0 );
	float3	env_d	= env_color.xyz * lerp( e0d, e1d, env_color.w );
//			env_d	*= L_sky_color;	//(L_sky_color.x + L_sky_color.y + L_sky_color.z)/3.f
			env_d	*=env_d * SunDirBoost;	// contrast
			hdiffuse= env_d * light.xyz + L_ambient.rgb;

	// specular color
	vreflect.y      = vreflect.y*2-1;	// fake remapping
//	float3	e0s		= texCUBE( env_s0, vreflect );
//	float3	e1s		= texCUBE( env_s1, vreflect );
//	float3	e0s		= env_s0.Sample( smp_rtlinear, vreflect );
//	float3	e1s		= env_s1.Sample( smp_rtlinear, vreflect );
	float3	e0s		= env_s0.SampleLevel( smp_rtlinear, vreflect, 0 );
	float3	e1s		= env_s1.SampleLevel( smp_rtlinear, vreflect, 0 );
	float3	env_s	= env_color.xyz * lerp( e0s, e1s, env_color.w);
			env_s	*=env_s  * SunDirBoost;	// contrast
		hspecular	= env_s*light.w*s;                //*h*m*s        ;        //env_s        *light.w         * s;


	// hemi based sun
	hsun		= float4(0,0,0,0);
	if (L_ambient.w == 1) {		// dynamic sun is off
		float lhemi				= max(max(light.x,light.y),light.z);
		float alt_f				= 1.h - saturate((Ldynamic_dir.w-20.h) / 30.h);
		float p2				= 0.25f + alt_f * 0.1f;		// from 20 = 0.35 up to 50 = 0.25
		if (lhemi > HSUN_P1)	lhemi = smoothstep(HSUN_P1,p2,lhemi)*(1.f-HSUN_P1) + HSUN_P1;
		else					lhemi = smoothstep(HSUN_P0,HSUN_P1,lhemi)*HSUN_P1;
		lhemi					*= saturate(Ldynamic_dir.w / 8.h);		// fade at low altitude
		hsun					= Ldynamic_color * plight_infinity(m, Pnt, normal, Ldynamic_dir) * lhemi;
	}
}

/*
void         hmodel_table        (out float3 hdiffuse, out float3 hspecular, float m, float h, float s, float3 point, float3 normal)
{
        // hscale - something like diffuse reflection
        float         hscale         = h;

        // reflection vector
        float3         v2point        = normalize        (Pnt);
        float3        vreflect= reflect         (v2point,normal);
        float         hspec         = .5h+.5h*dot        (vreflect,v2point);

        // material
          float4         light        = tex3D                (s_material, float3(hscale, hspec, m) );                // sample material

        // diffuse color
        float3         env_d         = texCUBE         (env_s0,normal);

        // specular color
        float3         env_s          = texCUBE         (env_s0,vreflect);

        //
        hdiffuse        = env_d        *light.xyz         + L_ambient.rgb        ;
        hspecular        = env_s        *light.w         * s                ;
}
*/
#endif
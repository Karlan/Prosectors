#include "common.h"
uniform float4		c_resolution;		// Screen resolution (x-Width,y-Height, zw - 1/resolution)

//////////////////////////////////////////////////////////////////////////////////////////
// Vertex
v2p_postpr main ( v_postpr I )
{
	v2p_postpr	O;

//	O.HPos	= I.P;

	{
		I.P.xy += 0.5f;
//		O.HPos.x = I.P.x/1024 * 2 - 1;
//		O.HPos.y = (I.P.y/768 * 2 - 1)*-1;
		O.HPos.x = I.P.x * c_resolution.z * 2 - 1;
		O.HPos.y = (I.P.y * c_resolution.w * 2 - 1)*-1;
		O.HPos.zw = I.P.zw;
	}


	O.Tex0	= I.Tex0;
	O.Tex1	= I.Tex1;
	O.Tex2	= I.Tex2;
	
	// O.Color = I.Color.bgra;		//	swizzle vertex colour
	O.Color = unpack_D3DCOLOR(I.Color);		//	swizzle vertex colour
	// O.Gray = I.Gray.bgra;		//	swizzle vertex colour
	O.Gray = unpack_D3DCOLOR(I.Gray);		//	swizzle vertex colour
	
 	return O;
}
#include "common.h"

struct	inWM
{
	float4	P		: POSITION;
	float4	N		: NORMAL;
	float2	Tex0	: TEXCOORD0;
	float4	Color	: COLOR; 
};

//////////////////////////////////////////////////////////////////////////////////////////
// Vertex
v2p_TL main ( inWM I )
{
	v2p_TL O;

	O.HPos = mul( m_VP, I.P );
	O.Tex0 = I.Tex0;
	// O.Color = I.Color.bgra;		//	swizzle vertex colour
	O.Color = unpack_D3DCOLOR(I.Color);		//	swizzle vertex colour

 	return O;
}
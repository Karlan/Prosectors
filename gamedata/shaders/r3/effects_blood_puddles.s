--// author: Karlan, Prosectors Project

local tex_env0                = "$user$sky0"
local tex_env1                = "$user$sky1"

function normal(shader, t_base, t_second, t_detail)
	shader:begin("effects_blood_puddles","effects_blood_puddles")
			: blend		(true, blend.srcalpha,blend.invsrcalpha)
			: zb 		(true,false)
			: distort	(true)
			: fog		(true)
	assert(t_second)
	assert(t_detail)
	shader:dx10texture	("s_base",		t_base)
	shader:dx10texture	("s_env0",		tex_env0)
	shader:dx10texture	("s_env1",		tex_env1)
	-- shader:dx10texture	("s_image",		"$user$generic")
	shader:dx10sampler	("smp_bmarks")
	shader:dx10color_write_enable(true, true, true, false)
end
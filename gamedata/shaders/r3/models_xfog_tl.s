--// Karlan: special shader for tl model fogging (like hvatalka-ebalka, etc...)
--// (c) Prosectors Project

function normal(shader, t_base)
  shader:begin  ("model_distort4ghost","particle_fog_tl")
      : sorting  	(2,true)
      : blend    	(true,blend.srcalpha,blend.invsrcalpha)
      : aref     	(true,0)
      : zb     		(true,false)
      : fog    		(false)
      : distort   	(false)
	  -- : dx10stencil	(true, cmp_func.equal, 0x0, 0x0, stencil_op.keep, stencil_op.incr, stencil_op.keep)
	  -- : dx10stencil_ref	(0x1)
	shader: dx10texture ("s_base", t_base)
	shader: dx10sampler ("smp_base")
end
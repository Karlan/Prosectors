--// Karlan: special shader for model fogging (like liana, etc...)
--// (c) Prosectors Project

function normal    (shader, t_base, t_second, t_detail)
	shader:begin  ("model_def_lq","particle_fog")
      : fog       (false) --// ���������� ����� (� ������ �� ������������)
      : zb        (true,false) --// ��������� ��������� D3DCMP_LESSEQUAL ��� D3DCMP_ALWAYS / ������ � ��
      : blend     (true,blend.srcalpha,blend.one) --// ��������� ���������� / �������� / ��������
      : aref      (true,0) --// �������� �� ����� / ��������� ��������
      : sorting   (2,true) --// ���������� ��������� / sorted|emissive
	  : distort   (false) --// ���������� distort
	shader: dx10texture ("s_base", t_base)
	shader: dx10sampler ("smp_base")
end
#include "common.h"

uniform float4		c_resolution;		// Screen resolution (x-Width,y-Height, zw - 1/resolution)

//////////////////////////////////////////////////////////////////////////////////////////
// Vertex
v2p_TL0uv main ( v_TL0uv_positiont I )
{
	v2p_TL0uv O;

	{
		I.P.xy += 0.5f;
//		O.HPos.x = I.P.x/1024 * 2 - 1;
//		O.HPos.y = (I.P.y/768 * 2 - 1)*-1;
		O.HPos.x = I.P.x * c_resolution.z * 2 - 1;
		O.HPos.y = (I.P.y * c_resolution.w * 2 - 1)*-1;
		O.HPos.zw = I.P.zw;
	}

	// O.Color = I.Color.bgra;		//	swizzle vertex colour
	O.Color = unpack_D3DCOLOR(I.Color);		//	swizzle vertex colour

 	return O;
}
#include "common.h"

// Karlan: ������ ����������� ��������
// struct	v2p 
// {
	// float2 	Tex0	: TEXCOORD0;
	// float4 	HPos	: SV_Position;	 
// };
//////////////////////////////////////////////////////////////////////////////////////////
// Vertex
// v2p  main (uint id : SV_VertexID)
// {
 	// v2p O;
    // O.Tex0	= float2((id << 1) & 2, id & 2);
    // O.HPos 	= float4(O.Tex0 * float2(2.0f, -2.0f) + float2(-1.0f, 1.0f), 0.0f, 1.0f);

    // return O; 
// }

// Karlan: ������ ����������
uniform float4 c_resolution;
//////////////////////////////////////////////////////////////////////////////////////////
// Vertex
p_screen  main ( v2p_screen I )
{
	p_screen O;
	O.hpos.x 	=  (I.HPos.x * c_resolution.z * 2 - 1);
	O.hpos.y 	= -(I.HPos.y * c_resolution.w * 2 - 1);
	O.hpos.zw 	=	I.HPos.zw;
	
	O.tc0 		= 	I.tc0;
	
	return O; 
}


local tex_base                = "water\\water_studen"
local tex_nmap                = "water\\water_normal_15"
local tex_env0                = "$user$sky0"
local tex_env1                = "$user$sky1"

local tex_water				  = "water\\water_SBumpVolume"

function normal	(shader, t_base, t_second, t_detail)
	shader	:begin		("water_studen","water_studen")
			:sorting	(2, false)
			:blend		(true,blend.srcalpha,blend.invsrcalpha)
			:zb			(true,false)
			:distort	(true)
			:fog		(true)

	shader:dx10texture	("s_base",		tex_base)
	shader:dx10texture	("s_water",		tex_water)
	shader:dx10texture	("s_nmap",		tex_nmap)
	shader:dx10texture	("s_env0",		tex_env0)
	shader:dx10texture	("s_env1",		tex_env1)
	shader:dx10texture	("s_position",	"$user$position")

	shader:dx10sampler	("smp_base")
	shader:dx10sampler	("smp_nofilter")
	shader:dx10sampler	("smp_rtlinear")
end

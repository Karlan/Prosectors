function normal		(shader, t_base, t_second, t_detail)
	shader:begin("collimator_mark_plain","collimator_mark_plain")
		: fog		(false)
		: zb		(true,true)
		: blend		(true,blend.srcalpha,blend.invsrcalpha)
		: aref		(true,0)
		: sorting	(2,true)
		: distort	(false)
	shader:sampler("s_base"):texture(t_base):clamp() --LV aight, clamp cuz we dont want it to repeat
end

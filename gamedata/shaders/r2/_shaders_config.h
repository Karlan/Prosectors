/////////////////////////////////////////////////////////////
// improved parallax occlusion mapping
	#define POM_PARALLAX_OFFSET float(0.02)			// ��������. ��� ������, ��� ������ ����� ��������� �������.

/////////////////////////////////////////////////////////////
// ����� �� �����
	#define USE_GRASS_WAVE							// �������� "�����" �� ����� �� �����
	#define GRASS_WAVE_POWER float(2.0)				// "�������", ���������� ����
	#define GRASS_WAVE_FREQ float(0.7)				// ������� ��������� ����

/////////////////////////////////////////////////////////////
// Screen-Space Directional Occlusion
	#define SSDO_RADIUS float(0.02)				// ������ ������������� � ������� ������. ��� ����, ��� ����� ������� ������, �� ��� ������ ������������� ��������� �����
	#define SSDO_BLEND_FACTOR float(0.950)		// ���� �������. 0 - ������������, ��� ������ ��������, ��� �������� ������.
	#define SSDO_GRASS_TUNING float(1.0)		// ��������� ��������� �����. ��� ������, ��� ������ ����������.
	#define SSDO_DISCARD_THRESHOLD float(1.0)	// ������������ ������� � ������� ��������, ��� ������� ������� ��� ����������� � ������� ���������. ���������� ������� "�����" � ��������� �������.
	#define SSDO_USE_INDIRECT_BOUNCES			// ���� "����������" �������. ����� �������� ���������� ���������. ��������� ������������������, ���� ���� �� ������, � ����� ������������.
	#define SSDO_COLOR_BLEEDING float(25.0)		// ���� ����� �������. ���� ����� ������� ����, �� ��������� ������������� ������� � �����. ��� ����������� ������������ SSDO_BLEND_FACTOR.

/////////////////////////////////////////////////////////////
// Horizon-Based Ambient Occlusion	
	#define HBAO_NUM_STEPS int(3)			// ���������� ���������� ����� ��� ������ ���������. �������� ��������, �������� ������������������
	#define HBAO_RADIUS float(2.0)			// ������ ������ ���������.
	#define HBAO_STEP_SIZE float(4)			// ��� �������������. ������� �������� �������� � ����� ������ �����
	#define HBAO_ANGLE_BIAS float(0.5)		// ���� � �������� ��� ����������� ������������� ����� ��������������� ���������. �����������, ���� ��  ������ ��������� ������� ����.
	#define HBAO_THRESHOLD float(0.3)		// ����� ������������ �������. ��� ������, ��� ����� ������ ������ ����������.
	#define HBAO_GRASS_TUNING float(2.0)	// ��������� ��������� �����. ��� ������, ��� ������ ����������.
	#define HBAO_BLEND_FACTOR float(0.7)	// ���� �������. 0 - ������������, 1 - ������ ����.

/////////////////////////////////////////////////////////////
// FXAA
	#define FXAA_QUALITY__SUBPIX float(0.5)			// Choose the amount of sub-pixel aliasing removal.
													// This can effect sharpness.
													//   1.00 - upper limit (softer)
													//   0.75 - default amount of filtering
													//   0.50 - lower limit (sharper, less sub-pixel aliasing removal)
													//   0.25 - almost off
													//   0.00 - completely off
	#define FXAA_QUALITY__EDGE_THRESHOLD float(0.063)    // The minimum amount of local contrast required to apply algorithm.
													//   0.333 - too little (faster)
													//   0.250 - low quality
													//   0.166 - default
													//   0.125 - high quality 
													//   0.063 - overkill (slower)
	#define FXAA_QUALITY__EDGE_THRESHOLD_MIN float(0.0312)    // Trims the algorithm from processing darks.
													//   0.0833 - upper limit (default, the start of visible unfiltered edges)
													//   0.0625 - high quality (faster)
													//   0.0312 - visible limit (slower)
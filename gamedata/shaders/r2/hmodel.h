#ifndef        HMODEL_H
#define HMODEL_H

#include "common.h"

uniform samplerCUBE         env_s0                ;
uniform samplerCUBE         env_s1                ;
//uniform samplerCUBE         sky_s0                ;
//uniform samplerCUBE         sky_s1                ;
uniform half4                         env_color        ;        // color.w  = lerp factor
uniform half3x4                        m_v2w                ;

#define SMALLSKY_CORRECTION .5f
#define HSUN_P0 0.04h
#define HSUN_P1 0.14h

void hmodel	(out half3 hdiffuse, out half3 hspecular, out half4 hsun,
			half m, half h, half s, float3 point, half3 normal)
{
        // hscale - something like diffuse reflection
        half3         nw                         = mul                 (m_v2w,normal);
        half         hscale                 = h;                                //. *        (.5h + .5h*nw.y);
#ifdef         USE_GAMMA_22
                        hscale                = (hscale*hscale);        // make it more linear
#endif

        // reflection vector
        float3        v2pointL        		= normalize        (point);
        half3         v2point               = mul                (m_v2w,v2pointL);
        half3        vreflect        		= reflect         (v2point,nw);
        half         hspec                 	= .5h+.5h*dot        (vreflect,v2point);

#ifdef CGIM
	float SunDirBoost = saturate(dot(nw, -L_sun_dir_w));
	SunDirBoost = saturate(SMALLSKY_CORRECTION + (1.0f - SMALLSKY_CORRECTION) * SunDirBoost);
#else
	static const float SunDirBoost = 1.0f;
#endif
        // material
          half4         light                = tex3D                (s_material, half3(hscale, hspec, m) );                // sample material

        // diffuse color
        half3         e0d               = texCUBE         (env_s0,nw);
        half3         e1d               = texCUBE         (env_s1,nw);
        half3         env_d             = env_color.xyz*lerp(e0d,e1d,env_color.w)        ;
//		env_d		*= L_sky_color;	//(L_sky_color.x + L_sky_color.y + L_sky_color.z)/3.f
		env_d		*=env_d * SunDirBoost;	// contrast
        hdiffuse                        = env_d * light.xyz + L_ambient.rgb;

        // specular color
                        vreflect.y      = vreflect.y*2-1;                                                        // fake remapping
        half3         e0s               = texCUBE         (env_s0,vreflect);
        half3         e1s               = texCUBE         (env_s1,vreflect);
        half3         env_s             = env_color.xyz*lerp(e0s,e1s,env_color.w)        ;
		env_s		*=env_s  * SunDirBoost;	// contrast
        hspecular                       = env_s*light.w*s;                //*h*m*s        ;


		// hemi based sun
		hsun		= half4(0,0,0,0);
		if (L_ambient.w == 1) {		// dynamic sun is off
			half lhemi				= max(max(light.x,light.y),light.z);
			half alt_f				= 1.h - saturate((Ldynamic_dir.w-20.h) / 30.h);
			half p2					= 0.25f + alt_f * 0.1f;		// from 20 = 0.35 up to 50 = 0.25
			if (lhemi > HSUN_P1)	lhemi = smoothstep(HSUN_P1,p2,lhemi)*(1.f-HSUN_P1) + HSUN_P1;
			else					lhemi = smoothstep(HSUN_P0,HSUN_P1,lhemi)*HSUN_P1;
			lhemi					*= saturate(Ldynamic_dir.w / 8.h);		// fade at low altitude
			hsun					= Ldynamic_color * plight_infinity(m, point, normal, Ldynamic_dir) * lhemi;
		}

		// draw hemi model
/* 		if (env_mul.y > 0) {
			half tst		= 0;
			half lhemi		=		max(max(light.x,light.y),light.z);
			if (lhemi >= env_mul.x && lhemi < env_mul.y) tst = 1;
			hdiffuse		+= half3(0,0,tst);
			hsun			= half4(0,0,0,0);
		} */
}

/* 
void         hmodel_table        (out half3 hdiffuse, out half3 hspecular, half m, half h, half s, half3 point, half3 normal)
{
        // hscale - something like diffuse reflection
        half         hscale         = h;

        // reflection vector
        half3         v2point        = normalize        (point);
        half3        vreflect= reflect         (v2point,normal);
        half         hspec         = .5h+.5h*dot        (vreflect,v2point);

        // material
          half4         light        = tex3D                (s_material, half3(hscale, hspec, m) );                // sample material

        // diffuse color
        half3         env_d         = texCUBE         (env_s0,normal);

        // specular color
        half3         env_s          = texCUBE         (env_s0,vreflect);

        //
        hdiffuse        = env_d        *light.xyz         + L_ambient.rgb        ;
        hspecular        = env_s        *light.w         * s                ;
} */
#endif

![Logo](repo_logo.png)

Prosectors Project [![Discord](https://img.shields.io/discord/632902088073936896?label=Discord)](https://discord.gg/pw68yHHKav)
==========================
In this project, a comprehensive in-depth refactoring of the scripts of S.T.A.L.K.E.R.: Shadow of Chernobyl v1.0006 was carried out. We aimed to completely rework all the original scripts and popular script systems (added in other mods) to get maximum performance using new methods and features that the source code gives. In addition, many other problems with config files, logic, spawn objects, engine bugs, materials, etc. were fixed. We also added new systems and modules such as lua extensions, helper functions, debugging functionality, timers, storage, event-driven and signals systems for more comfortable work with scripts. We hope you will find this useful.

|Current version|1.7 Fix #6|
|---|---|
|Links|[Discord](https://discord.com/channels/632902088073936896/1164898568990105670/1218537423668121660)


---
### Documentation
[Installation guide](https://gitlab.com/Karlan/Prosectors/-/wikis/Installation-guide-%7C-%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D1%8F-%D0%BF%D0%BE-%D1%83%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B5).

[Changelog](https://gitlab.com/Karlan/Prosectors/-/wikis/Changelog-%7C-%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B9) and more is available in [wiki](https://gitlab.com/Karlan/Prosectors/-/wikis/home).

---
### Contributing

Contributions are welcome!

The `master` branch is meant to be stable. `Develop` is normally done in separate commits. [Tags](https://gitlab.com/Karlan/Prosectors/-/tags) are created to indicate new stable release versions of Prosectors Project.

If you find a bug or have an enhancement request, file an [issue](https://gitlab.com/Karlan/Prosectors/-/issues) or contact us via [discord](https://discord.gg/YrW38Tc).

---
### Credits
* [GSC Game World](https://gsc-game.com/) – for creating S.T.A.L.K.E.R. and supporting the community;
* [xp-dev team](https://xp-dev.com/summary/210311) - for engine;
* Directly contributed:
  * alpet - memory work advices, C++
  * amik - SDK, particles
  * Bars - PR, models, SDK, sounds
  * Charsi - scripts
  * Chypakabra - texts, testing
  * Dennis_Chikin - scripts
  * ed_rez - models, SDK, texturing
  * Ell - PR, game-design, texts, testing
  * fastle (paradox) - ui-design, models, texturing
  * GeJorge (Nick_Mondyfic) - scripts, web
  * gurich - game-design
  * igor.doc - quests, texts
  * KD87 - renderer and kinematics
  * mauvais - models, SDK, texturing
  * Real_Wolf - С++
  * VeNoMMaNs - env-design, 3D-design, texturing
  * waylander - configs
  * yurv3 - texts
* Engine fixes: abramcumner, alpet, Charsi, KD87, Shoker, Lost Alpha, Lost Alpha DC, NLC 7, X-Ray Oxygen, OpenXRay, IX-Ray

* Renderer credits: Red panda, KD87, LVutner, mihan-323 (Миша Юдин), Zagolski, RainbowZerg, vTurbine, rafa, Crossire, Lanforse, daemonjax, Zhora Cementow, cjayho, Meltac, abramcumner, FozeSt, SonicEthers, David Hoskins, BigWIngs, Zavie, Ascii1457, Xerxes1138, Panos Karabelas, Sébastien Lagarde, xp-dev team, X-Ray Oxygen, Lost Alpha, Anomaly 1.5.1, Screen Space Shaders

* Scripts credits to all SoC-community (in the main it's AMK, SIMBION, ZRP, bug fix attempt; detailed can see in script-files)

* Content: STCoP Weapon Pack 3.4, AtmosFear 3, NLC 7, Particle Paradise 2.1.1.9, Anomaly 1.5.1, Lost Alpha DC, Food drug and drinks animations, OGSM CS 1.8 CE, Объединенный Пак 2.1, CGIM, Customization Pack 0.83, SRP 0.2, Stalker Artefact Pack volume 1 (by Valerich&Desertir), Arena Extension Mod 0.3.2 (by kstn&IG-2007), Domestos, 13DieS

* Maps: Darkscape from build 2571, Dead City from OGSE 0.6.9.3, Pripyat underground by krovosnork and the other maps from Stalker Map Pack (by Kostya_V)

If your work is being used in our project and you are not mentioned here, please, write to us and we will add you.